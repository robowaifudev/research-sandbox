# Hold onto your papers!

**Last updated**: 28 Oct 2020

This is a non-exhaustive curated list of papers to provide an overview of advancements in machine learning and links to [Google Scholar](https://scholar.google.com/) to make it easier to find new papers. Some abstracts have been truncated for brevity.

TL;DR summaries were generated and provided by [SciTLDR](https://scitldr.apps.allenai.org/) ([GitHub](https://github.com/allenai/scitldr))

## Content

+ [Glossary and Nomenclature](#glossary-and-nomenclature)
+ [Additional reading](#additional-reading)

## Glossary and Nomenclature

+ **AttentionGAN**: Attention-Guided Generative Adversarial Network
+ **BPTT**: Backpropagation Through Time
+ **CNN**: Convolutional Neural Network
+ **CycleGAN**: Cycle-consistent Generative Adversarial Network
+ **DenseNet**: Densely Connected Convolutional Networks
+ **DNC**: Differentiable Neural Computer
+ **GA**: Genetic Algorithm
+ **GAN**: Generative Adversarial Network
+ **Img2Img**: Image-to-image translation
+ **JSD**: Jensen-Shanon Divergence
+ **KLD**: Kullback-Leibler Divergence
+ **LVM**: latent variable model
+ **LSTM**: Long Short-Term Memory
+ **MANN**: Memory Augmented Neural Network 
+ **MCTS**: Monte Carlo Tree Search
+ **MIM**: Mututal Information Machine
+ **MoS**: Mixture of Softmaxes
+ **NLP**: Natural Language Processing
+ **PPL**: perplexity
+ **PPO**: Proximal Policy Optimization
+ **ResNet**: Residual Neural Network
+ **RL**: Reinforcement Learning
+ **SAM**: Sparse Access Memory
+ **Seq2Seq**: Sequence-to-sequence translation
+ **SE**: Squeeze and Excitation
+ **TCA**: Temporal Credit Assignment
+ **VAE-GAN**: Variational Auto-Encoder Generative Adversarial Network
+ **VAE**: Variational Auto-Encoder
+ **Vec2Vec**: Vector-to-vector translation

## 1997

### 🌟 *LSTM*: Long Short-Term Memory ([paper](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.676.4320&rep=rep1&type=pdf)) ([cited by 30000+](https://scholar.google.com/scholar?cites=1035920125298492854&as_sdt=2005&sciodt=0,5&hl=en))
By [Sepp Hochreiter](https://scholar.google.com/citations?user=tvUH3WMAAAAJ), [Jürgen Schmidhuber](https://scholar.google.com/citations?user=gLnCTgIAAAAJ)

**tl;dr** Learning to store information over extended time intervals via recurrent backpropagation takes a very long time, mostly due to insufficient, decaying error back flow.

**Keywords**: LSTM, RNN

---

### On the optimality of the simple Bayesian classifier under zero-one loss([paper](http://edlab-www.cs.umass.edu/cs689/reading/optimality%20of%20naive%20bayes%20classifier.pdf)) ([cited by 3000+](https://scholar.google.com/scholar?cites=8190045656011597473))
By [Pedro Domingos](https://scholar.google.com/citations?user=KOrhfVMAAAAJ), Michael Pazzani

**tl;dr** The Bayesian classifier is optimal under zero-one loss (misclassification rate) even when this assumption is violated by a wide margin.

**Keywords**: Bayesian

## 1998

### *LeNet-5*: Gradient-Based Learning Applied to Document Recognition ([paper](http://yann.lecun.com/exdb/publis/pdf/lecun-01a.pdf)) ([cited by 20000+](https://scholar.google.com/scholar?cites=1909057046224785356))
By Yann LeCun, Léon Bottou, Yoshua Bengio, Patrick Haffner

**tl;dr** Multilayer Neural Networks trained with the backpropagation algorithm constitute the best example of a successful Gradient-Based Learning technique.

**Keywords**: CNN

## 1999

### Learning to forget: Continual prediction with LSTM ([paper](https://www.researchgate.net/profile/Felix_Gers/publication/12292425_Learning_to_Forget_Continual_Prediction_with_LSTM/links/5759414608ae9a9c954e84c5/Learning-to-Forget-Continual-Prediction-with-LSTM.pdf)) ([cited by 2000+](https://scholar.google.com/scholar?cites=8872883539551190379))
By Felix A. Gers, [Jürgen Schmidhuber](https://scholar.google.com/citations?user=gLnCTgIAAAAJ), Fred Cummins

**tl;dr** A novel, adaptive "forget gate" that enables an LSTM cell to learn to reset itself at appropriate times, thus releasing internal resources.

**Keywords**: LSTM


## 2000

### *ReLU**: Digital selection and analogue amplification coexist in a cortex-inspired silicon circuit ([paper](http://csbi.mit.edu/people/pdf/nature_cover.pdf)) ([cited by 700+](https://scholar.google.com/scholar?cites=8091746083324814153))
By Richard H. R. Hahnloser, Rahul Sarpeshkar, Misha A. Mahowald, Rodney J. Douglas & H. Sebastian Seung

**tl;dr** The neocortex combines digital selection of an active set of neurons with analogue response by dynamically varying the positive feedback inherent in its recurrent connections.

**Keywords**: RELU, activation function

*: The authors did not name it but introduced the concept of ReLU (see Glorot et al. 2011)

## 2002

### *NEAT*: Evolving Neural Networks through Augmenting Topologies ([paper](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.28.5457&rep=rep1&type=pdf)) ([cited by 2000+](https://scholar.google.ca/scholar?cites=14852294327125551644))
By [Kenneth O. Stanley](https://scholar.google.ca/citations?user=6Q6oO1MAAAAJ), Risto Miikkulainen

**tl;dr** NeuroEvolution of Augmenting Topologies (NEAT) outperforms the best fixed-topology method on a challenging benchmark reinforcement learning task.

**Keywords**: neuroevolution, GA

## 2004


### Feature selection, L1 vs. L2 regularization, and rotational invariance ([paper](http://robotics.stanford.edu/~ang/papers/icml04-l1l2.pdf)) ([cited by 1000+](https://scholar.google.com/scholar?cites=11156383625480008410))
By [Andrew Y. Ng](https://scholar.google.com/citations?user=mG4imMEAAAAJ)

**tl;dr** We show that L1 regularized logistic regression can be effective even if there are exponentially many irrelevant features as there are training examples.

has a worst case sample complexity that grows at least linearly in the number of irrelevant features.

**Keywords**: regularization

## 2005


### Framewise Phoneme Classification with Bidirectional LSTM and Other Neural Network Architectures
By Alex Graves and Jurgen Schmidhuber

**tl;dr** In this paper, we present bidirectional LSTM networks, and a modified, full gradient version of the LSTM learning algorithm.

## 2006

### Connectionist temporal classification: labelling unsegmented sequence data with recurrent neural networks ([paper](http://www.idsia.ch/~santiago/papers/icml2006.pdf)) ([cited by 2000+](https://scholar.google.com/scholar?cites=6142126079742865912))
By Alex Graves, Santiago Fernández, Faustino John Gomez, Jürgen Schmidhuber

**tl;dr** This paper presents a novel method for training RNNs to label unsegmented sequences directly, thereby solving both problems.

**Keywords**: RNN, speech recognition

---

### Markov logic networks ([paper](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.170.7952&rep=rep1&type=pdf)) ([cited by 2000+](https://scholar.google.com/scholar?cites=1545177577956914978))
By Matthew Richardson, [Pedro Domingos](https://scholar.google.com/citations?user=KOrhfVMAAAAJ)

**tl;dr** We propose a simple approach to combining first-order logic and probabilistic graphical models in a single representation.

**Keywords**: Markov network

## 2009

### *ImageNet*: A Large-Scale Hierarchical Image Database ([paper](http://vision.stanford.edu/documents/ImageNet_CVPR2009.pdf)) ([cited by 10000+](https://scholar.google.com/scholar?cites=610894740843277394))

**tl;dr** ImageNet is a large-scale ontology of images built upon the WordNet structure.

**Keywords**: image classification, dataset

---

### *HyperNEAT*: A Hypercube-Based Encoding for Evolving Large-Scale Neural Networks ([paper](https://stars.library.ucf.edu/cgi/viewcontent.cgi?article=3177&context=facultybib2000)) ([cited by 600+](https://scholar.google.com/scholar?cites=5870589580018203360))
By [Kenneth O. Stanley](https://scholar.google.ca/citations?user=6Q6oO1MAAAAJ), David B. D'Ambrosio and Jason Gauci

**tl;dr** HyperNEAT employs an indirect encoding called connective compositional pattern-producing networks (CPPNs) to produce connectivity patterns with symmetries.

**Keywords**: NEAT, neuroevolution, GA

## 2011

### *RELU*: Deep Sparse Rectifier Neural Networks ([paper](http://proceedings.mlr.press/v15/glorot11a/glorot11a.pdf)) ([cited by 4000+](https://scholar.google.com/scholar?cites=10040883758431450991))
By Xavier Glorot, Antoine Bordes, Yoshua Bengio

**tl;dr** Logistic sigmoid neurons are more biologically plausible than hyperbolic tangent neurons and work better for training multi-layer neural networks.

Keywords: RELU, activation function

## 2012

### *AlexNet*: ImageNet Classification with Deep Convolutional Neural Networks ([paper](https://papers.nips.cc/paper/4824-imagenet-classification-with-deep-convolutional-neural-networks.pdf)) ([cited by 60000+](https://scholar.google.com/scholar?cites=2071317309766942398))
By Alex Krizhevsky, [Ilya Sutskever](https://scholar.google.ca/citations?user=x04W_mMAAAAJ), [Geoffrey E. Hinton](https://scholar.google.ca/citations?hl=en&user=JicYPdAAAAAJ)

**tl;dr** We trained a large, deep convolutional neural network to classify the 1.2 million high-resolution images in the ImageNet LSVRC-2010 contest into the 1000 different classes.

**Keywords**: CNN, dropout, regularization

## 2013

### Speech recognition with deep recurrent neural networks ([paper](https://www.academia.edu/download/57926482/10.pdf)) ([cited by 5000+](https://scholar.google.com/scholar?cites=9041586046771167211))
By [Alex Graves](https://scholar.google.com/citations?user=DaFHynwAAAAJ), Abdel-rahman Mohamed, [Geoffrey Hinton](https://scholar.google.ca/citations?hl=en&user=JicYPdAAAAAJ)

**tl;dr** Deep Long Short-term Memory RNNs for phoneme recognition with suitable regularisation.

**Keywords**: RNN, speech recognition

---

### *NIN*: Network In Network ([arXiv:1312.4400](https://arxiv.org/pdf/1312.4400)) ([cited by 3000+](https://scholar.google.com/scholar?cites=3211704355758672916))
By Min Lin, Qiang Chen, Shuicheng Yan

**tl;dr** We propose a novel deep network structure called Network In Network (NIN) to enhance model discriminability for local patches within the receptive field.

**Keywords**: network architecture

---

### k-Sparse Autoencoders ([arXiv:1312.5663](https://arxiv.org/pdf/1312.5663)) ([cited by 100+](https://scholar.google.com/scholar?cites=9239915888998917940))
By Alireza Makhzani, Brendan Frey

**tl;dr** We propose a k-sparse autoencoder with linear activation function, where in hidden layers only the k highest activities are kept.

**Keywords**: autoencoder

---

### *VAE*: Auto-Encoding Variational Bayes ([arXiv:1312.6114](https://arxiv.org/pdf/1312.6114)) ([cited by 8000+](https://scholar.google.com/scholar?cites=10486756931164834716))
By Diederik P Kingma, Max Welling

**tl;dr** Efficient inference and learning in directed probabilistic models with intractable posterior distributions, and large datasets.

**Example code (PyTorch)**: [github.com/pytorch/examples/blob/master/vae/main.py](https://github.com/pytorch/examples/blob/master/vae/main.py)

**Keywords**: variational autoencoder

## 2014

### Deep Symmetry Networks ([paper](http://papers.nips.cc/paper/5424-deep-symmetry-networks)) ([cited by 100+](https://scholar.google.com/scholar?cites=4820202775829919990))
By Robert Gens and [Pedro M. Domingos](https://scholar.google.com/citations?user=KOrhfVMAAAAJ)

**tl;dr** We introduce deep symmetry networks, a generalization of convnets that forms feature maps over arbitrary symmetry groups.

**Keywords**: network architecture, symmetry, symmetry groups, group theory

---

### *Dropout*: A Simple Way to Prevent Neural Networks from Overfitting ([paper](http://www.jmlr.org/papers/volume15/srivastava14a/srivastava14a.pdf)) ([cited by 10000+](https://scholar.google.com/scholar?cites=17092600409158696067))
By Nitish Srivastava, Geoffrey Hinton, Ilya Sutskever, Ruslan Salakhutdinov

**tl;dr** We show that dropout improves the performance of neural networks on supervised learning tasks in vision, speech recognition, document classification and computational biology.

**Keywords**: neural networks, regularization, model combination, deep learning

---

### *GAN*: Generative Adversarial Networks ([arXiv:1406.2661](https://arxiv.org/pdf/1406.2661)) ([cited by +10000](https://scholar.google.com/scholar?cites=11977070277539609369))
By Ian J. Goodfellow, Jean Pouget-Abadie, Mehdi Mirza, Bing Xu, David Warde-Farley, Sherjil Ozair, Aaron Courville, Yoshua Bengio

**tl;dr** We propose a new framework for estimating generative models via an adversarial process, in which we simultaneously train two models: a generative model and a discriminative model.

**Keywords**: GAN, image generation

---

### Robots that can adapt like animals ([arXiv:1407.3501](https://arxiv.org/pdf/1407.3501)) ([cited by 400+](https://scholar.google.com/scholar?cites=8737599281441058737))
By Antoine Cully, [Jeff Clune](https://scholar.google.ca/citations?hl=en&user=5TZ7f5wAAAAJ), Danesh Tarapore, Jean-Baptiste Mouret 

**tl;dr** We introduce an intelligent trial-and-error learning algorithm that allows robots to adapt to damage in less than two minutes in large search spaces.

**Keywords**: fail-safety


---

### *VGG*: Very Deep Convolutional Networks for Large-Scale Image Recognition ([arXiv:1409.1556](https://arxiv.org/pdf/1409.1556)) ([cited by 30000+](https://scholar.google.com/scholar?cites=15993525775437884507))
By Karen Simonyan, Andrew Zisserman

**tl;dr** In this work we investigate the effect of the convolutional network depth on its accuracy in the large-scale image recognition setting.

**Keywords**: CNN

---

### Neural Turing Machines ([arXiv:1410.5401](https://arxiv.org/pdf/1410.5401)) ([cited by 1000+](https://scholar.google.com/scholar?cites=8413767524552071308))
By Alex Graves, Greg Wayne, Ivo Danihelka

**tl;dr** We extend the capabilities of neural networks by coupling them to external memory resources, which they can interact with by attentional processes.

**Keywords**: network architecture, MANN

## 2015

### Human-level control through deep reinforcement learning ([paper](https://daiwk.github.io/assets/dqn.pdf)) ([cited by 9000+](https://scholar.google.com/scholar?cites=12439121588427761338))
By Volodymyr Mnih, Koray Kavukcuoglu, [David Silver](https://scholar.google.com/citations?user=-8DNE4UAAAAJ), Andrei A. Rusu, Joel Veness, Marc G. Bellemare, [Alex Graves](https://scholar.google.com/citations?user=DaFHynwAAAAJ), Martin Riedmiller, Andreas K. Fidjeland, Georg Ostrovski, Stig Petersen, Charles Beattie, Amir Sadik, Ioannis Antonoglou, Helen King, Dharshan Kumaran, Daan Wierstra, Shane Legg, Demis Hassabis

**tl;dr** A deep Q-network that can learn successful policies directly from high-dimensional sensory inputs using end-to-end reinforcement learning.

**Keywords**: RL, deep Q learning

---

### Show, Attend and Tell: Neural Image Caption Generation with Visual Attention ([arXiv:1502.03044](https://arxiv.org/pdf/1502.03044)) ([cited by 4000+](https://scholar.google.com/scholar?cites=9471583366007765258))
By Kelvin Xu, Jimmy Ba, Ryan Kiros, Kyunghyun Cho, Aaron Courville, Ruslan Salakhutdinov, Richard Zemel, Yoshua Bengio

**tl;dr** We introduce an attention based model that automatically learns to describe the content of images using a variational lower bound.

**Keywords**: attention

---

### DRAW: A Recurrent Neural Network For Image Generation ([arXiv:1502.04623](https://arxiv.org/pdf/1502.04623)) ([cited by 1000+](https://scholar.google.com/scholar?cites=8022513888710268841))
By Karol Gregor, Ivo Danihelka, Alex Graves, Danilo Jimenez Rezende, Daan Wierstra

**tl;dr** This paper introduces the Deep Recurrent Attentive Writer (DRAW) neural network architecture for image generation.

**Keywords**: RNN, image generation

---

### End-To-End Memory Networks ([arXiv:1503.08895](https://arxiv.org/pdf/1503.08895)) ([cited by 1000+](https://scholar.google.com/scholar?cites=9907515383987281804))
By Sainbayar Sukhbaatar, Arthur Szlam, Jason Weston, Rob Fergus

**tl;dr** We introduce a neural network with a recurrent attention model over a possibly large external memory.

**Keywords**: MANN

---

### *HighwayNet*: Highway networks ([arXiv:1505.00387](https://arxiv.org/pdf/1505.00387)) ([cited by 1000+](https://scholar.google.com/scholar?cites=5320226408800263727))
By Rupesh Kumar Srivastava, Klaus Greff, [Jürgen Schmidhuber](https://scholar.google.com/citations?user=gLnCTgIAAAAJ)

**tl;dr** We introduce a new architecture designed to ease gradient-based training of very deep networks, which learn to regulate the flow of information through a network.

**Keywords**: network architecture

---

### *Spatial Transformer*: Spatial Transformer Networks ([arXiv:1506.02025](https://arxiv.org/pdf/1506.02025)) ([cited by 2000+](https://scholar.google.com/scholar?cites=1662293494062093494))
By Max Jaderberg, Karen Simonyan, Andrew Zisserman, Koray Kavukcuoglu

**tl;dr** We introduce a new learnable module, the Spatial Transformer, which explicitly allows the spatial manipulation of data within the network.

**Keywords**: affine transformation

---

### Teaching Machines to Read and Comprehend ([arXiv:1506.03340](https://arxiv.org/pdf/1506.03340)) ([cited by 1000+](https://scholar.google.com/scholar?cites=5371787459515302436))
By Karl Moritz Hermann, Tomáš Kočiský, Edward Grefenstette, Lasse Espeholt, Will Kay, Mustafa Suleyman, Phil Blunsom

**tl;dr** Large scale supervised reading comprehension data for machine reading systems.

**Keywords**: NLP

---

### *Neural Style Transfer*: A Neural Algorithm of Artistic Style ([arXiv:1508.06576](https://arxiv.org/pdf/1508.06576)) ([cited by 1000+](https://scholar.google.com/scholar?cites=6343685530593283491))
By Leon A. Gatys, Alexander S. Ecker, Matthias Bethge

**tl;dr** A neural algorithm for the creation of artistic images of high perceptual quality.

**Keywords**: neural style transfer, CNN

---

### Design and Implementation of Multi-dimensional Flexible Antena-like Hair motivated by ’Aho-Hair’ in Japanese Anime Cartoons: Internal State Expressions beyond Design Limitations ([paper](https://spqrchan.xyz/.media/e9c9210fb1b8f194b82f65cd952c6c43-applicationpdf.pdf))
By Kazuhiro Sasabuchi, Yohei Kakiuchi, Kei Okada and Masayuki Inaba

**tl;dr** A non-facial contextual expression for emotion-expressive and interactive humanoid robots.

**Keywords**: robotics, emotional expression

---

### Continuous control with deep reinforcement learning ([arXiv:1509.02971](https://arxiv.org/pdf/1509.02971)) ([cited by 3000+](https://scholar.google.com/scholar?cites=4133004576987558805))
By Timothy P. Lillicrap, Jonathan J. Hunt, Alexander Pritzel, Nicolas Heess, Tom Erez, Yuval Tassa, [David Silver](https://scholar.google.com/citations?user=-8DNE4UAAAAJ), Daan Wierstra

**tl;dr** We present an actor-critic, model-free algorithm based on the deterministic policy gradient that can operate over continuous action spaces.

**Keywords**: deep Q learning, RL

---

### Deep Compression: Compressing Deep Neural Networks with Pruning, Trained Quantization and Huffman Coding ([arXiv:1510.00149](https://arxiv.org/pdf/1510.00149)) ([cited by 3000+](https://scholar.google.com/scholar?cites=7860777411990654691))
By Song Han, Huizi Mao, William J. Dally

**tl;dr** Deep compression reduces the storage requirement of neural networks by 35x to 49x without affecting their accuracy.

**Keywords**: compression

---

### Unsupervised Representation Learning with Deep Convolutional Generative Adversarial Networks ([arXiv:1511.06434](https://arxiv.org/pdf/1511.06434)) ([cited by 6000+](https://scholar.google.com/scholar?cites=3321343160055675528))
By Alec Radford, Luke Metz, Soumith Chintala

**tl;dr** DCGANs: Deep Convolutional Generative Adversarial Networks for unsupervised learning.

**Keywords**: CNN, GAN

---

### Fast and Accurate Deep Network Learning by Exponential Linear Units (ELUs) ([arXiv:1511.07289](https://arxiv.org/pdf/1511.07289)) ([cited by 2000+](https://scholar.google.com/scholar?cites=13103600599147838489))
By Djork-Arné Clevert, Thomas Unterthiner, [Sepp Hochreiter](https://scholar.google.com/citations?user=tvUH3WMAAAAJ)

**tl;dr** We introduce the "exponential linear unit" (ELU) which speeds up learning in deep neural networks and leads to higher classification accuracies.

**Keywords**: activation function

---

### *ResNet*: Deep Residual Learning for Image Recognition ([arXiv:1512.03385](https://arxiv.org/pdf/1512.03385)) ([cited by 45000+](https://scholar.google.com/scholar?cites=9281510746729853742))
By [Kaiming He](https://scholar.google.com/citations?user=DhtAFkwAAAAJ), [Xiangyu Zhang](https://scholar.google.com/citations?user=yuB-cfoAAAAJ), [Shaoqing Ren](https://scholar.google.com/citations?user=AUhj438AAAAJ), Jian Sun

**tl;dr** We present a residual learning framework to ease the training of networks that are substantially deeper than those used previously.


**Keywords**: ResNet

## 2016

### Interactive Sound Propagation with Bidirectional Path Tracing ([paper](https://dl.acm.org/doi/pdf/10.1145/2980179.2982431)) ([cited by 30+](https://scholar.google.com/scholar?cites=4970171412964785325))
By Chunxiao Cao, Zhong Ren, Carl Schissler, Dinesh Manocha, Kun Zhou

**tl;dr** We introduce Bidirectional Sound Transport (BST), a new algorithm that simulates sound propagation by bidirectional path tracing using multiple importance sampling.

Video: [Sound Propagation With Bidirectional Path Tracing | Two Minute Papers #111](https://www.youtube.com/watch?v=DzsZ2qMtEUE)

---

### Taking the Human Out of the Loop: A Review of Bayesian Optimization ([paper](https://www.cs.princeton.edu/~rpa/pubs/shahriari2016loop.pdf)) ([cited by 1000+](https://scholar.google.com/scholar?cites=2039456143890648437))
By Bobak Shahriari, Kevin Swersky, Ziyu Wang, Ryan P. Adams, Nando de Freitas

**tl;dr** Bayesian Optimization for Big Data Applications: A Review

**Keywords**: Bayesian, meta-learning

---

### *AlphaGo*: Mastering the game of Go with deep neural networks and tree search ([paper](https://www.cs.cmu.edu/afs/cs.cmu.edu/academic/class/15780-s16/www/AlphaGo.nature16961.pdf)) ([cited by 7000+](https://scholar.google.com/scholar?cites=300412370207407505))
By [David Silver](https://scholar.google.com/citations?user=-8DNE4UAAAAJ), Aja Huang, Chris J. Maddison, Arthur Guez, Laurent Sifre, George van den Driessche,  Julian Schrittwieser, Ioannis Antonoglou, Veda Panneershelvam, Marc Lanctot, Sander Dieleman, Dominik Grewe, John Nham, Nal Kalchbrenner, [Ilya Sutskever](https://scholar.google.ca/citations?user=x04W_mMAAAAJ), [Timothy Lillicrap](https://scholar.google.ca/citations?user=htPVdRMAAAAJ), Madeleine Leach, Koray Kavukcuoglu,  Thore Graepel, Demis Hassabis

**tl;dr** We train deep neural networks to play Go at the level of Monte Carlo tree search programs that simulate thousands of random games of self-play.

**Keywords**: RL, MCTS

---

### How a life-like system emerges from a simplistic particle motion law ([paper](https://www.nature.com/articles/srep37969)) ([cited by 10+](https://scholar.google.com/scholar?cites=1093018564468047343))
By Thomas Schmickl, Martin Stefanec, Karl Crailsheim

**tl;dr** We discovered a self-structuring, self-reproducing and self-sustaining life-like system.

**Video demo**: [Youtube](https://www.youtube.com/watch?v=makaJpLvbow)

**Keywords**: emergent behavior

---

### *A3C*: Asynchronous Methods for Deep Reinforcement Learning ([arXiv:1602.01783](https://arxiv.org/pdf/1602.01783)) ([cited by 3000+](https://scholar.google.com/scholar?cites=14460380466928185185))
By Volodymyr Mnih, Adrià Puigdomènech Badia, Mehdi Mirza, [Alex Graves](https://scholar.google.com/citations?user=DaFHynwAAAAJ), [Timothy P. Lillicrap](https://scholar.google.ca/citations?user=htPVdRMAAAAJ), Tim Harley, [David Silver](https://scholar.google.com/citations?user=-8DNE4UAAAAJ), Koray Kavukcuoglu

**tl;dr** We propose a conceptually simple and lightweight framework for deep reinforcement learning that uses asynchronous gradient descent for optimization of deep neural network controllers.

**Keywords**: RL, A3C

---

### Binarized Neural Networks: Training Deep Neural Networks with Weights and Activations Constrained to +1 or -1 ([arXiv:1602.02830](https://arxiv.org/pdf/1602.02830)) ([cited by 1000+](https://scholar.google.com/scholar?cites=3285803655136066432))
By Matthieu Courbariaux, Itay Hubara, Daniel Soudry, Ran El-Yaniv, Yoshua Bengio

**tl;dr** We introduce a method to train Binarized Neural Networks (BNNs) with binary weights and activations at run-time.

**Keywords**: network architecture

---

### *SqueezeNet*: AlexNet-level accuracy with 50x fewer parameters and <0.5MB model size ([arXiv:1602.07360](https://arxiv.org/pdf/1602.07360)) ([cited by 2000+](https://scholar.google.com/scholar?cites=17131899958223648583))
By Forrest N. Iandola, Song Han, Matthew W. Moskewicz, Khalid Ashraf, William J. Dally, Kurt Keutzer

**tl;dr** We propose a small DNN architecture called SqueeNet that achieves AlexNet-level accuracy on ImageNet with 50x fewer parameters.

**Source code**: [github.com/DeepScale/SqueezeNet](https://github.com/DeepScale/SqueezeNet)

**Keywords**: compression

---

### *PELU*: Parametric Exponential Linear Unit for Deep Convolutional Neural Networks ([arXiv:1605.09332](https://arxiv.org/pdf/1605.09332)) ([cited by 80+](https://scholar.google.com/scholar?cites=15166150093841301194))
By Ludovic Trottier, Philippe Giguère, Brahim Chaib-draa

**tl;dr** We propose learning a parameterization of ELU in order to learn the proper activation shape at each layer in the CNNs.

**Keywords**: PELU, ELU, RELU, activation function


---

### *DenseNet*: Densely Connected Convolutional Networks ([arXiv:1608.06993](https://arxiv.org/pdf/1608.06993)) ([cited by 8000+](https://scholar.google.com/scholar?cites=4205512852566836101))
By Gao Huang, Zhuang Liu, Laurens van der Maaten, Kilian Q. Weinberger

**tl;dr** We introduce the Dense Convolutional Network (DenseNet), which connects each layer to every other layer in a feed-forward fashion.

**Keywords**: ResNet, DenseNet, CNN

---

### *Layer Normalization* ([arXiv:1607.06450](https://arxiv.org/pdf/1607.06450)) ([cited by 1000+](https://scholar.google.com/scholar?cites=4160792425577293394))
By Jimmy Lei Ba, Jamie Ryan Kiros, Geoffrey E. Hinton

**tl;dr** We apply batch normalization to recurrent neural networks and we show that layer normalization can substantially reduce the training time compared with previously published techniques.

**Keywords**: normalization

---

### *Instance Normalization*: The Missing Ingredient for Fast Stylization ([arXiv:1607.08022](https://arxiv.org/pdf/1607.08022)) ([cited by 700+](https://scholar.google.com/scholar?cites=7452685526525981959))
By Dmitry Ulyanov, Andrea Vedaldi, Victor Lempitsky

**tl;dr** Fast Stylization with Instance Normalization for Real-time Image Generation.

**Github**: [github.com/DmitryUlyanov/texture_nets](https://github.com/DmitryUlyanov/texture_nets)

**Keywords**: instance normalization, neural style transfer

---

### 🌟 Learning to learn with backpropagation of Hebbian plasticity ([arXiv:1609.02228](https://arxiv.org/pdf/1609.02228)) ([cited by 3+](https://scholar.google.com/scholar?cites=13291537822698411280))
By [Thomas Miconi](https://scholar.google.ca/citations?hl=en&user=EXun8woAAAAJ)

**tl;dr** Backpropagation of Hebbian plasticity offers a powerful model for lifelong learning.

**Keywords**: meta-learning, Hebbian plasticity

---

### *WaveNet*: A Generative Model for Raw Audio ([arXiv:1609.03499](https://arxiv.org/pdf/1609.03499)) ([cited by 1000+](https://scholar.google.com/scholar?cites=18335624689534049212))
By Aaron van den Oord, Sander Dieleman, Heiga Zen, Karen Simonyan, Oriol Vinyals, Alex Graves, Nal Kalchbrenner, Andrew Senior, Koray Kavukcuoglu

**tl;dr** This paper introduces WaveNet, a deep neural network for generating raw audio waveforms for text-to-speech and music.

**Keywords**: speech synthesis, audio synthesis

---

### Photo-Realistic Single Image Super-Resolution Using a Generative Adversarial Network ([arXiv:1609.04802](https://arxiv.org/pdf/1609.04802)) ([cited by 3000+](https://scholar.google.com/scholar?cites=1219263946448760936))
By Christian Ledig, Lucas Theis, Ferenc Huszar, Jose Caballero, Andrew Cunningham, Alejandro Acosta, Andrew Aitken, Alykhan Tejani, Johannes Totz, Zehan Wang, Wenzhe Shi

**tl;dr** We propose SRGAN, a generative adversarial network for image super-resolution at 4x upscaling factors.

**Keywords**: GAN, image generation, super resolution

---

### Scaling Memory-Augmented Neural Networks with Sparse Reads and Writes ([arXiv:1610.09027](https://arxiv.org/pdf/1610.09027.pdf)) ([cited by 100+](https://scholar.google.com/scholar?cites=5913255713498338191))
By Jack W Rae, Jonathan J Hunt, Tim Harley, Ivo Danihelka, Andrew Senior, Greg Wayne, Alex Graves, Timothy P Lillicrap

**tl;dr** We present an end-to-end differentiable memory access scheme, which we call Sparse Access Memory (SAM), that retains the representational power of the original approaches whilst training efficiently with very large memories.

**Keywords**: SAM, MANN, memory

---

### Reinforcement Learning with Unsupervised Auxiliary Tasks ([arXiv:1611.05397](https://arxiv.org/pdf/1611.05397)) ([cited by 500+](https://scholar.google.com/scholar?cites=14888805482854497974))
By Max Jaderberg, Volodymyr Mnih, Wojciech Marian Czarnecki, Tom Schaul, Joel Z Leibo, David Silver, Koray Kavukcuoglu

**tl;dr** We introduce a novel reinforcement learning agent that maximises many other pseudo-reward functions simultaneously by reinforcement learning.

**Keywords**: RL, unsupervised learning

---

### Aggregated Residual Transformations for Deep Neural Networks ([arXiv:1611.05431](https://arxiv.org/pdf/1611.05431)) ([cited by 2000+](https://scholar.google.com/scholar?cites=11890020786646058356))
By Saining Xie, Ross Girshick, Piotr Dollár, Zhuowen Tu, Kaiming He

**tl;dr** We present a simple, highly modularized network architecture for image classification that has only a few hyper-parameters to set.

**Keywords**: image classification

---

### 🌟 Learning to reinforcement learn ([arXiv:1611.05763](https://arxiv.org/pdf/1611.05763.pdf)) ([cited by 350+](https://scholar.google.com/scholar?cites=2635837800726039837))
By Jane X Wang, Zeb Kurth-Nelson, Dhruva Tirumala, Hubert Soyer, Joel Z Leibo, Remi Munos, Charles Blundell, Dharshan Kumaran, Matt Botvinick

**tl;dr** Deep meta-reinforcement learning is a meta-learning approach to deep reinforcement learning that leverages structure in the training domain.

**Keywords**: reinforcement learning

---

### Image-to-Image Translation with Conditional Adversarial Networks ([arXiv:1611.07004](https://arxiv.org/pdf/1611.07004)) ([cited by 5000+](https://scholar.google.com/scholar?cites=16757839449706651543))
By Phillip Isola, Jun-Yan Zhu, Tinghui Zhou, Alexei A. Efros

**tl;dr** We investigate conditional adversarial networks as a general-purpose solution to image-to-image translation problems.

**Keywords**: img2img, image generation, GAN

---

### 🌟 *DNC*: Hybrid computing using a neural network with dynamic external memory ([paper](http://www.nature.com/articles/nature20101.epdf?author_access_token=ImTXBI8aWbYxYQ51Plys8NRgN0jAjWel9jnR3ZoTv0MggmpDmwljGswxVdeocYSurJ3hxupzWuRNeGvvXnoO8o4jTJcnAyhGuZzXJ1GEaD-Z7E6X_a9R-xqJ9TfJWBqz)) ([cited by 800+](https://scholar.google.com/scholar?cites=8100274942961380405))
By [Alex Graves](https://scholar.google.com/citations?user=DaFHynwAAAAJ), Greg Wayne, Malcolm Reynolds, Tim Harley, Ivo Danihelka, Agnieszka Grabska-Barwińska, Sergio Gómez Colmenarejo, Edward Grefenstette, Tiago Ramalho, John Agapiou, Adrià Puigdomènech Badia, Karl Moritz Hermann, Yori Zwols, Georg Ostrovski, Adam Cain, Helen King, Christopher Summerfield, Phil Blunsom, Koray Kavukcuoglu, Demis Hassabis

**tl;dr** A differentiable neural computer that can read from and write to an external memory matrix, analogous to the random-access memory in a conventional computer.

**Keywords**: DNC

---

### *StackGAN*: Text to Photo-realistic Image Synthesis with Stacked Generative Adversarial Networks ([arXiv:1612.03242](https://arxiv.org/pdf/1612.03242)) ([cited by 1000+](https://scholar.google.com/scholar?cites=17276767268002585863))
By Han Zhang, Tao Xu, Hongsheng Li, Shaoting Zhang, Xiaogang Wang, Xiaolei Huang, Dimitris Metaxas

**tl;dr** We propose Stacked Generative Adversarial Networks (StackGAN) to generate 256x256 photo-realistic images conditioned on text descriptions.

**Keywords**: StackGAN, GAN, text2img

## 2017

### *AdaIN*: Arbitrary Style Transfer in Real-time with Adaptive Instance Normalization ([arXiv:1703.06868](https://arxiv.org/pdf/1703.06868.pdf)) ([cited by 500+](https://scholar.google.com/scholar?cites=6462913724934880335))
By Xun Huang, Serge Belongie

**tl;dr** We present a novel adaptive instance normalization layer that aligns the mean and variance of the content features with those of the style features.

**Keywords**: AdaIN, neural style transfer

---

### Improved Texture Networks: Maximizing Quality and Diversity in Feed-forward Stylization and Texture Synthesis ([arXiv:1701.02096](https://arxiv.org/pdf/1701.02096)) ([cited by 200+](https://scholar.google.com/scholar?cites=15121556168732029899))
By Dmitry Ulyanov, Andrea Vedaldi, Victor Lempitsky

**tl;dr** Improve texture generation and image stylization by introducing a new learning formulation that encourages generators to sample unbiasedly from the Julesz texture ensemble.

**Keywords**: CNN, instance normalization, neural style transfer

---

### Unsupervised Image-to-Image Translation Networks ([arXiv:1703.00848](https://arxiv.org/pdf/1703.00848)) ([cited by 900+](https://scholar.google.com/scholar?cites=14169741715291172305))
By Ming-Yu Liu, Thomas Breuel, Jan Kautz

**tl;dr** We propose an unsupervised image-to-image translation framework based on Coupled GANs.

Keywords: GAN, VAE, VAE-GAN, img2img

---

### Intrinsic Motivation and Automatic Curricula via Asymmetric Self-Play ([arXiv:https://arxiv.org/pdf/1703.05407](https://arxiv.org/pdf/1703.05407)) ([cited by 100+](https://scholar.google.com/scholar?cites=4160913521858709316))
By Sainbayar Sukhbaatar, Zeming Lin, Ilya Kostrikov, Gabriel Synnaeve, Arthur Szlam, Rob Fergus

**tl;dr** We describe a simple scheme that allows an agent to learn about its environment in an unsupervised manner.

**Keywords**: artificial curiosity, RL, self-play

---

### Tacotron: Towards End-to-End Speech Synthesis ([arXiv:1703.10135](https://arxiv.org/pdf/1703.10135.pdf) ([cited 500+](https://scholar.google.ca/scholar?cites=11291739830353377265))
By Yuxuan Wang, RJ Skerry-Ryan, Daisy Stanton, Yonghui Wu, Ron J. Weiss, Navdeep Jaitly, Zongheng Yang, Ying Xiao, Zhifeng Chen, Samy Bengio, Quoc Le, Yannis Agiomyrgiannakis, Rob Clark, Rif A. Saurous

**tl;dr** Tacotron is a generative text-to-speech model that synthesizes speech directly from characters.

**Keywords**: speech synthesis

---

### *CycleGAN*: Unpaired Image-to-Image Translation using Cycle-Consistent Adversarial Networks ([arXiv:1703.10593](https://arxiv.org/pdf/1703.10593)) ([cited by 4000+](https://scholar.google.com/scholar?cites=18396328236259959400))

**tl;dr** Learning to translate an image from a source domain X to a target domain Y in the absence of paired examples using an adversarial loss.

**Keywords**: CycleGAN, GAN, img2img, style transfer

---

### MobileNets: Efficient Convolutional Neural Networks for Mobile Vision Applications ([arXiv:1704.04861](https://arxiv.org/pdf/1704.04861)) ([cited by 4000+](https://scholar.google.com/scholar?cites=5758617088745811384))
By Andrew G. Howard, Menglong Zhu, Bo Chen, Dmitry Kalenichenko, Weijun Wang, Tobias Weyand, Marco Andreetto, Hartwig Adam

**tl;dr** We present a class of efficient models called MobileNets for mobile and embedded vision applications.

**Keywords**: efficiency

---

### Curiosity-driven Exploration by Self-supervised Prediction ([arXiv:1705.05363](https://arxiv.org/pdf/1705.05363)) ([cited by 500+](https://scholar.google.com/scholar?cites=9379743003299559904))
By Deepak Pathak, Pulkit Agrawal, Alexei A. Efros, Trevor Darrell

**tl;dr** We formulate curiosity as an error in a visual feature space learned by a self-supervised inverse dynamics model.

**Source code**: [pathak22.github.io/noreward-rl/](https://pathak22.github.io/noreward-rl/)

**Keywords**: RL, artificial curiosity

---

### Universal Style Transfer via Feature Transforms ([arXiv:1705.08086](https://arxiv.org/pdf/1705.08086)) ([cited by 200+](https://scholar.google.com/scholar?cites=7001062204457348357))
By Yijun Li, Chen Fang, Jimei Yang, Zhaowen Wang, Xin Lu, Ming-Hsuan Yang

**tl;dr** We present a simple yet effective method that tackles these limitations without training on any pre-defined styles.

**Keywords**: neural style transfer

---

### *SELU*: Self-Normalizing Neural Networks ([arXiv:1706.02515](https://arxiv.org/pdf/1706.02515)) ([cited by 800+](https://scholar.google.com/scholar?cites=3659160383490046744))
By Günter Klambauer, Thomas Unterthiner, Andreas Mayr, [Sepp Hochreiter](https://scholar.google.com/citations?user=tvUH3WMAAAAJ)

**tl;dr** We introduce self-normalizing neural networks (SNNs) to enable high-level abstract representations.

**Source code**: [github.com/bioinf-jku/SNNs](https://github.com/bioinf-jku/SNNs)

**Keywords**: normalization

---

### Attention Is All You Need ([arXiv:1706.03762](https://arxiv.org/pdf/1706.03762)) ([cited by 7000+](https://scholar.google.com/scholar?cites=2960712678066186980))
By Ashish Vaswani, Noam Shazeer, Niki Parmar, Jakob Uszkoreit, Llion Jones, Aidan N. Gomez, Lukasz Kaiser, Illia Polosukhin<br>

**tl;dr** We propose a new simple network architecture, the Transformer, based solely on attention mechanisms, dispensing with recurrence and convolutions entirely.

**Keywords**: NLP, transformer

---

### *ShuffleNet*: An Extremely Efficient Convolutional Neural Network for Mobile Devices ([arXiv:1707.01083](https://arxiv.org/pdf/1707.01083)) ([cited by 1000+](https://scholar.google.com/scholar?cites=6469340744827368429))
By Xiangyu Zhang, Xinyu Zhou, Mengxiao Lin, Jian Sun

**tl;dr** We introduce an extremely computation-efficient CNN architecture named ShuffleNet, which is designed specially for mobile devices with very limited computing power.

**Keywords**: CNN

---

### *HER*: Hindsight Experience Replay ([arXiv:1707.01495](https://arxiv.org/pdf/1707.01495)) ([cited by 500+](https://scholar.google.com/scholar?cites=14733084267697271284))

**tl;dr** We present a novel technique called Hindsight Experience Replay which allows sample-efficient learning from rewards which are sparse and binary and therefore avoid the need for complicated reward engineering.

**Demo**: [Video](https://www.youtube.com/watch?v=Dz_HuzgMxzo)

**Summaries**

+ [Video by Two Minute Papers](https://www.youtube.com/watch?v=Dvd1jQe3pq0)
+ [Video by ArXiv Insights](https://www.youtube.com/watch?v=0Ey02HT_1Ho)

**Explanation**: [Video by Olivier Siguad](https://www.youtube.com/watch?v=77xkqEAsHFI)

**Keywords**: RL, experience replay

---

### *PPO*: Proximal Policy Optimization Algorithms ([arXiv:1707.06347](https://arxiv.org/pdf/1707.06347)) ([cited by 2000+](https://scholar.google.com/scholar?cites=2664197784944153194))
By John Schulman, Filip Wolski, Prafulla Dhariwal, Alec Radford, Oleg Klimov

**tl;dr** We propose a new family of policy gradient methods for reinforcement learning that alternate between sampling data through interaction with the environment, and optimizing a "surrogate" objective function using stochastic gradient ascent. 

**Keywords**: PPO, RL

---

### *SE*: Squeeze-and-Excitation Networks ([arXiv:1709.01507](https://arxiv.org/pdf/1709.01507)) ([cited by 2000+](https://scholar.google.com/scholar?cites=11424287065250598243))
By Jie Hu, Li Shen, Samuel Albanie, Gang Sun, Enhua Wu

**tl;dr** We propose a novel architectural unit that adaptively recalibrates channel-wise feature responses by explicitly modelling interdependencies between channels.

**Source code**: [github.com/hujie-frank/SENet](https://github.com/hujie-frank/SENet)

**Keywords**: CNN

---

### Progressive growing of gans for improved quality, stability, and variation ([arXiv:1710.10196](https://arxiv.org/pdf/1710.10196)) ([cited by 1000+](https://scholar.google.com/scholar?cites=11486098150916361186))
By Tero Karras, Timo Aila, Samuli Laine, Jaakko Lehtinen

**tl;dr** We describe a new training methodology for generative adversarial networks that add new layers that model increasingly fine details as training progresses.

**Keywords**: GAN, image generation

---

### *AlphaGo Zero*: Mastering the game of Go without human knowledge ([paper](https://www.nature.com/articles/nature24270)) ([cited by 3000+](https://scholar.google.com/scholar?cites=7419430260981373186))
By [David Silver](https://scholar.google.com/citations?user=-8DNE4UAAAAJ), Julian Schrittwieser, Karen Simonyan, Ioannis Antonoglou, Aja Huang, Arthur Guez, Thomas Hubert, Lucas Baker, Matthew Lai, Adrian Bolton, Yutian Chen, [Timothy Lillicrap](https://scholar.google.ca/citations?user=htPVdRMAAAAJ), Fan Hui, Laurent Sifre, George van den Driessche, Thore Graepel, Demis Hassabis

**tl;dr** A long-standing goal of artificial intelligence is an algorithm that learns, tabula rasa, superhuman proficiency in challenging domains.

**Publication**: [deepmind.com](https://deepmind.com/blog/alphago-zero-learning-scratch/)

**How it works (summary)**: [YouTube](https://www.youtube.com/watch?v=MgowR4pq3e8)

**How it works**: [YouTube](https://www.youtube.com/watch?v=XuzIqE2IshY)

**Keywords**: RL, self-play, MCTS

---

### *CapsuleNet*: Dynamic Routing Between Capsules ([arXiv:1710.09829](https://arxiv.org/pdf/1710.09829)) ([cited by 1000+](https://scholar.google.com/scholar?cites=5914955692202761908))
By Sara Sabour, Nicholas Frosst, [Geoffrey E Hinton](https://scholar.google.ca/citations?hl=en&user=JicYPdAAAAAJ)

**tl;dr** We show that a multi-layer capsule system achieves state-of-the-art performance on MNIST.

**Explanation**: [YouTube](https://www.youtube.com/watch?v=pPN8d0E3900)

**Keywords**: CapsuleNet, network architecture

---

### *StackGAN++*: Realistic Image Synthesis with Stacked Generative Adversarial Networks ([arXiv:1710.10916](https://arxiv.org/pdf/1710.10916)) ([cited by 200+](https://scholar.google.com/scholar?cites=5251688290326540457))
By Han Zhang, Tao Xu, Hongsheng Li, Shaoting Zhang, Xiaogang Wang, Xiaolei Huang, Dimitris Metaxas

**tl;dr** StackGAN: Stacked Generative Adversarial Networks for Photo-Realistic Image Generation

**Keywords**: StackGAN, GAN, text2img

---

### *MoS*: Breaking the Softmax Bottleneck: A High-Rank RNN Language Model ([arXiv:1711.03953](https://arxiv.org/pdf/1711.03953)) ([cited by 100+](https://scholar.google.com/scholar?cites=15538946355362697879))
By Zhilin Yang, Zihang Dai, Ruslan Salakhutdinov, William W. Cohen

**tl;dr** We formulate language modeling as a matrix factorization problem, and show that the expressiveness of Softmax-based models (including the majority of neural language models) is limited by a Softmax bottleneck.

**Keywords**: NLP, RNN, activation function

---

### Natural TTS Synthesis by Conditioning WaveNet on Mel Spectrogram Predictions
By Jonathan Shen, Ruoming Pang, Ron J. Weiss, Mike Schuster, Navdeep Jaitly, Zongheng Yang, Zhifeng Chen, Yu Zhang, Yuxuan Wang, RJ Skerry-Ryan, Rif A. Saurous, Yannis Agiomyrgiannakis, Yonghui Wu

**tl;dr** Tacotron 2: A neural network architecture for speech synthesis directly from text.

---

### Open-endedness: The last grand challenge you’ve never heard of ([article](https://www.oreilly.com/radar/open-endedness-the-last-grand-challenge-youve-never-heard-of/))

**tl;dr** Artificial intelligence is a grand challenge for computer science, but it’s something that will take astronomical effort over expansive time to achieve.

---

### A generative vision model that trains with high data efficiency and breaks text-based CAPTCHAs ([pdf](http://www.academia.edu/download/55648400/science.aag2612.full.pdf))
By D. George, W. Lehrach, K. Kansky, M. Lázaro-Gredilla, C. Laan, B. Marthi, X. Lou, Z. Meng, Y. Liu, H. Wang,
A. Lavin, D. S. Phoenix

**tl;dr** Learning from few examples and generalizing to dramatically different situations are capabilities of human visual intelligence that are yet to be matched by leading machine learning models.

**Keywords**: computer vision

## 2018

### Innateness, AlphaZero, and Artificial Intelligence ([arXiv:1801.05667](https://arxiv.org/pdf/1801.05667)) ([cited by 30+](https://scholar.google.com/scholar?cites=13839738697223328361))
By Gary Marcus

**tl;dr** Artificial intelligence needs greater attention to innateness.

**Keywords**: biological learning

---

### Soft Actor-Critic: Off-Policy Maximum Entropy Deep Reinforcement Learning with a Stochastic Actor ([arXiv:1801.01290](https://arxiv.org/pdf/1801.01290)) ([cited by 500+](https://scholar.google.com/scholar?cites=13282174879342015249))
By Tuomas Haarnoja, Aurick Zhou, Pieter Abbeel, Sergey Levine

**tl;dr** We propose soft actor-critic, an off-policy actor-Critic deep RL algorithm based on the maximum entropy reinforcement learning framework.

**Keywords**: A3C, RL

---

### *MVAE*: Multimodal Generative Models for Scalable Weakly-Supervised Learning ([arXiv:1802.05335](https://arxiv.org/pdf/1802.05335)) ([cited by 33+](https://scholar.google.com/scholar?cites=3003516605424055081))
By Mike Wu, Noah Goodman

**tl;dr** A multimodal variational autoencoder that uses a product-of-experts inference network and a sub-sampled training paradigm.

as a set of modalities, followed by one of machine translation between two languages. We find appealing results across this range of tasks.

**Keywords**: VAE

---

### Addressing Function Approximation Error in Actor-Critic Methods ([arXiv:1802.09477](https://arxiv.org/pdf/1802.09477)) ([cited by 300+](https://scholar.google.com/scholar?cites=2930747733592680111))
By Scott Fujimoto, Herke van Hoof, David Meger

**tl;dr** We show that function approximation errors persist in an actor-critic setting and propose novel mechanisms to minimize its effects on both the actor and the critic.

**Keywords**: A3C, reinforcement learning

---

### Diversity is All You Need: Learning Skills without a Reward Function ([arXiv:1802.06070](https://arxiv.org/pdf/1802.06070)) ([cited by 100+](https://scholar.google.com/scholar?cites=12324439663284457782))
By Benjamin Eysenbach, Abhishek Gupta, Julian Ibarz, Sergey Levine

**tl;dr** We propose DIAYN ('Diversity is All You Need'), a method for learning useful skills without a reward function.

**Keywords**: RL, artificial curiosity

---

### Investigating Human Priors for Playing Video Games ([arXiv:1802.10217](https://arxiv.org/pdf/1802.10217)) ([cited by 53+](https://scholar.google.com/scholar?cites=2202192690517876762))
By Rachit Dubey, Pulkit Agrawal, Deepak Pathak, Thomas L. Griffiths, Alexei A. Efros

**tl;dr** What makes humans so good at solving video games?

**Website**: [rach0012.github.io/humanRL_website/](https://rach0012.github.io/humanRL_website/)

**Keywords**: RL, priors, biological learning

---

### 🌟 *MERLIN*: Unsupervised Predictive Memory in a Goal-Directed Agent ([arXiv:1803.10760](https://arxiv.org/pdf/1803.10760)) ([cited by 70+](https://scholar.google.com/scholar?cites=1449593791259825848))
By Greg Wayne, Chia-Chun Hung, David Amos, Mehdi Mirza, Arun Ahuja, Agnieszka Grabska-Barwinska, Jack Rae, Piotr Mirowski, Joel Z. Leibo, Adam Santoro, Mevlana Gemici, Malcolm Reynolds, Tim Harley, Josh Abramson, Shakir Mohamed, Danilo Rezende, David Saxton, Adam Cain, Chloe Hillier, [David Silver](https://scholar.google.com/citations?user=-8DNE4UAAAAJ), Koray Kavukcuoglu, Matt Botvinick, Demis Hassabis, [Timothy Lillicrap](https://scholar.google.ca/citations?user=htPVdRMAAAAJ)

**tl;dr** We show that memory is not enough; it is critical that the right information be stored in the right format. We develop a model, the Memory, RL, and Inference Network (MERLIN), in which memory formation is guided by a process of predictive modeling, to solve canonical behavioural tasks in psychology and neurobiology.

**Keywords**: RL, MANN

**Video Links**

* [https://youtu.be/YFx-D4eEs5A](https://youtu.be/YFx-D4eEs5A)
* [https://youtu.be/IiR_NOomcpk](https://youtu.be/IiR_NOomcpk)
* [https://youtu.be/dQMKJtLScmk](https://youtu.be/dQMKJtLScmk)
* [https://youtu.be/xrYDlTXyC6Q](https://youtu.be/xrYDlTXyC6Q)
* [https://youtu.be/04H28-qA3f8](https://youtu.be/04H28-qA3f8)
* [https://youtu.be/3iA19h0Vvq0](https://youtu.be/3iA19h0Vvq0)

---

### The Kanerva Machine: A Generative Distributed Memory ([arXiv:1804.01756](https://arxiv.org/pdf/1804.01756)) ([cited by 10+](https://scholar.google.com/scholar?cites=9888262262485457347))
By Yan Wu, Greg Wayne, [Alex Graves](https://scholar.google.com/citations?user=DaFHynwAAAAJ), [Timothy Lillicrap](https://scholar.google.ca/citations?user=htPVdRMAAAAJ)

**tl;dr** We present an end-to-end trained memory system that quickly adapts to new data and generates samples like them.

**Jupyter Notebook (PyTorch)**: [github.com/Kajiyu/kanerva_machine](https://nbviewer.jupyter.org/github/Kajiyu/kanerva_machine/blob/master/notebook/kanerva_machine_pytorch.ipynb)

**Keywords**: Bayesian, MANN, memory model, one-shot learning

---

### 🌟 Differentiable plasticity: training plastic neural networks with backpropagation ([arXiv:1804.02464](https://arxiv.org/pdf/1804.02464)) ([cited by 40+](https://scholar.google.com/scholar?cites=16849084099727983459))
By [Thomas Miconi](https://scholar.google.ca/citations?hl=en&user=EXun8woAAAAJ), [Jeff Clune](https://scholar.google.ca/citations?hl=en&user=5TZ7f5wAAAAJ), [Kenneth O. Stanley](https://scholar.google.ca/citations?user=6Q6oO1MAAAAJ)

**tl;dr** We show that plasticity, just like connection weights, can be optimized by gradient descent in large recurrent networks with Hebbian plastic connections.

**Keywords**: meta-learning, Hebbian plasticity

---

### Deep Reinforcement Learning for Playing 2.5D Fighting Games ([arXiv:1805.02070](https://arxiv.org/pdf/1805.02070)) ([cited by 40+](https://scholar.google.com/scholar?cites=3298968977177896332))
By Yu-Jhe Li, Hsin-Yu Chang, Yu-Jing Lin, Po-Wei Wu, Yu-Chiang Frank Wang

**tl;dr** We present a novel A3C+ network for learning RL agents for 2.5D fighting games.

**Keywords**: RL, A3C

---

### World Models ([arXiv:1803.10122](https://arxiv.org/pdf/1803.10122)) ([cited by 200+](https://scholar.google.com/scholar?cites=8020027393506054346))
By David Ha, [Jürgen Schmidhuber](https://scholar.google.com/citations?user=gLnCTgIAAAAJ)

**tl;dr** We explore building generative neural network models of popular reinforcement learning environments to learn a compressed spatial and temporal representation of the environment.

**Interactive paper**: [worldmodels.github.io](https://worldmodels.github.io/)

**Keywords**: RL, model-based

---

### Binary Ensemble Neural Network: More Bits per Network or More Networks per Bit? ([arXiv:1806.07550](https://arxiv.org/pdf/1806.07550)) ([cited by 30+](https://scholar.google.com/scholar?cites=17391681667773276447))
By Shilin Zhu, Xin Dong, Hao Su

**tl;dr** We propose the Binary Ensemble Neural Network (BENN) to improve the performance of BNNs with limited efficiency cost.

**Keywords**: network architecture, compression

---

### Recurrent World Models Facilitate Policy Evolution ([arXiv:1809.01999](https://arxiv.org/pdf/1809.01999)) ([cited by 100+](https://scholar.google.com/scholar?cites=15327043406005665564))
By David Ha, [Jürgen Schmidhuber](https://scholar.google.com/citations?user=gLnCTgIAAAAJ)

**tl;dr** A generative recurrent neural network is quickly trained to model popular reinforcement learning environments through compressed spatio-temporal representations.

**Interactive paper**: https://worldmodels.github.io/

**Keywords**: RL, RNN

---

### Neural Approaches to Conversational AI ([arXiv:1809.08267](https://arxiv.org/pdf/1809.08267)) ([cited by 100+](https://scholar.google.com/scholar?cites=11698047246691750774))
By Jianfeng Gao, Michel Galley, Lihong Li

**tl;dr** Neural approaches to conversational AI: A review and comparison.

**Keywords**: NLP, chatbot

---

### 🌟 BERT: Pre-training of Deep Bidirectional Transformers for Language Understanding ([arXiv:1810.04805](https://arxiv.org/abs/1810.04805)) ([cited by 10,000+](https://scholar.google.com/scholar?cites=3166990653379142174))
By Jacob Devlin, Ming-Wei Chang, Kenton Lee, Kristina Toutanova

**tl;dr** We introduce a new language representation model called BERT, which stands for Bidirectional Encoder Representations from Transformers.

**Keywords**: language model

---

### 🌟 Dreaming neural networks: forgetting spurious memories and reinforcing pure ones ([arXiv:1810.12217](https://arxiv.org/pdf/1810.12217)) ([cited by 10+](https://scholar.google.com/scholar?cites=9537827505537532968))
By Alberto Fachechi, Elena Agliari, Adriano Barra

**tl;dr** We propose an extension of the Hopfield model for associative neural networks that saturates the theoretical bound for symmetric networks.

**Keywords**: Hopfield networks, dreaming, network pruning

---

### *WaveGlow*: A Flow-based Generative Network for Speech Synthesis ([arXiv:1811.00002](https://arxiv.org/pdf/1811.00002.pdf)) ([cited by 200+](https://scholar.google.ca/scholar?cites=16743544780840044925))
By Ryan Prenger, Rafael Valle, Bryan Catanzaro

**tl;dr** WaveGlow: a flow-based network capable of generating high quality speech from mel-spectrograms.

**Keywords**: speech synthesis

---

### ACE: An Actor Ensemble Algorithm for Continuous Control with Tree Search ([arXiv:1811.02696](https://arxiv.org/pdf/1811.02696)) ([cited by 5+](https://scholar.google.com/scholar?cites=12478862000634249438))
By Shangtong Zhang, Hao Chen, Hengshuai Yao

**tl;dr** We propose an actor ensemble algorithm for continuous control with a deterministic policy in reinforcement learning.

**Keywords**: 

---

### Learning Attractor Dynamics for Generative Memory ([arXiv:1811.09556](https://arxiv.org/pdf/1811.09556)) ([cited by 8+](https://scholar.google.com/scholar?cites=9940290258944118765))
By Yan Wu, Greg Wayne, Karol Gregor, [Timothy Lillicrap](https://scholar.google.ca/citations?user=htPVdRMAAAAJ)

**tl;dr** We train a generative distributed memory without simulating the attractor dynamics, which improves robust retrieval in the presence of interference due to other stored patterns.

**Keywords**: MANN, Kanerva machine, one-shot learning

**Source code (Tensorflow)**: [github.com/deepmind/dynamic-kanerva-machines](https://github.com/deepmind/dynamic-kanerva-machines)

---

### A Style-Based Generator Architecture for Generative Adversarial Networks ([paper](https://www.researchgate.net/profile/Timo_Aila/publication/329607896_A_Style-Based_Generator_Architecture_for_Generative_Adversarial_Networks/links/5cff879692851c874c5db5f0/A-Style-Based-Generator-Architecture-for-Generative-Adversarial-Networks.pdf)) ([cited by 700+](https://scholar.google.com/scholar?cites=9092299839549248053))
By Tero Karras, Samuli Laine, Timo Aila

**tl;dr** We propose an alternative generator architecture for generative adversarial networks, borrowing from style transfer literature.

**Keywords**: GAN, image generation

---

### Priors in Animal and Artificial Intelligence: Where Does Learning Begin? ([paper](https://r.unitn.it/filesresearch/images/cimec-abc/Publications/2018/versace_et_al._trend_cogn_sci_2018.pdf)) ([cited by 30+](https://scholar.google.com/scholar?cites=5763478379456668774))
By Elisabetta Versace, Antone Martinho-Truswell, Alex Kacelnik, and Giorgio Vallortigara

**tl;dr** A major goal for the next generation of artificial intelligence is to build machines that are able to reason and cope with novel tasks, environments, and situations.

**Keywords**: biological learning

---

### *AlphaZero*: A general reinforcement learning algorithm that masters chess, shogi, and Go through self-play ([paper](http://science.sciencemag.org/cgi/content/full/362/6419/1140?ijkey=XGd77kI6W4rSc&keytype=ref&siteid=sci)) ([cited by 500+](https://scholar.google.com/scholar?cites=13089619183937314101))
By [David Silver](https://scholar.google.com/citations?user=-8DNE4UAAAAJ), Thomas Hubert, Julian Schrittwieser, Ioannis Antonoglou, Matthew Lai, Arthur Guez, Marc Lanctot, Laurent Sifre, Dharshan Kumaran, Thore Graepel, [Timothy Lillicrap](https://scholar.google.ca/citations?user=htPVdRMAAAAJ), Karen Simonyan, Demis Hassabis

**tl;dr** We generalize the AlphaGo Zero program and achieve superhuman performance in the games of chess, shogi, and Go.

**Publication**: [deepmind.com](https://deepmind.com/blog/alphazero-shedding-new-light-grand-games-chess-shogi-and-go/)

**Early paper**: [arXiv:1712.01815](https://arxiv.org/pdf/1712.01815)

**How it works (in-depth)**: [YouTube](https://www.youtube.com/watch?v=_Z31-5D3RZg)

**Tutorial**: [web.stanford.edu](https://web.stanford.edu/~surag/posts/alphazero.html)

**Keywords**: RL, MCTS, self-play

## 2019

### Designing neural networks through neuroevolution ([paper](https://www.gwern.net/docs/rl/2019-stanley.pdf)) ([cited by 80+](https://scholar.google.com/scholar?cites=5962020871608022062))
By [Kenneth O. Stanley](https://scholar.google.ca/citations?user=6Q6oO1MAAAAJ), [Jeff Clune](https://scholar.google.ca/citations?hl=en&user=5TZ7f5wAAAAJ), Joel Lehman and Risto Miikkulainen

**tl;dr** This Review looks at several key aspects of modern neuroevolution, including large-scale computing, the benefits of novelty and diversity, the power of indirect encoding, and the field’s contributions to meta-learning and architecture search.

**Keywords**: meta-learning, architecture search

---

### Paired Open-Ended Trailblazer (POET): Endlessly Generating Increasingly Complex and Diverse Learning Environments and Their Solutions ([arXiv:1901.01753](https://arxiv.org/pdf/1901.01753)) ([cited by 30+](https://scholar.google.com/scholar?cites=11725210653398039148))
By Rui Wang, Joel Lehman, [Jeff Clune](https://scholar.google.ca/citations?hl=en&user=5TZ7f5wAAAAJ), [Kenneth O. Stanley](https://scholar.google.ca/citations?user=6Q6oO1MAAAAJ)

**tl;dr** The Paired Open-Ended Trailblazer (POET) algorithm introduced in this paper explores many different paths through the space of possible problems.

**Keywords**: neuroevolution

---

### *Transformer-XL*: Attentive Language Models Beyond a Fixed-Length Context ([arXiv:1901.02860](https://arxiv.org/pdf/1901.02860)) ([cited by 300+](https://scholar.google.com/scholar?cites=7150055013029036741))
By Zihang Dai, Zhilin Yang, Yiming Yang, Jaime Carbonell, Quoc V. Le, Ruslan Salakhutdinov

**tl;dr** We propose a novel neural architecture Transformer-XL that enables learning longer-term dependency beyond a fixed length without disrupting temporal coherence.

---

### 🌟 *Go-Explore*: a New Approach for Hard-Exploration Problems ([arXiv:1901.10995](https://arxiv.org/pdf/1901.10995)) ([cited by 60+](https://scholar.google.com/scholar?cites=4261026080332318785))
By Adrien Ecoffet, Joost Huizinga, Joel Lehman, [Kenneth O. Stanley](https://scholar.google.ca/citations?user=6Q6oO1MAAAAJ), [Jeff Clune](https://scholar.google.ca/citations?hl=en&user=5TZ7f5wAAAAJ)

**tl;dr** We introduce a new RL algorithm that exploits the following principles: (1) remember previously visited states, (2) first return to a promising state, then explore from it, and (3) solve simulated environments through any available means, then robustify via imitation learning.

![Detachment and derailment](https://i.imgur.com/B6ig7ul.png)

![A high-level overview of the Go-Explore algorithm](https://i.imgur.com/oF5p3mS.png)

**Keywords**: RL, exploration, curiosity

---

### Model-Based Reinforcement Learning for Atari ([arXiv:1903.00374](https://arxiv.org/pdf/1903.00374)) ([cited by 80+](https://scholar.google.com/scholar?cites=4841006515344388997))
By Lukasz Kaiser, Mohammad Babaeizadeh, Piotr Milos, Blazej Osinski, Roy H Campbell, Konrad Czechowski, Dumitru Erhan, Chelsea Finn, Piotr Kozakowski, Sergey Levine, Afroz Mohiuddin, Ryan Sepassi, George Tucker, Henryk Michalewski

**tl;dr** Simulated Policy Learning: A Deep RL Algorithm based on Video Prediction Models

**Keywords**: RL

---

### MSG-GAN: Multi-Scale Gradients for Generative Adversarial Networks ([arXiv:1903.06048](https://arxiv.org/pdf/1903.06048.pdf)) ([cited by 10+](https://scholar.google.com/scholar?cites=11292105841871558486))
By Animesh Karnewar, Oliver Wang

**tl;dr** Multi-Scale Gradient Generative Adversarial Networks for High Resolution Image Synthesis

**Keywords**: GAN, high resolution image generation

---

### *SPADE*: Semantic Image Synthesis with Spatially-Adaptive Normalization ([arXiv:1903.07291](https://arxiv.org/pdf/1903.07291)) ([cited by 400+](https://scholar.google.com/scholar?cites=12479535951654162053))
By Taesung Park, Ming-Yu Liu, Ting-Chun Wang, Jun-Yan Zhu

**tl;dr** We propose spatially-adaptive normalization, a simple but effective layer for synthesizing photorealistic images given an input semantic layout.

**Interactive Demo**: [www.nvidia.com/en-us/research/ai-playground/](https://www.nvidia.com/en-us/research/ai-playground/)

**Video demos**

* [GauGAN: Changing Sketches into Photorealistic Masterpieces](https://www.youtube.com/watch?v=p5U4NgVGAwg)
* [Interactive Demo App "GauGAN", built using SPADE](https://youtu.be/MXWm6w4E5q0)

**Code**: [https://github.com/NVlabs/SPADE](https://github.com/NVlabs/SPADE)

**Keywords**: normalization, style transfer, image generation, GAN

---

### Regularizing Trajectory Optimization with Denoising Autoencoders ([arXiv:1903.11981](https://arxiv.org/pdf/1903.11981)) ([cited by 1+](https://scholar.google.com/scholar?cites=31624080029180005))
By Rinu Boney, Norman Di Palo, Mathias Berglund, Alexander Ilin, Juho Kannala, Antti Rasmus, Harri Valpola

**tl;dr** We propose to regularize trajectory optimization by means of a denoising autoencoder that is trained on the same trajectories as the model.

**Keywords**: regularization, RL, planning

**Explanation**: [Video by Yannic Kilcher](https://www.youtube.com/watch?v=UjJU13GdL94)

---

### Generating Long Sequences with Sparse Transformers ([arXiv:1904.10509](https://arxiv.org/pdf/1904.10509)) ([cited by 60+](https://scholar.google.com/scholar?cites=15804183398893801414))
By Rewon Child, Scott Gray, Alec Radford, [Ilya Sutskever](https://scholar.google.ca/citations?user=x04W_mMAAAAJ)

**tl;dr** Sparse attention matrices, fast attention kernels, and fast kernels for training.

**Keywords**: transformers

---

### *XLNet*: Generalized Autoregressive Pretraining for Language Understanding ([arXiv:1906.08237](https://arxiv.org/pdf/1906.08237)) ([cited by 500+](https://scholar.google.com/scholar?cites=14487406216105917109))
By Zhilin Yang, Zihang Dai, Yiming Yang, Jaime Carbonell, Ruslan Salakhutdinov, Quoc V. Le

**tl;dr** We propose XLNet, a generalized autoregressive pretraining method that enables learning bidirectional contexts by maximizing the expected likelihood over all permutations.

**Keywords**: NLP, transformer

---

### 🌟 Large Memory Layers with Product Keys ([arXiv:1907.05242](https://arxiv.org/pdf/1907.05242)) ([cited by 10+](https://scholar.google.com/scholar?cites=8134570978766877507))
By Guillaume Lample, Alexandre Sablayrolles, Marc'Aurelio Ranzato, Ludovic Denoyer, Hervé Jégou

**tl;dr** This paper introduces a structured memory which can be easily integrated into a neural network, by up to a billion parameters with a negligible computational overhead.

**Source code**: [github.com/facebookresearch/XLM/blob/master/src/model/memory/memory.py](https://github.com/facebookresearch/XLM/blob/master/src/model/memory/memory.py)

**Keywords**: approximate k-nearest neighbor search

---

### Backpropagation through time and the brain ([paper](https://www.sciencedirect.com/science/article/pii/S0959438818302009)) ([cited by 20+](https://scholar.google.com/scholar?cites=14033049240084634707))
By [Timothy P. Lillicrap](https://scholar.google.ca/citations?user=htPVdRMAAAAJ), Adam Santoro

**tl;dr** It has long been speculated that the backpropagation-of-error algorithm may be a model of how the brain learns.

**Keywords**: BPTT, TCA, RNN, biological learning

---

### 🌟 ALBERT: A Lite BERT for Self-supervised Learning of Language Representations ([arXiv:1909.11942](https://arxiv.org/abs/1909.11942)) ([cited by 500+](https://scholar.google.com/scholar?cites=6606720413006378435))
By Zhenzhong Lan, Mingda Chen, Sebastian Goodman, Kevin Gimpel, Piyush Sharma, Radu Soricut

**tl;dr** We present two parameter-reduction techniques to lower memory consumption and increase the training speed of BERT.

**Keywords**: language model, NLP

---

### 🌟 *MIM*: High Mutual Information in Representation Learning with Symmetric Variational Inference ([arXiv:1910.04153](https://arxiv.org/pdf/1910.04153)) ([waiting for citations?<sup>*</sup>](https://scholar.google.com/scholar?cites=12274618158995019067))
By [Micha Livne](https://scholar.google.com/citations?user=Cmm8ZugAAAAJ), [Kevin Swersky](https://scholar.google.com/citations?hl=en&user=IrixA8MAAAAJ), [David J. Fleet](https://scholar.google.com/citations?hl=en&user=njOmQFsAAAAJ)

**tl;dr** We introduce the Mutual Information Machine (MIM), a novel formulation of representation learning, using a joint distribution over the observations and latent state in an encoder/decoder framework.

**Keywords**: VAE, JSD

*: _Already cited by SentenceMIM_ ([arXiv:2003.02645](https://arxiv.org/pdf/2003.02645))

---

### Regularizing Model-Based Planning with Energy-Based Models ([arXiv:1910.05527](https://arxiv.org/pdf/1910.05527)) ([cited by 7+](https://scholar.google.com/scholar?cites=17518653031307172157))
By Rinu Boney, Juho Kannala, Alexander Ilin

**tl;dr** We propose to regularize planning with learned dynamics models using energy estimates of state transitions in the environment.

**Keywords**: regularization, RL, planning

---

### BART: Denoising Sequence-to-Sequence Pre-training for Natural Language Generation, Translation, and Comprehension ([arXiv:1910.13461](https://arxiv.org/abs/1910.13461)) ([cited by 190+](https://scholar.google.com/scholar?cites=10589398302527104823))
By Mike Lewis, Yinhan Liu, Naman Goyal, Marjan Ghazvininejad, Abdelrahman Mohamed, Omer Levy, Ves Stoyanov, Luke Zettlemoyer

**tl;dr** We present BART, a denoising autoencoder for pretraining sequence-to-sequence models.

**Keywords**: language model, NLP

---

### 🌟 *MuZero*: Mastering Atari, Go, Chess and Shogi by Planning with a Learned Model ([arXiv:1911.08265](https://arxiv.org/pdf/1911.08265)) ([cited by 20+](https://scholar.google.com/scholar?cites=14616963305010665808))
By Julian Schrittwieser, Ioannis Antonoglou, Thomas Hubert, Karen Simonyan, Laurent Sifre, Simon Schmitt, Arthur Guez, Edward Lockhart, Demis Hassabis, Thore Graepel, [Timothy Lillicrap](https://scholar.google.ca/citations?user=htPVdRMAAAAJ), [David Silver](https://scholar.google.com/citations?user=-8DNE4UAAAAJ)

**tl;dr** We present the MuZero algorithm, which, by combining a tree-based search with a learned model, achieves superhuman performance in a range of challenging and visually complex domains.

**Publication**: [deepmind.com/research/publications/Mastering-Atari-Go-Chess-and-Shogi-by-Planning-with-a-Learned-Model](https://deepmind.com/research/publications/Mastering-Atari-Go-Chess-and-Shogi-by-Planning-with-a-Learned-Model)

**Keywords**: game agent, planning, tree-based search, learned model

---

### *AttentionGAN*: Unpaired Image-to-Image Translation using Attention-Guided Generative Adversarial Networks ([arXiv:1911.11897](https://arxiv.org/pdf/1911.11897)) ([cited by 2+](https://scholar.google.com/scholar?cites=877210874690838822))
By Hao Tang, Hong Liu, Dan Xu, Philip H.S. Torr, Nicu Sebe

**tl;dr** AttentionGAN: Attention-Guided Generative Adversarial Networks for Unpaired Image-to-Image Translation

**Source code**: [github.com/Ha0Tang/AttentionGAN](https://github.com/Ha0Tang/AttentionGAN)

**Keywords**: AttentionGAN, GAN, img2img

---

### Dream to Control: Learning Behaviors by Latent Imagination ([arXiv:1912.01603](https://arxiv.org/pdf/1912.01603)) ([cited by 8+](https://scholar.google.com/scholar?cites=14974700822970491825))
By Danijar Hafner, Timothy Lillicrap, Jimmy Ba, Mohammad Norouzi

**tl;dr** We present Dreamer, a reinforcement learning agent that solves long-horizon tasks from images purely by latent imagination.

**Keywords**: imagination, planning, RL

---

### 🌟 12-in-1: Multi-Task Vision and Language Representation Learning ([arXiv:1912.02315](https://arxiv.org/pdf/1912.02315.pdf)) ([cited by 20+](https://scholar.google.com/scholar?cites=17276757515931533114))
By Jiasen Lu, Vedanuj Goswami, Marcus Rohrbach, Devi Parikh, Stefan Lee

**tl;dr** Large-scale multi-task training for vision-and-language tasks improves performance by up to 2.05 points on average across tasks, with a reduction from approximately 3 billion parameters to 270 million.

**Keywords**: vision-language models


---

### Analyzing and Improving the Image Quality of StyleGAN ([arXiv:1912.04958](https://arxiv.org/pdf/1912.04958)) ([cited by 40+](https://scholar.google.com/scholar?cites=1745485284765270951))
By Tero Karras, Samuli Laine, Miika Aittala, Janne Hellsten, Jaakko Lehtinen, Timo Aila

**tl;dr** The style-based GAN architecture yields state-of-the-art results in data-driven unconditional generative image modeling.

![Example](https://i.imgur.com/DvOkEuo.png)

**Explanation**

+ [StyleGANv2 Explained!](https://www.youtube.com/watch?v=u8qPvzk0AfY)
+ [Face editing with Generative Adversarial Networks](https://www.youtube.com/watch?v=dCKbRCUyop8)
+ [StyleGAN2: Near-Perfect Human Face Synthesis and More](https://www.youtube.com/watch?v=SWoravHhsUU)

**Source code**: [github.com/NVlabs/stylegan2](https://github.com/NVlabs/stylegan2)

**Source code (PyTorch)**: [github.com/lucidrains/stylegan2-pytorch](https://github.com/lucidrains/stylegan2-pytorch)

**Keywords**: GAN, image generation

---

### *GTN*: Generative Teaching Networks: Accelerating Neural Architecture Search by Learning to Generate Synthetic Training Data ([arXiv:1912.07768](https://arxiv.org/pdf/1912.07768)) ([cited by 2+](https://scholar.google.com/scholar?cites=17431064421189476903))
By Felipe Petroski Such, Aditya Rawal, Joel Lehman, [Kenneth O. Stanley](https://scholar.google.ca/citations?user=6Q6oO1MAAAAJ), [Jeff Clune](https://scholar.google.ca/citations?hl=en&user=5TZ7f5wAAAAJ)

**tl;dr** This paper investigates the intriguing question of whether we can create learning algorithms that automatically generate training data, learning environments, and curricula in order to help AI agents rapidly learn.

**Keywords**: meta-learning, training data generation

---

### *FARGAN*: Alleviation of Gradient Exploding in GANs: Fake Can Be Real ([arXiv:1912.12485](https://arxiv.org/pdf/1912.12485)) ([waiting for citations](https://scholar.google.com/scholar?cites=2597201475146774957))
By Song Tao, Jia Wang

**tl;dr** We propose a novel training method of GANs in which certain fake samples are considered as real ones during the training process to alleviate mode collapse phenomenon.

**Keywords**: GAN, exploding gradient problem

## 2020

### Reformer: The Efficient Transformer ([arXiv:2001.04451](https://arxiv.org/pdf/2001.04451)) ([cited by 20+](https://scholar.google.com/scholar?cites=16827908105960721293))
By Nikita Kitaev, Łukasz Kaiser, Anselm Levskaya

**tl;dr** Efficient Transformer with locality-sensitive hashing and reversible residuals.

**Keywords**: NLP, transformers

---

### 🌟 PET: Exploiting Cloze Questions for Few Shot Text Classification and Natural Language Inference ([arXiv:2001.07676](https://arxiv.org/abs/2001.07676)) ([cited by 4+](https://scholar.google.com/scholar?cites=11757272191456169611))
By Timo Schick, Hinrich Schütze

**tl;dr** A semi-supervised training procedure that reformulates input examples as cloze-style phrases to help language models understand a given task.

Blog summary: [pragmatic.ml/pet](https://www.pragmatic.ml/pet/)
Code: [timoschick/pet](https://github.com/timoschick/pet)

**Keywords**: language models, natural language, NLP

---

### Towards a Human-like Open-Domain Chatbot ([arXiv:2001.09977](https://arxiv.org/pdf/2001.09977)) ([cited by 20+](https://scholar.google.com/scholar?cites=4079885979813489169))
By Daniel Adiwardana, Minh-Thang Luong, David R. So, Jamie Hall, Noah Fiedel, Romal Thoppilan, Zi Yang, Apoorv Kulshreshtha, Gaurav Nemade, Yifeng Lu, Quoc V. Le

**tl;dr** A multi-turn open-domain chatbot trained end-to-end on data mined and filtered from public domain social media conversations.

**Keywords**: NLP, chatbot

---

### Generating Digital Painting Lighting Effects via RGB-space Geometry ([paper](https://lllyasviel.github.io/PaintingLight/files/TOG20PaintingLight.pdf)) (---
)
By Lvmin Zhang, Edgar Simo-Serra, Yi Ji, and Chunping Liu 

**tl;dr** We present an algorithm to generate digital painting lighting effects from a single image by simulating artists' coarse-to-fine workflow.

**Website**: [lllyasviel.github.io/PaintingLight/](https://lllyasviel.github.io/PaintingLight/)

**Video**: [YouTube](https://www.youtube.com/watch?v=X7li86oMBLA)

**Keywords**: art generation, image lighting

---

### Product Kanerva Machines: Factorized Bayesian Memory ([arXiv:2002.02385](https://arxiv.org/pdf/2002.02385.pdf)) ([cited by 2+](https://scholar.google.com/scholar?cites=14422382372492266069))
By Adam Marblestone, Yan Wu, Greg Wayne

**tl;dr** We introduce the Product Kanerva Machine, which dynamically combines many smaller Kanerva Machines into one large one, which can exhibit unsupervised clustering, find sparse and combinatorial allocation patterns, and discover spatial tunings that approximately factorize simple images by object.

**Keywords**: memory

---

### The Next Decade in AI: Four Steps Towards Robust Artificial Intelligence ([arXiv:2002.06177](https://arxiv.org/pdf/2002.06177)) ([cited by 3+](https://scholar.google.com/scholar?cites=10762068228089742706))
By Gary Marcus

**tl;dr** I propose a knowledge-driven, reasoning-based approach, centered around cognitive models, that could provide the substrate for a richer, more robust AI.

**Keywords**: 

---

### 🌟 *ANML*: Learning to Continually Learn ([arXiv:2002.09571](https://arxiv.org/pdf/2002.09571)) ([waiting for citations](https://scholar.google.com/scholar?cites=300016629715374995))
By Shawn Beaulieu, Lapo Frati, [Thomas Miconi](https://scholar.google.ca/citations?hl=en&user=EXun8woAAAAJ), Joel Lehman, [Kenneth O. Stanley](https://scholar.google.ca/citations?user=6Q6oO1MAAAAJ), [Jeff Clune](https://scholar.google.ca/citations?hl=en&user=5TZ7f5wAAAAJ), Nick Cheney

**tl;dr** We propose A Neuromodulated Meta-Learning Algorithm for continual learning without catastrophic forgetting at scale.

**Keywords**: meta-learning, neuromodulation

---

### 🌟 *Backpropamine*: training self-modifying neural networks with differentiable neuromodulated plasticity ([arXiv:2002.10585](https://arxiv.org/pdf/2002.10585)) ([cited by 20+](https://scholar.google.com/scholar?cites=15035648294490623736))
By [Thomas Miconi](https://scholar.google.ca/citations?hl=en&user=EXun8woAAAAJ), Aditya Rawal, [Jeff Clune](https://scholar.google.ca/citations?hl=en&user=5TZ7f5wAAAAJ), [Kenneth O. Stanley](https://scholar.google.ca/citations?user=6Q6oO1MAAAAJ)

**tl;dr** We show that neuromodulated plasticity improves the performance of neural networks on both reinforcement learning and supervised learning tasks.

**Keywords**: meta-learning, LSTM, biological learning, Hebbian learning

---

### 🌟 *SentenceMIM*: A Latent Variable Language Model ([arXiv:2003.02645](https://arxiv.org/pdf/2003.02645)) ([cited by 2+](https://scholar.google.com/scholar?cites=10250754712606094616))
By Micha Livne, Kevin Swersky, David J. Fleet

**tl;dr** We introduce SentenceMIM, a probabilistic auto-encoder for language modelling, trained with Mutual Information Machine learning.

![Chart](https://i.imgur.com/XBzVfyX.png "Chart")
*[Achieved state of the art](https://paperswithcode.com/sota/language-modelling-on-penn-treebank-word) on Penn Treebank (Word Level) dataset with 179M parameters and outperforms GPT2 with 12M parameters and no extra training.*

**Keywords**: NLP, VAE, MIM, JSD, LVM

---

### NeRF: Representing Scenes as Neural Radiance Fields for View Synthesis ([arXiv:2003.08934](https://arxiv.org/pdf/2003.08934)) ([cited by 1+](https://scholar.google.com/scholar?cites=9378169911033868166))
By Ben Mildenhall, Pratul P. Srinivasan, Matthew Tancik, Jonathan T. Barron, Ravi Ramamoorthi, Ren Ng

**tl;dr** We present a method for synthesizing novel views of complex scenes by optimizing an underlying continuous volumetric scene function using a sparse set of input views.

**Website**: [www.matthewtancik.com/nerf](http://www.matthewtancik.com/nerf)

**Summary**: [Video by Two Minute Papers](https://www.youtube.com/watch?v=nCpGStnayHk)

**Keywords**: volumetric scene generation

---

### GAN Compression: Efficient Architectures for Interactive Conditional GANs ([arXiv:2003.08936](https://arxiv.org/pdf/2003.08936)) ([cited by 10+](https://scholar.google.com/scholar?cites=4661770039879488594))
By Muyang Li, Ji Lin, Yaoyao Ding, Zhijian Liu, Jun-Yan Zhu, Song Han

**tl;dr** We propose a general-purpose compression framework for reducing the inference time and model size of the generator in cGANs.

**Keywords**: GAN, compression, image generation

---

### 🌟 Enhanced POET: Open-Ended Reinforcement Learning through Unbounded Invention of Learning Challenges and their Solutions ([arXiv:2003.08536](https://arxiv.org/pdf/2003.08536)) ([waiting for citations](https://scholar.google.com/scholar?cites=17583648324422024748))
By Rui Wang, Joel Lehman, Aditya Rawal, Jiale Zhi, Yulun Li, [Jeff Clune](https://scholar.google.ca/citations?hl=en&user=5TZ7f5wAAAAJ), [Kenneth O. Stanley](https://scholar.google.ca/citations?user=6Q6oO1MAAAAJ)

**tl;dr** We introduce and empirically validate two new innovations to the original POET, as well as two external innovations designed to elucidate its full potential.

**Explanation**: [Video by Henry AI Labs](https://www.youtube.com/watch?v=jxIkPxkN10U)

**Keywords**: POET, neuroevolution

---

### In-Domain GAN Inversion for Real Image Editing ([arXiv:2004.00049](https://arxiv.org/pdf/2004.00049.pdf)) (New)
By Jiapeng Zhu,  Yujun Shen,  Deli Zhao,  Bolei Zhou

**tl;dr** In this work, we argue that the GAN inversion task is required not only to reconstruct the target image by pixel values, but also to keep the inverted code in the semantic domain of the original latent space of well-trained GANs.

Website: [genforce.github.io/idinvert](https://genforce.github.io/idinvert/)
Code (PyTorch): [genforce/idinvert_pytorch](https://github.com/genforce/idinvert_pytorch)
Code (Tensorflow): [genforce/idinvert](https://github.com/genforce/idinvert)

**Keywords**: image generation, style control

---

### Longformer: The Long-Document Transformer ([arXiv:2004.05150](https://arxiv.org/pdf/2004.05150)) ([cited by 4+](https://scholar.google.com/scholar?cites=9544623782762227924))
By Iz Beltagy, Matthew E. Peters, Arman Cohan

**tl;dr** We introduce Longformer with an attention mechanism that scales linearly with sequence length, making it easy to process documents of thousands of tokens or longer.

**Explanation**: [YouTube](https://www.youtube.com/watch?v=_8KNb5iqblE)

**Memory requirements**: [YouTube](https://www.youtube.com/watch?v=gJR28onlqzs)

**Keywords**: transformer

---

### 🌟 Divide-and-Conquer Monte Carlo Tree Search For Goal-Directed Planning ([arXiv:2004.11410](https://arxiv.org/pdf/2004.11410)) ([waiting for citations](https://scholar.google.com/scholar?cites=8335433676614214635))
By Giambattista Parascandolo, Lars Buesing, Josh Merel, Leonard Hasenclever, John Aslanides, Jessica B. Hamrick, Nicolas Heess, Alexander Neitz, Theophane Weber

**tl;dr** We propose a planning algorithm, Divide-and-Conquer Monte Carlo Tree Search (DC-MCTS), for approximating the optimal plan.

**Explanation**: [YouTube](https://www.youtube.com/watch?v=tjbEVY5XIk0)

**Keywords**: MCTS, planning, RL, search

---

### 🌟 First return then explore ([arXiv:2004.12919](https://arxiv.org/pdf/2004.12919)) ([waiting for citations](https://scholar.google.com/scholar?cluster=16571518062556861489))
By Adrien Ecoffet, Joost Huizinga, Joel Lehman, [Kenneth O. Stanley](https://scholar.google.ca/citations?user=6Q6oO1MAAAAJ), [Jeff Clune](https://scholar.google.ca/citations?hl=en&user=5TZ7f5wAAAAJ)

**tl;dr** We introduce Go-Explore, a family of RL algorithms that addresses these two challenges directly through the simple principles of explicitly remembering promising states and first returning.

**Keywords**: RL, exploration, curiosity

---

### Recipes for building an open-domain chatbot ([arXiv:2004.13637](https://arxiv.org/pdf/2004.13637)) ([cited by 1+](https://scholar.google.com/scholar?cites=14419775353832230497))
By Stephen Roller, Emily Dinan, Naman Goyal, Da Ju, Mary Williamson, Yinhan Liu, Jing Xu, Myle Ott, Kurt Shuster, Eric M. Smith, Y-Lan Boureau, Jason Weston

**tl;dr** We show that large scale models can learn conversationalist skills when given appropriate training data and choice of generation strategy.

**Keywords**: NLP, chatbots

---

### Reinforcement Learning with Augmented Data ([arXiv:2004.14990](https://arxiv.org/pdf/2004.14990)) ([waiting for citations](https://scholar.google.com/scholar?cites=6008226201213548118))
By Michael Laskin, Kimin Lee, Adam Stooke, Lerrel Pinto, Pieter Abbeel, Aravind Srinivas

**tl;dr** We show that data diversity alone can make agents focus on meaningful information from high-dimensional observations without any changes to the reinforcement learning method.

**Explanation**: [Video by Yannic Kilcher](https://www.youtube.com/watch?v=to7vCdkLi4s)

**Keywords**: RL, data augmentation

---

### TLDR: Extreme Summarization of Scientific Documents ([arXiv:2004.15011](https://arxiv.org/abs/2004.15011)) (New)

**tl;dr** We introduce TLDR generation, a new form of extreme summarization, for scientific papers that produces high-quality summaries while minimizing annotation burden.

---

### It's Easier to Translate out of English than into it: Measuring Neural Translation Difficulty by Cross-Mutual Information ([arXiv:2005.02354](https://arxiv.org/abs/2005.02354)) (New)
By Emanuele Bugliarello, Sabrina J. Mielke, Antonios Anastasopoulos, Ryan Cotterell, Naoaki Okazaki

**tl;dr** An asymmetric information-theoretic metric of machine translation difficulty that exploits the probabilistic nature of most neural machine translation models.

**Keywords**: mutual information

---

### Reference-Based Sketch Image Colorization using Augmented-Self Reference and Dense Semantic Correspondence ([arXiv:2005.05207](https://arxiv.org/pdf/2005.05207)) ([waiting for citations](https://scholar.google.com/scholar?cites=14285897374649732318))
By Junsoo Lee, Eungyeup Kim, Yunsung Lee, Dongjun Kim, Jaehyuk Chang, Jaegul Choo

**tl;dr** This paper tackles the automatic colorization task of a sketch image given an already-colored reference image.

**Keywords**: image colorization, style transfer

---

### *Plan2Explore*: Planning to Explore via Self-Supervised World Models ([arXiv:2005.05960](https://arxiv.org/pdf/2005.05960)) (New)
By Ramanan Sekar, Oleh Rybkin, Kostas Daniilidis, Pieter Abbeel, Danijar Hafner, Deepak Pathak

**tl;dr** Plan2Explore is a self-supervised exploration and fast adaptation to new tasks, which need not be known during exploration.

**Keywords**: intrinsic curiosity, exploration, RL

**Demo and source code**: [ramanans1.github.io/plan2explore/](https://ramanans1.github.io/plan2explore/)

**Explanation**: [Video by Yannic Kilcher](https://www.youtube.com/watch?v=IiBFqnNu7A8)

---

### Language Models are Few-Shot Learners ([arXiv:2005.14165](https://arxiv.org/pdf/2005.14165)) ([cited by 160+](https://scholar.google.com/scholar?cites=15953747982133883426))
By Tom B. Brown, Benjamin Mann, Nick Ryder, Melanie Subbiah, Jared Kaplan, Prafulla Dhariwal, Arvind Neelakantan, Pranav Shyam, Girish Sastry, Amanda Askell, Sandhini Agarwal, Ariel Herbert-Voss, Gretchen Krueger, Tom Henighan, Rewon Child, Aditya Ramesh, Daniel M. Ziegler, Jeffrey Wu, Clemens Winter, Christopher Hesse, Mark Chen, Eric Sigler, Mateusz Litwin, Scott Gray, Benjamin Chess, Jack Clark, Christopher Berner, Sam McCandlish, Alec Radford, Ilya Sutskever, Dario Amodei

**tl;dr** We train GPT-3, an autoregressive language model with 175 billion parameters, 10x more than any previous non-sparse language

**Keywords**: language model, distributed computing

---

### Learning to Simulate Dynamic Environments with GameGAN ([preprint release coming May 25th](https://nv-tlabs.github.io/gameGAN/))
By Seung Wook Kim, Yuhao Zhou, Antonio Torralba, Sanja Fidler

**tl;dr** We introduce GameGAN, a generative model that learns to visually imitate a desired game by ingesting screenplay and keyboard actions during training.

**Publication**: [blogs.nvidia.com/blog/2020/05/22/gamegan-research-pacman-anniversary/](https://blogs.nvidia.com/blog/2020/05/22/gamegan-research-pacman-anniversary/)

**Explanation**: [Video by Henry AI Labs](https://www.youtube.com/watch?v=H8F6J7mYyz0)

**Website**: [nv-tlabs.github.io/gameGAN/](https://nv-tlabs.github.io/gameGAN/)

**Keywords**: GAN, game agent, planning, image generation

---

### 🌟 Unsupervised Translation of Programming Languages ([arXiv:2006.03511](https://arxiv.org/pdf/2006.03511))
By Marie-Anne Lachaux, Baptiste Roziere, Lowik Chanussot, Guillaume Lample

**tl;dr** A fully unsupervised neural transcompiler that can translate functions between C++, Java, and Python with high accuracy.

**Keywords**: transcompiler

---

### 🌟 Predictive Coding Approximates Backprop along Arbitrary Computation Graphs ([arXiv:2006.04182](https://arxiv.org/pdf/2006.04182.pdf)) ([cited by 3+](https://scholar.google.com/scholar?cites=12701710878381226840))
By Beren Millidge, Alexander Tschantz, Christopher L. Buckley

**tl;dr** Predictive coding can approximate backprop in multilayer-perceptrons using only local and Hebbian updates, while utilising only local Hebbian plasticity.

**Keywords**: metalearning, backpropagation

---

### A bio-inspired bistable recurrent cell allows for long-lasting memory ([arXiv:2006.05252](https://arxiv.org/pdf/2006.05252.pdf)) ([New](https://scholar.google.com/scholar?cites=11215192328819614152))
By Nicolas Vecoven, Damien Ernst, Guillaume Drion

**tl;dr** We take inspiration from biological neuron bistability to embed RNNs with long-lasting memory at the cellular level.

**Keywords**: RNN, LSTM, GRU, catastrophic forgetting

---

### Distributed Memory based Self-Supervised Differentiable Neural Computer ([arXiv:2007.10637](https://arxiv.org/pdf/2007.10637.pdf)) ([New](https://scholar.google.com/scholar?cites=3626825700559595339))
By Taewon Park, Inchul Choi, Minho Lee

**tl;dr** We propose a novel distributed memory-based self-supervised DNC architecture for enhanced memory augmented neural network performance.

**Keywords**: MANN

---

### Learning Disentangled Representations with Latent Variation Predictability ([arXiv:2007.12885](https://arxiv.org/pdf/2007.12885.pdf)) (New)
By Xinqi Zhu, Chang Xu, Dacheng Tao

**tl;dr** A fully unsupervised neural transcompiler that can translate functions between C++, Java, and Python with high accuracy.

**Keywords**: mutual information

---

### NVAE: A Deep Hierarchical Variational Autoencoder ([arXiv:2007.03898](https://arxiv.org/pdf/2007.03898)) (New)
By Arash Vahdat, Jan Kautz

**tl;dr** We propose Nouveau VAE, a deep hierarchical VAE for image generation using depth-wise separable convolutions and batch normalization.

**Keywords**: VAE

---

### Audiovisual Speech Synthesis using Tacotron2 ([arXiv:2008.00620](https://arxiv.org/abs/2008.00620)) (New)
By Ahmed Hussen Abdelaziz, Anushree Prasanna Kumar, Chloe Seivwright, Gabriele Fanelli, Justin Binder, Yannis Stylianou, Sachin Kajarekar

**tl;dr** AVTacotron2: End-to-end and modular audiovisual speech synthesis for 3D face models.

---

### It's Not Just Size That Matters: Small Language Models Are Also Few-Shot Learners ([arXiv:2009.07118](https://arxiv.org/pdf/2009.07118)) (New)
By Timo Schick, Hinrich Schütze

**tl;dr** We show that state-of-the-art natural language understanding can be obtained with language models whose parameter count is several orders of magnitude smaller.

**Keywords**: GPT3, few-shot learning

---

### Contrastive learning of medical visual representations from paired images and text ([arXiv:2010.00747](https://arxiv.org/pdf/2010.00747.pdf)) (New)
By Yuhao Zhang, Hang Jiang, Yasuhide Miura, Christopher D. Manning, Curtis P. Langlotz

**tl;dr** Learning visual representations of medical images from the naturally occurring pairing of images and textual data via a bidirectional contrastive objective between the two modalities.

**Keywords**: vision-language models

---

### Vokenization: Improving Language Understanding with Contextualized, Visual-Grounded Supervision ([arXiv:2010.06775](https://arxiv.org/pdf/2010.06775.pdf)) (New)
By Hao Tan, Mohit Bansal

**tl;dr** Humans learn language by listening, speaking, writing, reading, and also, via interaction with the multimodal real world.

**Keywords**: vision-language models

Video: [Vokenization Explained](https://www.youtube.com/watch?v=KhWj2twqud8)

## Research paper channels

+ [Henry AI Labs](https://www.youtube.com/channel/UCHB9VepY6kYvZjj0Bgxnpbw)
+ [Yannic Kilcher](https://www.youtube.com/channel/UCZHmQk67mSJgfCCTn7xBfew)
+ [Lex Fridman](https://www.youtube.com/channel/UCSHZKyawb77ixDdsGog4iWA)
+ [Two Minute Papers](https://www.youtube.com/channel/UCbfYPyITQ-7l4upoX8nvctg)
+ [ArXiv Insights](https://www.youtube.com/channel/UCNIkB2IeJ-6AmZv7bQ1oBYg)

## Additional reading

For a deeper exploration into machine learning, its history, and other techniques:

+ [Highlights in artificial intelligence history before 2000](http://people.idsia.ch/~juergen/ai.html)
+ [Deep Learning in Neural Networks: An Overview](https://arxiv.org/pdf/1404.7828)
+ [Deep Reinforcement Learning](https://arxiv.org/pdf/1810.06339)
+ [Jürgen Schmidhuber's home page](http://people.idsia.ch/~juergen/)
+ [Activation Functions: Comparison of Trends in Practice and Research for Deep Learning](https://arxiv.org/pdf/1811.03378.pdf)
+ [Deep Reinforcement Learning that Matters](https://arxiv.org/pdf/1709.06560)

## Tips

- You can find recommended papers on Arxiv by clicking the Recommenders tab below the abstract and enabling the CORE recommender.

## Changelog

### 28 Oct 2020

1. Product Kanerva Machines: Factorized Bayesian Memory (Marblestone et al., 2020)
2. Scaling Memory-Augmented Neural Networks with Sparse Reads and Writes (Rae et al., 2016)
3. Distributed Memory based Self-Supervised Differentiable Neural Computer (Park et al., 2020)

### 26 Oct 2020

1. A bio-inspired bistable recurrent cell allows for long-lasting memory (Vecoven et al., 2020)

### 23 Oct 2020

Generated tl;dr summaries.

1. TLDR: Extreme Summarization of Scientific Documents (Cachola et al., 2020)
2. Tacotron: Towards End-to-End Speech Synthesis (Wang et al. 2017)
3. Natural TTS Synthesis by Conditioning WaveNet on Mel Spectrogram Predictions (Shen et al. 2017)
4. Audiovisual Speech Synthesis using Tacotron2 (Abdelaziz et al. 2020)
5. Waveglow: A flow-based generative network for speech synthesis (Prenger et al., 2019)
6. Language Models are Few-Shot Learners (Brown et al. 2020)
7. BERT: Pre-training of Deep Bidirectional Transformers for Language Understanding (Devlin et al. 2018)
8. BART: Denoising Sequence-to-Sequence Pre-training for Natural Language Generation, Translation, and Comprehension (Lewis et al., 2019)
9. ALBERT: A Lite BERT for Self-supervised Learning of Language Representations (Lan et al. 2019)

### 22 Oct 2020

1. Learning to reinforcement learn (Wang et al., 2016)
2. Predictive Coding Approximates Backprop along Arbitrary Computation Graphs (Millidge et al., 2020)

### 20 Oct 2020

1. In-Domain GAN Inversion for Real Image Editing (Zhu et al., 2020)
2. Interactive Sound Propagation with Bidirectional Path Tracing (Cao et al., 2016)
3. Exploiting Cloze Questions for Few Shot Text Classification and Natural Language Inference (Schick et al. 2020)
4. It's Not Just Size That Matters: Small Language Models Are Also Few-Shot Learners (Schick et al. 2020)
5. Vokenization: Improving Language Understanding with Contextualized, Visual-Grounded Supervision (Tan et al. 2020)
6. Contrastive learning of medical visual representations from paired images and text (Zhang et al. 2020)
7. 12-in-1: Multi-Task Vision and Language Representation Learning (Lu et al. 2020)
8. MSG-GAN: Multi-Scale Gradients for Generative Adversarial Networks (Karnewar et al. 2019)

### 15 Oct 2020

1. It's Easier to Translate out of English than into it: Measuring Neural Translation Difficulty by Cross-Mutual Information (Bugliarello et all. 2020)
2. Learning Disentangled Representations with Latent Variation Predictability (Zhu et al., 2020)
3. NVAE: A Deep Hierarchical Variational Autoencoder (Vahdat et al., 2020)
4. Unsupervised Translation of Programming Languages (Lachaux et al., 2020)
5. A generative vision model that trains with high data efficiency and breaks text-based CAPTCHAs (George et al. 2017)

### 17 May 2020
**New papers**

1. Large Memory Layers with Product Keys (Lample et al., Jul 2019)
2. Reformer: The Efficient Transformer (Kitaev et al., Jan 2020)
3. Recipes for building an open-domain chatbot (Roller et all., Apr 2020)
4. Multimodal Generative Models for Scalable Weakly-Supervised Learning (Wu et al. 2018)
5. Alleviation of Gradient Exploding in GANs: Fake Can Be Real (Tao et al. 2019)
6. Reference-Based Sketch Image Colorization using Augmented-Self Reference and Dense Semantic Correspondence (Lee et al. 2020)
7. Planning to Explore via Self-Supervised World Models (Sekar et al. 2020)
8. ACE: An Actor Ensemble Algorithm for Continuous Control with Tree Search (Zhang et al. 2018)
9. Dream to Control: Learning Behaviors by Latent Imagination (Hafner et al. 2019)
10. Generating Digital Painting Lighting Effects via RGB-space Geometry (Zhang et al. 2020)
11. Investigating Human Priors for Playing Video Games (Dubey et al. 2018)
12. Learning to Simulate Dynamic Environments with GameGAN (Kim et al. 2020)
13. How a life-like system emerges from a simplistic particle motion law (Schmickl et al. 2016)
14. Regularizing Model-Based Planning with Energy-Based Models (Boney et al. 2019)
15. Regularizing Trajectory Optimization with Denoising Autoencoders (Boney et al. 2019)
16. Design and Implementation of Multi-dimensional Flexible Antena-like Hair motivated by ’Aho-Hair’ in Japanese Anime Cartoons: Internal State Expressions beyond Design Limitations (Sasabuchi et al. 2015)

## To-do

1. Write a paper scraper that finds novel papers by constructing a Markov chain from citations and finding the stationary distribution

