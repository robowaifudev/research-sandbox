from argparse import ArgumentParser

p = ArgumentParser()
p.add_argument("input")
p.add_argument("output")
args = p.parse_args()

f = open(args.output, 'w')
skip = 0
line_no = 0
for line in open(args.input):
    line_no += 1
    if line[0] == '>':
        skip = 1
        continue
    if skip > 0:
        skip -= 1
        continue
    f.write(line)
    if line.strip() == "**Keywords**":
        print("Warning: missing keywords at line {}".format(line_no))
    if line.find("[cited by]") >= 0:
        print("Warning: missing citations at line {}".format(line_no))
    if line.strip() == "**tl;dr**":
        print("Warning: missing tl;dr {}".format(line_no))
    if line.strip() == "By":
        print("Warning: missing authors {}".format(line_no))
