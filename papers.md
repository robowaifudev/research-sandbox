# Hold onto your papers!

**Last updated**: 28 Oct 2020

This is a non-exhaustive curated list of papers to provide an overview of advancements in machine learning and links to [Google Scholar](https://scholar.google.com/) to make it easier to find new papers. Some abstracts have been truncated for brevity.

TL;DR summaries were generated and provided by [SciTLDR](https://scitldr.apps.allenai.org/) ([GitHub](https://github.com/allenai/scitldr))

## Content

+ [Glossary and Nomenclature](#glossary-and-nomenclature)
+ [Additional reading](#additional-reading)

## Glossary and Nomenclature

+ **AttentionGAN**: Attention-Guided Generative Adversarial Network
+ **BPTT**: Backpropagation Through Time
+ **CNN**: Convolutional Neural Network
+ **CycleGAN**: Cycle-consistent Generative Adversarial Network
+ **DenseNet**: Densely Connected Convolutional Networks
+ **DNC**: Differentiable Neural Computer
+ **GA**: Genetic Algorithm
+ **GAN**: Generative Adversarial Network
+ **Img2Img**: Image-to-image translation
+ **JSD**: Jensen-Shanon Divergence
+ **KLD**: Kullback-Leibler Divergence
+ **LVM**: latent variable model
+ **LSTM**: Long Short-Term Memory
+ **MANN**: Memory Augmented Neural Network 
+ **MCTS**: Monte Carlo Tree Search
+ **MIM**: Mututal Information Machine
+ **MoS**: Mixture of Softmaxes
+ **NLP**: Natural Language Processing
+ **PPL**: perplexity
+ **PPO**: Proximal Policy Optimization
+ **ResNet**: Residual Neural Network
+ **RL**: Reinforcement Learning
+ **SAM**: Sparse Access Memory
+ **Seq2Seq**: Sequence-to-sequence translation
+ **SE**: Squeeze and Excitation
+ **TCA**: Temporal Credit Assignment
+ **VAE-GAN**: Variational Auto-Encoder Generative Adversarial Network
+ **VAE**: Variational Auto-Encoder
+ **Vec2Vec**: Vector-to-vector translation

## 1997

### 🌟 *LSTM*: Long Short-Term Memory ([paper](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.676.4320&rep=rep1&type=pdf)) ([cited by 30000+](https://scholar.google.com/scholar?cites=1035920125298492854&as_sdt=2005&sciodt=0,5&hl=en))
By [Sepp Hochreiter](https://scholar.google.com/citations?user=tvUH3WMAAAAJ), [Jürgen Schmidhuber](https://scholar.google.com/citations?user=gLnCTgIAAAAJ)

**tl;dr** Learning to store information over extended time intervals via recurrent backpropagation takes a very long time, mostly due to insufficient, decaying error back flow.

> Learning to store information over extended time intervals via recurrent backpropagation takes a very long time, mostly due to insufficient, decaying error back flow. We briefly review Hochreiter's 1991 analysis of this problem, then address it by introducing a novel, efficient, gradient-based method called "Long Short-Term Memory" (LSTM). Truncating the gradient where this does not do harm, LSTM can learn to bridge minimal time lags in excess of 1000 discrete time steps by enforcing constant error flow through "constant error carrousels" within special units. Multiplicative gate units learn to open and close access to the constant error flow. LSTM is local in space and time; its computational complexity per time step and weight is O(1). Our experiments with artificial data involve local, distributed, real-valued, and noisy pattern representations. ... LSTM also solves complex, artificial long time lag tasks that have never been solved by previous recurrent network algorithms.

**Keywords**: LSTM, RNN

---

### On the optimality of the simple Bayesian classifier under zero-one loss([paper](http://edlab-www.cs.umass.edu/cs689/reading/optimality%20of%20naive%20bayes%20classifier.pdf)) ([cited by 3000+](https://scholar.google.com/scholar?cites=8190045656011597473))
By [Pedro Domingos](https://scholar.google.com/citations?user=KOrhfVMAAAAJ), Michael Pazzani

**tl;dr** The Bayesian classifier is optimal under zero-one loss (misclassification rate) even when this assumption is violated by a wide margin.

> The simple Bayesian classifier is known to be optimal when attributes are independent given the class, but the question of whether other sufficient conditions for its optimality exist has so far not been explored. Empirical results showing that it performs surprisingly well in many domains containing clear attribute dependences suggest that the answer to this question may be positive. This article shows that, although the Bayesian classifier's probability estimates are only optimal under quadratic loss if the independence assumption holds, the classifier itself can be optimal under zero-one loss (misclassification rate) even when this assumption is violated by a wide margin. The region of quadratic-loss optimality of the Bayesian classifier is in fact a second-order infinitesimal fraction of the region of zero-one optimality. This implies that the Bayesian classifier has a much greater range of applicability than previously thought. For example, in this article it is shown to be optimal for learning conjunctions and disjunctions, even though they violate the independence assumption. Further, studies in artificial domains show that it will often outperform more powerful classifiers for common training set sizes and numbers of attributes, even if its bias is a priori much less appropriate to the domain. This article's results also imply that detecting attribute dependence is not necessarily the best way to extend the Bayesian classifier, and this is also verified empirically.

**Keywords**: Bayesian

## 1998

### *LeNet-5*: Gradient-Based Learning Applied to Document Recognition ([paper](http://yann.lecun.com/exdb/publis/pdf/lecun-01a.pdf)) ([cited by 20000+](https://scholar.google.com/scholar?cites=1909057046224785356))
By Yann LeCun, Léon Bottou, Yoshua Bengio, Patrick Haffner

**tl;dr** Multilayer Neural Networks trained with the backpropagation algorithm constitute the best example of a successful Gradient-Based Learning technique.

> Multilayer Neural Networks trained with the backpropagation algorithm constitute the best example of a successful Gradient-Based Learning technique. Given an appropriate network architecture, Gradient-Based Learning algorithms can be used to synthesize a complex decision surface that can classify high-dimensional patterns such as handwritten characters, with minimal preprocessing. This paper reviews various methos applied to handwritten character recognition and compares them on a standard handwritten digit recognition task. Convolutional Neural Networks, that are specifically designed to deal with the variability of 2D shapes, are shown to outperform all other techniques.

**Keywords**: CNN

## 1999

### Learning to forget: Continual prediction with LSTM ([paper](https://www.researchgate.net/profile/Felix_Gers/publication/12292425_Learning_to_Forget_Continual_Prediction_with_LSTM/links/5759414608ae9a9c954e84c5/Learning-to-Forget-Continual-Prediction-with-LSTM.pdf)) ([cited by 2000+](https://scholar.google.com/scholar?cites=8872883539551190379))
By Felix A. Gers, [Jürgen Schmidhuber](https://scholar.google.com/citations?user=gLnCTgIAAAAJ), Fred Cummins

**tl;dr** A novel, adaptive "forget gate" that enables an LSTM cell to learn to reset itself at appropriate times, thus releasing internal resources.

> Long Short-Term Memory (LSTM, Hochreiter & Schmidhuber, 1997) can solve numerous taks not solvable by previous learning algorithms for recurrent neural networks (RNNs). We identify a weakness of LSTM networks processing continual input streams that are not *a priori* segmented into subsequences with explicitly marked ends at which the network's internal state could be reset. Without resets, the state may grow indefinitely and eventually cause the network to break down. Our remedy is a novel, adaptive "forget gate" that enables an LSTM cell to learn to reset itself at appropriate times, thus releasing internal resources. We review illustrative benchmark problems on which standard LSTM outperforms other RNN algorithms. All algorithms (including LSTM) fail to solve continual versions of these problems. LSTM with forget gates, however, easily solves them in an elegant way.

**Keywords**: LSTM


## 2000

### *ReLU**: Digital selection and analogue amplification coexist in a cortex-inspired silicon circuit ([paper](http://csbi.mit.edu/people/pdf/nature_cover.pdf)) ([cited by 700+](https://scholar.google.com/scholar?cites=8091746083324814153))
By Richard H. R. Hahnloser, Rahul Sarpeshkar, Misha A. Mahowald, Rodney J. Douglas & H. Sebastian Seung

**tl;dr** The neocortex combines digital selection of an active set of neurons with analogue response by dynamically varying the positive feedback inherent in its recurrent connections.

> Digital circuits such as the flip-flop use feedback to achieve multi-stability and nonlinearity to restore signals to logical levels, for example 0 and 1. Analogue feedback circuits are generally designed to operate linearly, so that signals are over a range, and the response is unique. By contrast, the response of cortical circuits to sensory stimulation can be both multistable and graded. We propose that the neocortex combines digital selection of an active set of neurons with analogue response by dynamically varying the positive feedback inherent in its recurrent connections. Strong positive feedback causes differential instabilities that drive the selection of a set of active neurons under the constraints embedded in the synaptic weights. Once selected, the active neurons generate weaker, stable feedback that provides analogue amplification of the input.

**Keywords**: RELU, activation function

*: The authors did not name it but introduced the concept of ReLU (see Glorot et al. 2011)

## 2002

### *NEAT*: Evolving Neural Networks through Augmenting Topologies ([paper](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.28.5457&rep=rep1&type=pdf)) ([cited by 2000+](https://scholar.google.ca/scholar?cites=14852294327125551644))
By [Kenneth O. Stanley](https://scholar.google.ca/citations?user=6Q6oO1MAAAAJ), Risto Miikkulainen

**tl;dr** NeuroEvolution of Augmenting Topologies (NEAT) outperforms the best fixed-topology method on a challenging benchmark reinforcement learning task.

> An important question in neuroevolution is how to gain an advantage from evolving neural network topologies along with weights. We present a method, NeuroEvolution of Augmenting Topologies (NEAT) that outperforms the best fixed-topology method on a challenging benchmark reinforcement learning task. We claim that the increased efficiency is due to (1) employing a principled method of crossover of different topologies, (2) protecting structural innovation using speciation, and (3) incrementally growing from minimal structure. We test this claim through a series of ablation studies that demonstrate that each component is necessary to the system as a whole and to each other. What results is significantly faster learning. NEAT is also an important contribution to GAs because it shows howit is possible for evolution to both optimize and complexify solutions simultaneously, offering the possibility of evolving increasingly complex solutions over generations, and strengthening the analogy with biological evolution.

**Keywords**: neuroevolution, GA

## 2004


### Feature selection, L1 vs. L2 regularization, and rotational invariance ([paper](http://robotics.stanford.edu/~ang/papers/icml04-l1l2.pdf)) ([cited by 1000+](https://scholar.google.com/scholar?cites=11156383625480008410))
By [Andrew Y. Ng](https://scholar.google.com/citations?user=mG4imMEAAAAJ)

**tl;dr** We show that L1 regularized logistic regression can be effective even if there are exponentially many irrelevant features as there are training examples.

> We consider supervised learning in the presence of very many irrelevant features, and study two different regularization methods for preventing overfitting. Focusing on logistic regression, we show that using L1 regularization of the parameters, the sample complexity (i.e., the number of training examples required to learn "well,") grows only logarithmically in the number of irrelevant features. This logarithmic rate matches the best known bounds for feature selection, and indicates that L1 regularized logistic regression can be effective even if there are exponentially many irrelevant features as there are training examples. We also give a lower-bound showing that any rotationally invariant algorithm---
including logistic regression with L2 regularization, SVMs, and neural networks trained by backpropagation---
has a worst case sample complexity that grows at least linearly in the number of irrelevant features.

**Keywords**: regularization

## 2005


### Framewise Phoneme Classification with Bidirectional LSTM and Other Neural Network Architectures
By Alex Graves and Jurgen Schmidhuber

**tl;dr** In this paper, we present bidirectional LSTM networks, and a modified, full gradient version of the LSTM learning algorithm.

> In this paper, we present bidirectional Long Short Term Memory (LSTM) networks, and a modified, full gradient version of the LSTM learning algorithm. We evaluate bidirectional LSTM (BLSTM) and several other network architectures on the benchmark task of framewise phoneme classification, using the TIMIT database. Our main findings are that bidirectional networks outperform unidirectional ones, and that LSTM is much faster and also more accurate than both standard Recurrent Neural Nets (RNNs) and time-windowed Multilayer Perceptrons (MLPs). Our results support the view that contextual information is crucial to speech processing, and suggest that BLSTM is an effective architecture with which to exploit it.

## 2006

### Connectionist temporal classification: labelling unsegmented sequence data with recurrent neural networks ([paper](http://www.idsia.ch/~santiago/papers/icml2006.pdf)) ([cited by 2000+](https://scholar.google.com/scholar?cites=6142126079742865912))
By Alex Graves, Santiago Fernández, Faustino John Gomez, Jürgen Schmidhuber

**tl;dr** This paper presents a novel method for training RNNs to label unsegmented sequences directly, thereby solving both problems.

> Many real-world sequence learning tasks require the prediction of sequences of labels from noisy, unsegmented input data. In speech recognition, for example, an acoustic signal is transcribed into words or sub-word units. Recurrent neural networks (RNNs) are powerful sequence learners that would seem well suited to such tasks. However, because they require pre-segmented training data, and post-processing to transform their outputs into label sequences, their applicability has so far been limited. This paper presents a novel method for training RNNs to label unsegmented sequences directly, thereby solving both problems. An experiment on the TIMIT speech corpus demonstrates its advantages over both a baseline HMM and a hybrid HMM-RNN.

**Keywords**: RNN, speech recognition

---

### Markov logic networks ([paper](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.170.7952&rep=rep1&type=pdf)) ([cited by 2000+](https://scholar.google.com/scholar?cites=1545177577956914978))
By Matthew Richardson, [Pedro Domingos](https://scholar.google.com/citations?user=KOrhfVMAAAAJ)

**tl;dr** We propose a simple approach to combining first-order logic and probabilistic graphical models in a single representation.

> We propose a simple approach to combining first-order logic and probabilistic graphical models in a single representation. A Markov logic network (MLN) is a first-order knowledge base with a weight attached to each formula (or clause). Together with a set of constants representing objects in the domain, it specifies a ground Markov network containing one feature for each possible grounding of a first-order formula in the KB, with the corresponding weight. Inference in MLNs is performed by MCMC over the minimal subset of the ground network required for answering the query. Weights are efficiently learned from relational databases by iteratively optimizing a pseudo-likelihood measure. Optionally, additional clauses are learned using inductive logic programming techniques. Experiments with a real-world database and knowledge base in a university domain illustrate the promise of this approach.

**Keywords**: Markov network

## 2009

### *ImageNet*: A Large-Scale Hierarchical Image Database ([paper](http://vision.stanford.edu/documents/ImageNet_CVPR2009.pdf)) ([cited by 10000+](https://scholar.google.com/scholar?cites=610894740843277394))

**tl;dr** ImageNet is a large-scale ontology of images built upon the WordNet structure.

> The explosion of image data on the Internet has the potential to foster more sophisticated and robust models and algorithms to index, retrieve, organize and interact with images and multimedia data.  But exactly how such data can be harnessed and organized remains a critical problem. We introduce here a new database called “ImageNet”, a large-scale  ontology  of  images  built  upon  the  backbone  of  the WordNet structure. ImageNet aims to populate the majority of the 80,000 synsets of WordNet with an average of 500-1000 clean and full resolution images.  This will result in tens of millions of annotated images organized by the semantic hierarchy of WordNet.

**Keywords**: image classification, dataset

---

### *HyperNEAT*: A Hypercube-Based Encoding for Evolving Large-Scale Neural Networks ([paper](https://stars.library.ucf.edu/cgi/viewcontent.cgi?article=3177&context=facultybib2000)) ([cited by 600+](https://scholar.google.com/scholar?cites=5870589580018203360))
By [Kenneth O. Stanley](https://scholar.google.ca/citations?user=6Q6oO1MAAAAJ), David B. D'Ambrosio and Jason Gauci

**tl;dr** HyperNEAT employs an indirect encoding called connective compositional pattern-producing networks (CPPNs) to produce connectivity patterns with symmetries.

> Research in neuroevolution—that is, evolving artificial neural networks (ANNs) through evolutionary algorithms—is inspired by the evolution of biological brains, which can contain trillions of connections. Yet while neuroevolution has produced successful results, the scale of natural brains remains far beyond reach. This article presents a method called hypercube-based NeuroEvolution of Augmenting Topologies (HyperNEAT) that aims to narrow this gap. HyperNEAT employs an indirect encoding called connective compositional pattern-producing networks (CPPNs) that can produce connectivity patterns with symmetries and repeating motifs by interpreting spatial patterns generated within a hypercube as connectivity patterns in a lower-dimensional space. This approach can exploit the geometry of the task by mapping its regularities onto the topology of the network, thereby shifting problem difficulty away from dimensionality to the underlying problem structure. Furthermore, connective CPPNs can represent the same connectivity pattern at any resolution, allowing ANNs to scale to new numbers of inputs and outputs without further evolution. HyperNEAT is demonstrated through visual discrimination and food-gathering tasks, including successful visual discrimination networks containing over eight million connections. The main conclusion is that the ability to explore the space of regular connectivity patterns opens up a new class of complex high-dimensional tasks to neuroevolution.

**Keywords**: NEAT, neuroevolution, GA

## 2011

### *RELU*: Deep Sparse Rectifier Neural Networks ([paper](http://proceedings.mlr.press/v15/glorot11a/glorot11a.pdf)) ([cited by 4000+](https://scholar.google.com/scholar?cites=10040883758431450991))
By Xavier Glorot, Antoine Bordes, Yoshua Bengio

**tl;dr** Logistic sigmoid neurons are more biologically plausible than hyperbolic tangent neurons and work better for training multi-layer neural networks.

> While logistic sigmoid neurons are more biologically plausible than hyperbolic tangent neurons, the latter work better for training multi-layer neural networks. This paper shows that rectifying neurons are an even better model of biological neurons and yield equal or better performance than hyperbolic tangent networks in spite of the hard non-linearity and non-differentiability at zero, creating sparse representations with true  zeros, which seem remarkably suitable for naturally sparse data.

Keywords: RELU, activation function

## 2012

### *AlexNet*: ImageNet Classification with Deep Convolutional Neural Networks ([paper](https://papers.nips.cc/paper/4824-imagenet-classification-with-deep-convolutional-neural-networks.pdf)) ([cited by 60000+](https://scholar.google.com/scholar?cites=2071317309766942398))
By Alex Krizhevsky, [Ilya Sutskever](https://scholar.google.ca/citations?user=x04W_mMAAAAJ), [Geoffrey E. Hinton](https://scholar.google.ca/citations?hl=en&user=JicYPdAAAAAJ)

**tl;dr** We trained a large, deep convolutional neural network to classify the 1.2 million high-resolution images in the ImageNet LSVRC-2010 contest into the 1000 different classes.

> We trained a large, deep convolutional neural network to classify the 1.2 million high-resolution images in the ImageNet LSVRC-2010 contest into the 1000 different classes. On the test data, we achieved top-1 and top-5 error rates of 37.5% and 17.0% which is considerably better than the previous state-of-the-art. The neural network, which has 60 million parameters and 650,000 neurons, consists of five convolutional layers, some of which are followed by max-pooling layers, and three fully-connected layers with a final 1000-way softmax. To make training faster, we used non-saturating neurons and a very efficient GPU implementation of the convolution operation. To reduce overfitting in the fully-connected layers we employed a recently-developed regularization method called “dropout” that proved to be very effective. We also entered a variant of this model in the ILSVRC-2012 competition and achieved a winning top-5 test error rate of 15.3%, compared to 26.2% achieved by the second-best entry.

**Keywords**: CNN, dropout, regularization

## 2013

### Speech recognition with deep recurrent neural networks ([paper](https://www.academia.edu/download/57926482/10.pdf)) ([cited by 5000+](https://scholar.google.com/scholar?cites=9041586046771167211))
By [Alex Graves](https://scholar.google.com/citations?user=DaFHynwAAAAJ), Abdel-rahman Mohamed, [Geoffrey Hinton](https://scholar.google.ca/citations?hl=en&user=JicYPdAAAAAJ)

**tl;dr** Deep Long Short-term Memory RNNs for phoneme recognition with suitable regularisation.

> Recurrent neural networks (RNNs) are a powerful model for sequential data. End-to-end training methods such as Connectionist Temporal Classification make it possible to train RNNs for sequence labelling problems where the input-output alignment is unknown. The combination of these methods with the Long Short-term Memory RNN architecture has proved particularly fruitful, delivering state-of-the-art results in cursive handwriting recognition. However RNN performance in speech recognition has so far been disappointing, with better results returned by deep feedforward networks. This paper investigates deep recurrent neural networks, which combine the multiple levels of representation that have proved so effective in deep networks with the flexible use of long range context that empowers RNNs. When trained end-to-end with suitable regularisation, we find that deep Long Short-term Memory RNNs achieve a test set error of 17.7% on the TIMIT phoneme recognition benchmark, which to our knowledge is the best recorded score.

**Keywords**: RNN, speech recognition

---

### *NIN*: Network In Network ([arXiv:1312.4400](https://arxiv.org/pdf/1312.4400)) ([cited by 3000+](https://scholar.google.com/scholar?cites=3211704355758672916))
By Min Lin, Qiang Chen, Shuicheng Yan

**tl;dr** We propose a novel deep network structure called Network In Network (NIN) to enhance model discriminability for local patches within the receptive field.

> We propose a novel deep network structure called "Network In Network" (NIN) to enhance model discriminability for local patches within the receptive field. The conventional convolutional layer uses linear filters followed by a nonlinear activation function to scan the input. Instead, we build micro neural networks with more complex structures to abstract the data within the receptive field. We instantiate the micro neural network with a multilayer perceptron, which is a potent function approximator. The feature maps are obtained by sliding the micro networks over the input in a similar manner as CNN; they are then fed into the next layer. Deep NIN can be implemented by stacking mutiple of the above described structure. With enhanced local modeling via the micro network, we are able to utilize global average pooling over feature maps in the classification layer, which is easier to interpret and less prone to overfitting than traditional fully connected layers. We demonstrated the state-of-the-art classification performances with NIN on CIFAR-10 and CIFAR-100, and reasonable performances on SVHN and MNIST datasets.

**Keywords**: network architecture

---

### k-Sparse Autoencoders ([arXiv:1312.5663](https://arxiv.org/pdf/1312.5663)) ([cited by 100+](https://scholar.google.com/scholar?cites=9239915888998917940))
By Alireza Makhzani, Brendan Frey

**tl;dr** We propose a k-sparse autoencoder with linear activation function, where in hidden layers only the k highest activities are kept.

> Recently, it has been observed that when representations are learnt in a way that encourages sparsity, improved performance is obtained on classification tasks. These methods involve combinations of activation functions, sampling steps and different kinds of penalties. To investigate the effectiveness of sparsity by itself, we propose the k-sparse autoencoder, which is an autoencoder with linear activation function, where in hidden layers only the k highest activities are kept. When applied to the MNIST and NORB datasets, we find that this method achieves better classification results than denoising autoencoders, networks trained with dropout, and RBMs. k-sparse autoencoders are simple to train and the encoding stage is very fast, making them well-suited to large problem sizes, where conventional sparse coding algorithms cannot be applied.

**Keywords**: autoencoder

---

### *VAE*: Auto-Encoding Variational Bayes ([arXiv:1312.6114](https://arxiv.org/pdf/1312.6114)) ([cited by 8000+](https://scholar.google.com/scholar?cites=10486756931164834716))
By Diederik P Kingma, Max Welling

**tl;dr** Efficient inference and learning in directed probabilistic models with intractable posterior distributions, and large datasets.

> How can we perform efficient inference and learning in directed probabilistic models, in the presence of continuous latent variables with intractable posterior distributions, and large datasets? We introduce a stochastic variational inference and learning algorithm that scales to large datasets and, under some mild differentiability conditions, even works in the intractable case. Our contributions is two-fold. First, we show that a reparameterization of the variational lower bound yields a lower bound estimator that can be straightforwardly optimized using standard stochastic gradient methods. Second, we show that for i.i.d. datasets with continuous latent variables per datapoint, posterior inference can be made especially efficient by fitting an approximate inference model (also called a recognition model) to the intractable posterior using the proposed lower bound estimator. Theoretical advantages are reflected in experimental results.

**Example code (PyTorch)**: [github.com/pytorch/examples/blob/master/vae/main.py](https://github.com/pytorch/examples/blob/master/vae/main.py)

**Keywords**: variational autoencoder

## 2014

### Deep Symmetry Networks ([paper](http://papers.nips.cc/paper/5424-deep-symmetry-networks)) ([cited by 100+](https://scholar.google.com/scholar?cites=4820202775829919990))
By Robert Gens and [Pedro M. Domingos](https://scholar.google.com/citations?user=KOrhfVMAAAAJ)

**tl;dr** We introduce deep symmetry networks, a generalization of convnets that forms feature maps over arbitrary symmetry groups.

> The chief difficulty in object recognition is that objects' classes are obscured by a large number of extraneous sources of variability, such as pose and part deformation. These sources of variation can be represented by symmetry groups, sets of composable transformations that preserve object identity. Convolutional neural networks (convnets) achieve a degree of translational invariance by computing feature maps over the translation group, but cannot handle other groups. As a result, these groups' effects have to be approximated by small translations, which often requires augmenting datasets and leads to high sample complexity. In this paper, we introduce deep symmetry networks (symnets), a generalization of convnets that forms feature maps over arbitrary symmetry groups. Symnets use kernel-based interpolation to tractably tie parameters and pool over symmetry spaces of any dimension. Like convnets, they are trained with backpropagation. The composition of feature transformations through the layers of a symnet provides a new approach to deep learning. Experiments on NORB and MNIST-rot show that symnets over the affine group greatly reduce sample complexity relative to convnets by better capturing the symmetries in the data.

**Keywords**: network architecture, symmetry, symmetry groups, group theory

---

### *Dropout*: A Simple Way to Prevent Neural Networks from Overfitting ([paper](http://www.jmlr.org/papers/volume15/srivastava14a/srivastava14a.pdf)) ([cited by 10000+](https://scholar.google.com/scholar?cites=17092600409158696067))
By Nitish Srivastava, Geoffrey Hinton, Ilya Sutskever, Ruslan Salakhutdinov

**tl;dr** We show that dropout improves the performance of neural networks on supervised learning tasks in vision, speech recognition, document classification and computational biology.

> Deep neural nets with a large number of parameters are very powerful machine learning systems. However, overfitting is a serious problem in such networks. Large networks are also slow to use, making it difficult to deal with overfitting by combining the predictions of many different large neural nets at test time.  Dropout is a technique for addressing this problem. The key idea is to randomly drop units (along with their connections) from the neural network during training. This prevents units from co-adapting too much. During training, dropout samples from an exponential number of different “thinned” networks. At test time, it is easy to approximate the effect of averaging the predictions of all these thinned networks by simply using a single unthinned network that has smaller weights. This significantly reduces overfitting and gives major improvements over other regularization methods. We show that dropout improves the performance of neural networks on supervised learning tasks in vision, speech recognition, document classification and computational biology, obtaining state-of-the-art results on many benchmark data sets.

**Keywords**: neural networks, regularization, model combination, deep learning

---

### *GAN*: Generative Adversarial Networks ([arXiv:1406.2661](https://arxiv.org/pdf/1406.2661)) ([cited by +10000](https://scholar.google.com/scholar?cites=11977070277539609369))
By Ian J. Goodfellow, Jean Pouget-Abadie, Mehdi Mirza, Bing Xu, David Warde-Farley, Sherjil Ozair, Aaron Courville, Yoshua Bengio

**tl;dr** We propose a new framework for estimating generative models via an adversarial process, in which we simultaneously train two models: a generative model and a discriminative model.

> We propose a new framework for estimating generative models via an adversarial process, in which we simultaneously train two models: a generative model G that captures the data distribution, and a discriminative model D that estimates the probability that a sample came from the training data rather than G. The training procedure for G is to maximize the probability of D making a mistake. This framework corresponds to a minimax two-player game. In the space of arbitrary functions G and D, a unique solution exists, with G recovering the training data distribution and D equal to 1/2 everywhere. In the case where G and D are defined by multilayer perceptrons, the entire system can be trained with backpropagation. There is no need for any Markov chains or unrolled approximate inference networks during either training or generation of samples. Experiments demonstrate the potential of the framework through qualitative and quantitative evaluation of the generated samples.

**Keywords**: GAN, image generation

---

### Robots that can adapt like animals ([arXiv:1407.3501](https://arxiv.org/pdf/1407.3501)) ([cited by 400+](https://scholar.google.com/scholar?cites=8737599281441058737))
By Antoine Cully, [Jeff Clune](https://scholar.google.ca/citations?hl=en&user=5TZ7f5wAAAAJ), Danesh Tarapore, Jean-Baptiste Mouret 

**tl;dr** We introduce an intelligent trial-and-error learning algorithm that allows robots to adapt to damage in less than two minutes in large search spaces.

> Robots have transformed many industries, most notably manufacturing, and have the power to deliver tremendous benefits to society, such as in search and rescue, disaster response, health care and transportation. They are also invaluable tools for scientific exploration in environments inaccessible to humans, from distant planets to deep oceans. A major obstacle to their widespread adoption in more complex environments outside factories is their fragility. Whereas animals can quickly adapt to injuries, current robots cannot ‘think outside the box’ to find a compensatory behaviour when they are damaged: they are limited to their pre-specified self-sensing abilities, can diagnose only anticipated failure modes, and require a pre-programmed contingency plan for every type of potential damage, an impracticality for complex robots. A promising approach to reducing robot fragility involves having robots learn appropriate behaviours in response to damage, but current techniques are slow even with small, constrained search spaces. Here we introduce an intelligent trial-and-error algorithm that allows robots to adapt to damage in less than two minutes in large search spaces without requiring self-diagnosis or pre-specified contingency plans. Before the robot is deployed, it uses a novel technique to create a detailed map of the space of high-performing behaviours. This map represents the robot’s prior knowledge about what behaviours it can perform and their value. When the robot is damaged, it uses this prior knowledge to guide a trial-and-error learning algorithm that conducts intelligent experiments to rapidly discover a behaviour that compensates for the damage. Experiments reveal successful adaptations for a legged robot injured in five different ways, including damaged, broken, and missing legs, and for a robotic arm with joints broken in 14 different ways. This new algorithm will enable more robust, effective, autonomous robots, and may shed light on the principles that animals use to adapt to injury.

**Keywords**: fail-safety


---

### *VGG*: Very Deep Convolutional Networks for Large-Scale Image Recognition ([arXiv:1409.1556](https://arxiv.org/pdf/1409.1556)) ([cited by 30000+](https://scholar.google.com/scholar?cites=15993525775437884507))
By Karen Simonyan, Andrew Zisserman

**tl;dr** In this work we investigate the effect of the convolutional network depth on its accuracy in the large-scale image recognition setting.

> In this work we investigate the effect of the convolutional network depth on its accuracy in the large-scale image recognition setting. Our main contribution is a thorough evaluation of networks of increasing depth using an architecture with very small (3x3) convolution filters, which shows that a significant improvement on the prior-art configurations can be achieved by pushing the depth to 16-19 weight layers. These findings were the basis of our ImageNet Challenge 2014 submission, where our team secured the first and the second places in the localisation and classification tracks respectively. We also show that our representations generalise well to other datasets, where they achieve state-of-the-art results. We have made our two best-performing ConvNet models publicly available to facilitate further research on the use of deep visual representations in computer vision.

**Keywords**: CNN

---

### Neural Turing Machines ([arXiv:1410.5401](https://arxiv.org/pdf/1410.5401)) ([cited by 1000+](https://scholar.google.com/scholar?cites=8413767524552071308))
By Alex Graves, Greg Wayne, Ivo Danihelka

**tl;dr** We extend the capabilities of neural networks by coupling them to external memory resources, which they can interact with by attentional processes.

> We extend the capabilities of neural networks by coupling them to external memory resources, which they can interact with by attentional processes. The combined system is analogous to a Turing Machine or Von Neumann architecture but is differentiable end-to-end, allowing it to be efficiently trained with gradient descent. Preliminary results demonstrate that Neural Turing Machines can infer simple algorithms such as copying, sorting, and associative recall from input and output examples.

**Keywords**: network architecture, MANN

## 2015

### Human-level control through deep reinforcement learning ([paper](https://daiwk.github.io/assets/dqn.pdf)) ([cited by 9000+](https://scholar.google.com/scholar?cites=12439121588427761338))
By Volodymyr Mnih, Koray Kavukcuoglu, [David Silver](https://scholar.google.com/citations?user=-8DNE4UAAAAJ), Andrei A. Rusu, Joel Veness, Marc G. Bellemare, [Alex Graves](https://scholar.google.com/citations?user=DaFHynwAAAAJ), Martin Riedmiller, Andreas K. Fidjeland, Georg Ostrovski, Stig Petersen, Charles Beattie, Amir Sadik, Ioannis Antonoglou, Helen King, Dharshan Kumaran, Daan Wierstra, Shane Legg, Demis Hassabis

**tl;dr** A deep Q-network that can learn successful policies directly from high-dimensional sensory inputs using end-to-end reinforcement learning.

> The theory of reinforcement learning provides a normative account, deeply rooted in psychological and neuroscientific perspectives on animal behaviour, of how agents may optimize their control of an environment. To use reinforcement learning successfully in situations approaching real-world complexity, however, agents are confronted with a difficult task: they must derive efficient representations of the environment from high-dimensional sensory inputs, and use these to generalize past experience to new situations. Remarkably, humans and other animals seem to solve this problem through a harmonious combination of reinforcement learning and hierarchical sensory processing systems, the former evidenced by a wealth of neural data revealing notable parallels between the phasic signals emitted by dopaminergic neurons and temporal difference reinforcement learning algorithms. While reinforcement learning agents have achieved some successes in a variety of domains, their applicability has previously been limited to domains in which useful features can be handcrafted, or to domains with fully observed, low-dimensional state spaces. Here we use recent advances in training deep neural networks to develop a novel artificial agent, termed a deep Q-network, that can learn successful policies directly from high-dimensional sensory inputs using end-to-end reinforcement learning. We tested this agent on the challenging domain of classic Atari 2600 games. We demonstrate that the deep Q-network agent, receiving only the pixels and the game score as inputs, was able to surpass the performance of all previous algorithms and achieve a level comparable to that of a professional human games tester across a set of 49 games, using the same algorithm, network architecture and hyperparameters. This work bridges the divide between high-dimensional sensory inputs and actions, resulting in the first artificial agent that is capable of learning to excel at a diverse array of challenging tasks.

**Keywords**: RL, deep Q learning

---

### Show, Attend and Tell: Neural Image Caption Generation with Visual Attention ([arXiv:1502.03044](https://arxiv.org/pdf/1502.03044)) ([cited by 4000+](https://scholar.google.com/scholar?cites=9471583366007765258))
By Kelvin Xu, Jimmy Ba, Ryan Kiros, Kyunghyun Cho, Aaron Courville, Ruslan Salakhutdinov, Richard Zemel, Yoshua Bengio

**tl;dr** We introduce an attention based model that automatically learns to describe the content of images using a variational lower bound.

> Inspired by recent work in machine translation and object detection, we introduce an attention based model that automatically learns to describe the content of images. We describe how we can train this model in a deterministic manner using standard backpropagation techniques and stochastically by maximizing a variational lower bound. We also show through visualization how the model is able to automatically learn to fix its gaze on salient objects while generating the corresponding words in the output sequence. We validate the use of attention with state-of-the-art performance on three benchmark datasets: Flickr8k, Flickr30k and MS COCO.

**Keywords**: attention

---

### DRAW: A Recurrent Neural Network For Image Generation ([arXiv:1502.04623](https://arxiv.org/pdf/1502.04623)) ([cited by 1000+](https://scholar.google.com/scholar?cites=8022513888710268841))
By Karol Gregor, Ivo Danihelka, Alex Graves, Danilo Jimenez Rezende, Daan Wierstra

**tl;dr** This paper introduces the Deep Recurrent Attentive Writer (DRAW) neural network architecture for image generation.

> This paper introduces the Deep Recurrent Attentive Writer (DRAW) neural network architecture for image generation. DRAW networks combine a novel spatial attention mechanism that mimics the foveation of the human eye, with a sequential variational auto-encoding framework that allows for the iterative construction of complex images. The system substantially improves on the state of the art for generative models on MNIST, and, when trained on the Street View House Numbers dataset, it generates images that cannot be distinguished from real data with the naked eye.

**Keywords**: RNN, image generation

---

### End-To-End Memory Networks ([arXiv:1503.08895](https://arxiv.org/pdf/1503.08895)) ([cited by 1000+](https://scholar.google.com/scholar?cites=9907515383987281804))
By Sainbayar Sukhbaatar, Arthur Szlam, Jason Weston, Rob Fergus

**tl;dr** We introduce a neural network with a recurrent attention model over a possibly large external memory.

> We introduce a neural network with a recurrent attention model over a possibly large external memory. The architecture is a form of Memory Network (Weston et al., 2015) but unlike the model in that work, it is trained end-to-end, and hence requires significantly less supervision during training, making it more generally applicable in realistic settings. It can also be seen as an extension of RNNsearch to the case where multiple computational steps (hops) are performed per output symbol. The flexibility of the model allows us to apply it to tasks as diverse as (synthetic) question answering and to language modeling. For the former our approach is competitive with Memory Networks, but with less supervision. For the latter, on the Penn TreeBank and Text8 datasets our approach demonstrates comparable performance to RNNs and LSTMs. In both cases we show that the key concept of multiple computational hops yields improved results.

**Keywords**: MANN

---

### *HighwayNet*: Highway networks ([arXiv:1505.00387](https://arxiv.org/pdf/1505.00387)) ([cited by 1000+](https://scholar.google.com/scholar?cites=5320226408800263727))
By Rupesh Kumar Srivastava, Klaus Greff, [Jürgen Schmidhuber](https://scholar.google.com/citations?user=gLnCTgIAAAAJ)

**tl;dr** We introduce a new architecture designed to ease gradient-based training of very deep networks, which learn to regulate the flow of information through a network.

> There is plenty of theoretical and empirical evidence that depth of neural networks is a crucial ingredient for their success. However, network training becomes more difficult with increasing depth and training of very deep networks remains an open problem. In this extended abstract, we introduce a new architecture designed to ease gradient-based training of very deep networks. We refer to networks with this architecture as highway networks, since they allow unimpeded information flow across several layers on "information highways". The architecture is characterized by the use of gating units which learn to regulate the flow of information through a network. Highway networks with hundreds of layers can be trained directly using stochastic gradient descent and with a variety of activation functions, opening up the possibility of studying extremely deep and efficient architectures.

**Keywords**: network architecture

---

### *Spatial Transformer*: Spatial Transformer Networks ([arXiv:1506.02025](https://arxiv.org/pdf/1506.02025)) ([cited by 2000+](https://scholar.google.com/scholar?cites=1662293494062093494))
By Max Jaderberg, Karen Simonyan, Andrew Zisserman, Koray Kavukcuoglu

**tl;dr** We introduce a new learnable module, the Spatial Transformer, which explicitly allows the spatial manipulation of data within the network.

> Convolutional Neural Networks define an exceptionally powerful class of models, but are still limited by the lack of ability to be spatially invariant to the input data in a computationally and parameter efficient manner. In this work we introduce a new learnable module, the Spatial Transformer, which explicitly allows the spatial manipulation of data within the network. This differentiable module can be inserted into existing convolutional architectures, giving neural networks the ability to actively spatially transform feature maps, conditional on the feature map itself, without any extra training supervision or modification to the optimisation process. We show that the use of spatial transformers results in models which learn invariance to translation, scale, rotation and more generic warping, resulting in state-of-the-art performance on several benchmarks, and for a number of classes of transformations.

**Keywords**: affine transformation

---

### Teaching Machines to Read and Comprehend ([arXiv:1506.03340](https://arxiv.org/pdf/1506.03340)) ([cited by 1000+](https://scholar.google.com/scholar?cites=5371787459515302436))
By Karl Moritz Hermann, Tomáš Kočiský, Edward Grefenstette, Lasse Espeholt, Will Kay, Mustafa Suleyman, Phil Blunsom

**tl;dr** Large scale supervised reading comprehension data for machine reading systems.

> Teaching machines to read natural language documents remains an elusive challenge. Machine reading systems can be tested on their ability to answer questions posed on the contents of documents that they have seen, but until now large scale training and test datasets have been missing for this type of evaluation. In this work we define a new methodology that resolves this bottleneck and provides large scale supervised reading comprehension data. This allows us to develop a class of attention based deep neural networks that learn to read real documents and answer complex questions with minimal prior knowledge of language structure.

**Keywords**: NLP

---

### *Neural Style Transfer*: A Neural Algorithm of Artistic Style ([arXiv:1508.06576](https://arxiv.org/pdf/1508.06576)) ([cited by 1000+](https://scholar.google.com/scholar?cites=6343685530593283491))
By Leon A. Gatys, Alexander S. Ecker, Matthias Bethge

**tl;dr** A neural algorithm for the creation of artistic images of high perceptual quality.

> In fine art, especially painting, humans have mastered the skill to create unique visual experiences through composing a complex interplay between the content and style of an image. Thus far the algorithmic basis of this process is unknown and there exists no artificial system with similar capabilities. However, in other key areas of visual perception such as object and face recognition near-human performance was recently demonstrated by a class of biologically inspired vision models called Deep Neural Networks. Here we introduce an artificial system based on a Deep Neural Network that creates artistic images of high perceptual quality. The system uses neural representations to separate and recombine content and style of arbitrary images, providing a neural algorithm for the creation of artistic images.

**Keywords**: neural style transfer, CNN

---

### Design and Implementation of Multi-dimensional Flexible Antena-like Hair motivated by ’Aho-Hair’ in Japanese Anime Cartoons: Internal State Expressions beyond Design Limitations ([paper](https://spqrchan.xyz/.media/e9c9210fb1b8f194b82f65cd952c6c43-applicationpdf.pdf))
By Kazuhiro Sasabuchi, Yohei Kakiuchi, Kei Okada and Masayuki Inaba

**tl;dr** A non-facial contextual expression for emotion-expressive and interactive humanoid robots.

> Recent research in psychology argue the importance of “context” in  emotion perception. According to these recent studies, facial expressions do not possess discrete emotional meanings; rather the meaning depends on the social situation of how and when the expressions are used. These research results imply that the emotion expressivity depends on the appropriate combination of context and expression, and not the distinctiveness of the expressions themselves. Therefore, it is inferable that relying on facial expressions may not be essential. Instead, when appropriate pairs of  context and expression are applied, emotional internal states  perhaps emerge. This paper first discusses how facial expressions of robots limit their head design, and can be hardware costly. Then, the paper proposes a way of expressing context-based emotions as an alternative to facial expressions. The paper introduces the mechanical structure for applying a specific non-facial contextual expression. The expression was originated from Japanese animation, and the mechanism was applied to a real desktop size humanoid robot. Finally, an experiment on whether the contextual expression is capable of linking humanoid motions and its emotional internal states was conducted under a sound-context condition. Although the results are limited in cultural aspects, this paper presents the possibilities  of future robotic interface for emotion-expressive and interactive humanoid robots.

**Keywords**: robotics, emotional expression

---

### Continuous control with deep reinforcement learning ([arXiv:1509.02971](https://arxiv.org/pdf/1509.02971)) ([cited by 3000+](https://scholar.google.com/scholar?cites=4133004576987558805))
By Timothy P. Lillicrap, Jonathan J. Hunt, Alexander Pritzel, Nicolas Heess, Tom Erez, Yuval Tassa, [David Silver](https://scholar.google.com/citations?user=-8DNE4UAAAAJ), Daan Wierstra

**tl;dr** We present an actor-critic, model-free algorithm based on the deterministic policy gradient that can operate over continuous action spaces.

> We adapt the ideas underlying the success of Deep Q-Learning to the continuous action domain. We present an actor-critic, model-free algorithm based on the deterministic policy gradient that can operate over continuous action spaces. Using the same learning algorithm, network architecture and hyper-parameters, our algorithm robustly solves more than 20 simulated physics tasks, including classic problems such as cartpole swing-up, dexterous manipulation, legged locomotion and car driving. Our algorithm is able to find policies whose performance is competitive with those found by a planning algorithm with full access to the dynamics of the domain and its derivatives. We further demonstrate that for many of the tasks the algorithm can learn policies end-to-end: directly from raw pixel inputs.

**Keywords**: deep Q learning, RL

---

### Deep Compression: Compressing Deep Neural Networks with Pruning, Trained Quantization and Huffman Coding ([arXiv:1510.00149](https://arxiv.org/pdf/1510.00149)) ([cited by 3000+](https://scholar.google.com/scholar?cites=7860777411990654691))
By Song Han, Huizi Mao, William J. Dally

**tl;dr** Deep compression reduces the storage requirement of neural networks by 35x to 49x without affecting their accuracy.

> Neural networks are both computationally intensive and memory intensive, making them difficult to deploy on embedded systems with limited hardware resources. To address this limitation, we introduce "deep compression", a three stage pipeline: pruning, trained quantization and Huffman coding, that work together to reduce the storage requirement of neural networks by 35x to 49x without affecting their accuracy. Our method first prunes the network by learning only the important connections. Next, we quantize the weights to enforce weight sharing, finally, we apply Huffman coding. After the first two steps we retrain the network to fine tune the remaining connections and the quantized centroids. Pruning, reduces the number of connections by 9x to 13x; Quantization then reduces the number of bits that represent each connection from 32 to 5. On the ImageNet dataset, our method reduced the storage required by AlexNet by 35x, from 240MB to 6.9MB, without loss of accuracy. Our method reduced the size of VGG-16 by 49x from 552MB to 11.3MB, again with no loss of accuracy. This allows fitting the model into on-chip SRAM cache rather than off-chip DRAM memory. Our compression method also facilitates the use of complex neural networks in mobile applications where application size and download bandwidth are constrained. Benchmarked on CPU, GPU and mobile GPU, compressed network has 3x to 4x layerwise speedup and 3x to 7x better energy efficiency.

**Keywords**: compression

---

### Unsupervised Representation Learning with Deep Convolutional Generative Adversarial Networks ([arXiv:1511.06434](https://arxiv.org/pdf/1511.06434)) ([cited by 6000+](https://scholar.google.com/scholar?cites=3321343160055675528))
By Alec Radford, Luke Metz, Soumith Chintala

**tl;dr** DCGANs: Deep Convolutional Generative Adversarial Networks for unsupervised learning.

> In recent years, supervised learning with convolutional networks (CNNs) has seen huge adoption in computer vision applications. Comparatively, unsupervised learning with CNNs has received less attention. In this work we hope to help bridge the gap between the success of CNNs for supervised learning and unsupervised learning. We introduce a class of CNNs called deep convolutional generative adversarial networks (DCGANs), that have certain architectural constraints, and demonstrate that they are a strong candidate for unsupervised learning. Training on various image datasets, we show convincing evidence that our deep convolutional adversarial pair learns a hierarchy of representations from object parts to scenes in both the generator and discriminator. Additionally, we use the learned features for novel tasks - demonstrating their applicability as general image representations.

**Keywords**: CNN, GAN

---

### Fast and Accurate Deep Network Learning by Exponential Linear Units (ELUs) ([arXiv:1511.07289](https://arxiv.org/pdf/1511.07289)) ([cited by 2000+](https://scholar.google.com/scholar?cites=13103600599147838489))
By Djork-Arné Clevert, Thomas Unterthiner, [Sepp Hochreiter](https://scholar.google.com/citations?user=tvUH3WMAAAAJ)

**tl;dr** We introduce the "exponential linear unit" (ELU) which speeds up learning in deep neural networks and leads to higher classification accuracies.

> We introduce the "exponential linear unit" (ELU) which speeds up learning in deep neural networks and leads to higher classification accuracies. Like rectified linear units (ReLUs), leaky ReLUs (LReLUs) and parametrized ReLUs (PReLUs), ELUs alleviate the vanishing gradient problem via the identity for positive values. However, ELUs have improved learning characteristics compared to the units with other activation functions. In contrast to ReLUs, ELUs have negative values which allows them to push mean unit activations closer to zero like batch normalization but with lower computational complexity. Mean shifts toward zero speed up learning by bringing the normal gradient closer to the unit natural gradient because of a reduced bias shift effect. While LReLUs and PReLUs have negative values, too, they do not ensure a noise-robust deactivation state. ELUs saturate to a negative value with smaller inputs and thereby decrease the forward propagated variation and information. Therefore, ELUs code the degree of presence of particular phenomena in the input, while they do not quantitatively model the degree of their absence. In experiments, ELUs lead not only to faster learning, but also to significantly better generalization performance than ReLUs and LReLUs on networks with more than 5 layers. On CIFAR-100 ELUs networks significantly outperform ReLU networks with batch normalization while batch normalization does not improve ELU networks. ELU networks are among the top 10 reported CIFAR-10 results and yield the best published result on CIFAR-100, without resorting to multi-view evaluation or model averaging. On ImageNet, ELU networks considerably speed up learning compared to a ReLU network with the same architecture, obtaining less than 10% classification error for a single crop, single model network.

**Keywords**: activation function

---

### *ResNet*: Deep Residual Learning for Image Recognition ([arXiv:1512.03385](https://arxiv.org/pdf/1512.03385)) ([cited by 45000+](https://scholar.google.com/scholar?cites=9281510746729853742))
By [Kaiming He](https://scholar.google.com/citations?user=DhtAFkwAAAAJ), [Xiangyu Zhang](https://scholar.google.com/citations?user=yuB-cfoAAAAJ), [Shaoqing Ren](https://scholar.google.com/citations?user=AUhj438AAAAJ), Jian Sun

**tl;dr** We present a residual learning framework to ease the training of networks that are substantially deeper than those used previously.

> Deeper neural networks are more difficult to train. We present a residual learning framework to ease the training of networks that are substantially deeper than those used previously. We explicitly reformulate the layers as learning residual functions with reference to the layer inputs, instead of learning unreferenced functions. We provide comprehensive empirical evidence showing that these residual networks are easier to optimize, and can gain accuracy from considerably increased depth. On the ImageNet dataset we evaluate residual nets with a depth of up to 152 layers---
8x deeper than VGG nets but still having lower complexity. An ensemble of these residual nets achieves 3.57% error on the ImageNet test set. This result won the 1st place on the ILSVRC 2015 classification task. We also present analysis on CIFAR-10 with 100 and 1000 layers.

**Keywords**: ResNet

## 2016

### Interactive Sound Propagation with Bidirectional Path Tracing ([paper](https://dl.acm.org/doi/pdf/10.1145/2980179.2982431)) ([cited by 30+](https://scholar.google.com/scholar?cites=4970171412964785325))
By Chunxiao Cao, Zhong Ren, Carl Schissler, Dinesh Manocha, Kun Zhou

**tl;dr** We introduce Bidirectional Sound Transport (BST), a new algorithm that simulates sound propagation by bidirectional path tracing using multiple importance sampling.

> We introduce Bidirectional Sound Transport (BST), a new algorithm that simulates sound propagation by bidirectional path tracing using multiple importance sampling. Our approach can handle multiple sources in large virtual environments with complex occlusion, and can produce plausible acoustic effects at an interactive rate on a desktop PC. We introduce a new metric based on the signalto-noise ratio (SNR) of the energy response and use this metric to evaluate the performance of ray-tracing-based acoustic simulation methods. Our formulation exploits temporal coherence in terms of using the resulting sample distribution of the previous frame to guide the sample distribution of the current one. We show that our sample redistribution algorithm converges and better balances between early and late reflections. We evaluate our approach on different benchmarks and demonstrate significant speedup over prior geometric acoustic algorithms.

Video: [Sound Propagation With Bidirectional Path Tracing | Two Minute Papers #111](https://www.youtube.com/watch?v=DzsZ2qMtEUE)

---

### Taking the Human Out of the Loop: A Review of Bayesian Optimization ([paper](https://www.cs.princeton.edu/~rpa/pubs/shahriari2016loop.pdf)) ([cited by 1000+](https://scholar.google.com/scholar?cites=2039456143890648437))
By Bobak Shahriari, Kevin Swersky, Ziyu Wang, Ryan P. Adams, Nando de Freitas

**tl;dr** Bayesian Optimization for Big Data Applications: A Review

> Big Data applications are typically associated with systems involving large numbers of users, massive complex software systems, and large-scale heterogeneous computing and storage architectures. The construction of such systems involves many distributed design choices. The end products (e.g., recommendation systems, medical analysis tools, real-time game engines, speech recognizers) thus involve many tunable configuration parameters. These parameters are often specified and hard-coded into the software by various developers or teams. If optimized jointly, these parameters can result in significant improvements. Bayesian optimization is a powerful tool for the joint optimization of design choices that is gaining great popularity in recent years. It promises greater automation so as to increase both product quality and human productivity. This review paper introduces Bayesian optimization, highlights some of its methodological aspects, and showcases a wide range of applications.

**Keywords**: Bayesian, meta-learning

---

### *AlphaGo*: Mastering the game of Go with deep neural networks and tree search ([paper](https://www.cs.cmu.edu/afs/cs.cmu.edu/academic/class/15780-s16/www/AlphaGo.nature16961.pdf)) ([cited by 7000+](https://scholar.google.com/scholar?cites=300412370207407505))
By [David Silver](https://scholar.google.com/citations?user=-8DNE4UAAAAJ), Aja Huang, Chris J. Maddison, Arthur Guez, Laurent Sifre, George van den Driessche,  Julian Schrittwieser, Ioannis Antonoglou, Veda Panneershelvam, Marc Lanctot, Sander Dieleman, Dominik Grewe, John Nham, Nal Kalchbrenner, [Ilya Sutskever](https://scholar.google.ca/citations?user=x04W_mMAAAAJ), [Timothy Lillicrap](https://scholar.google.ca/citations?user=htPVdRMAAAAJ), Madeleine Leach, Koray Kavukcuoglu,  Thore Graepel, Demis Hassabis

**tl;dr** We train deep neural networks to play Go at the level of Monte Carlo tree search programs that simulate thousands of random games of self-play.

> The game of Go has long been viewed as the most challenging of classic games for artificial intelligence owing to its enormous search space and the difficulty of evaluating board positions and moves. Here we introduce a new approach to computer Go that uses ‘value networks’ to evaluate board positions and ‘policy networks’ to select moves. These deep neural networks are trained by a novel combination of supervised learning from human expert games, and reinforcement learning from games of self-play. Without any lookahead search, the neural networks play Go at the level of state-of-the-art Monte Carlo tree search programs that simulate thousands of random games of self-play. We also introduce a new search algorithm that combines Monte Carlo simulation with value and policy networks. Using this search algorithm, our program AlphaGo achieved a 99.8% winning rate against other Go programs, and defeated the human European Go champion by 5 games to 0. This is the first time that a computer program has defeated a human professional player in the full-sized game of Go, a feat previously thought to be at least a decade away.

**Keywords**: RL, MCTS

---

### How a life-like system emerges from a simplistic particle motion law ([paper](https://www.nature.com/articles/srep37969)) ([cited by 10+](https://scholar.google.com/scholar?cites=1093018564468047343))
By Thomas Schmickl, Martin Stefanec, Karl Crailsheim

**tl;dr** We discovered a self-structuring, self-reproducing and self-sustaining life-like system.

> Self-structuring patterns can be observed all over the universe, from galaxies to molecules to living matter, yet their emergence is waiting for full understanding. We discovered a simple motion law for moving and interacting self-propelled particles leading to a self-structuring, self-reproducing and self-sustaining life-like system. The patterns emerging within this system resemble patterns found in living organisms. The emergent cells we found show a distinct life cycle and even create their own ecosystem from scratch. These structures grow and reproduce on their own, show self-driven behavior and interact with each other. Here we analyze the macroscopic properties of the emerging ecology, as well as the microscopic properties of the mechanism that leads to it. Basic properties of the emerging structures (size distributions, longevity) are analyzed as well as their resilience against sensor or actuation noise. Finally, we explore parameter space for potential other candidates of life. The generality and simplicity of the motion law provokes the thought that one fundamental rule, described by one simple equation yields various structures in nature: it may work on different time- and size scales, ranging from the self-structuring universe, to emergence of living beings, down to the emergent subatomic formation of matter.

**Video demo**: [Youtube](https://www.youtube.com/watch?v=makaJpLvbow)

**Keywords**: emergent behavior

---

### *A3C*: Asynchronous Methods for Deep Reinforcement Learning ([arXiv:1602.01783](https://arxiv.org/pdf/1602.01783)) ([cited by 3000+](https://scholar.google.com/scholar?cites=14460380466928185185))
By Volodymyr Mnih, Adrià Puigdomènech Badia, Mehdi Mirza, [Alex Graves](https://scholar.google.com/citations?user=DaFHynwAAAAJ), [Timothy P. Lillicrap](https://scholar.google.ca/citations?user=htPVdRMAAAAJ), Tim Harley, [David Silver](https://scholar.google.com/citations?user=-8DNE4UAAAAJ), Koray Kavukcuoglu

**tl;dr** We propose a conceptually simple and lightweight framework for deep reinforcement learning that uses asynchronous gradient descent for optimization of deep neural network controllers.

> We propose a conceptually simple and lightweight framework for deep reinforcement learning that uses asynchronous gradient descent for optimization of deep neural network controllers. We present asynchronous variants of four standard reinforcement learning algorithms and show that parallel actor-learners have a stabilizing effect on training allowing all four methods to successfully train neural network controllers. The best performing method, an asynchronous variant of actor-critic, surpasses the current state-of-the-art on the Atari domain while training for half the time on a single multi-core CPU instead of a GPU. Furthermore, we show that asynchronous actor-critic succeeds on a wide variety of continuous motor control problems as well as on a new task of navigating random 3D mazes using a visual input.

**Keywords**: RL, A3C

---

### Binarized Neural Networks: Training Deep Neural Networks with Weights and Activations Constrained to +1 or -1 ([arXiv:1602.02830](https://arxiv.org/pdf/1602.02830)) ([cited by 1000+](https://scholar.google.com/scholar?cites=3285803655136066432))
By Matthieu Courbariaux, Itay Hubara, Daniel Soudry, Ran El-Yaniv, Yoshua Bengio

**tl;dr** We introduce a method to train Binarized Neural Networks (BNNs) with binary weights and activations at run-time.

> We introduce a method to train Binarized Neural Networks (BNNs) - neural networks with binary weights and activations at run-time. At training-time the binary weights and activations are used for computing the parameters gradients. During the forward pass, BNNs drastically reduce memory size and accesses, and replace most arithmetic operations with bit-wise operations, which is expected to substantially improve power-efficiency. To validate the effectiveness of BNNs we conduct two sets of experiments on the Torch7 and Theano frameworks. On both, BNNs achieved nearly state-of-the-art results over the MNIST, CIFAR-10 and SVHN datasets. Last but not least, we wrote a binary matrix multiplication GPU kernel with which it is possible to run our MNIST BNN 7 times faster than with an unoptimized GPU kernel, without suffering any loss in classification accuracy. The code for training and running our BNNs is available on-line.

**Keywords**: network architecture

---

### *SqueezeNet*: AlexNet-level accuracy with 50x fewer parameters and <0.5MB model size ([arXiv:1602.07360](https://arxiv.org/pdf/1602.07360)) ([cited by 2000+](https://scholar.google.com/scholar?cites=17131899958223648583))
By Forrest N. Iandola, Song Han, Matthew W. Moskewicz, Khalid Ashraf, William J. Dally, Kurt Keutzer

**tl;dr** We propose a small DNN architecture called SqueeNet that achieves AlexNet-level accuracy on ImageNet with 50x fewer parameters.

> Recent research on deep neural networks has focused primarily on improving accuracy. For a given accuracy level, it is typically possible to identify multiple DNN architectures that achieve that accuracy level. With equivalent accuracy, smaller DNN architectures offer at least three advantages: (1) Smaller DNNs require less communication across servers during distributed training. (2) Smaller DNNs require less bandwidth to export a new model from the cloud to an autonomous car. (3) Smaller DNNs are more feasible to deploy on FPGAs and other hardware with limited memory. To provide all of these advantages, we propose a small DNN architecture called SqueezeNet. SqueezeNet achieves AlexNet-level accuracy on ImageNet with 50x fewer parameters. Additionally, with model compression techniques we are able to compress SqueezeNet to less than 0.5MB (510x smaller than AlexNet).

**Source code**: [github.com/DeepScale/SqueezeNet](https://github.com/DeepScale/SqueezeNet)

**Keywords**: compression

---

### *PELU*: Parametric Exponential Linear Unit for Deep Convolutional Neural Networks ([arXiv:1605.09332](https://arxiv.org/pdf/1605.09332)) ([cited by 80+](https://scholar.google.com/scholar?cites=15166150093841301194))
By Ludovic Trottier, Philippe Giguère, Brahim Chaib-draa

**tl;dr** We propose learning a parameterization of ELU in order to learn the proper activation shape at each layer in the CNNs.

> Object recognition is an important task for improving the ability of visual systems to perform complex scene understanding. Recently, the Exponential Linear Unit (ELU) has been proposed as a key component for managing bias shift in Convolutional Neural Networks (CNNs), but defines a parameter that must be set by hand. In this paper, we propose learning a parameterization of ELU in order to learn the proper activation shape at each layer in the CNNs. Our results on the MNIST, CIFAR-10/100 and ImageNet datasets using the NiN, Overfeat, All-CNN and ResNet networks indicate that our proposed Parametric ELU (PELU) has better performances than the non-parametric ELU. We have observed as much as a 7.28% relative error improvement on ImageNet with the NiN network, with only 0.0003% parameter increase.

**Keywords**: PELU, ELU, RELU, activation function


---

### *DenseNet*: Densely Connected Convolutional Networks ([arXiv:1608.06993](https://arxiv.org/pdf/1608.06993)) ([cited by 8000+](https://scholar.google.com/scholar?cites=4205512852566836101))
By Gao Huang, Zhuang Liu, Laurens van der Maaten, Kilian Q. Weinberger

**tl;dr** We introduce the Dense Convolutional Network (DenseNet), which connects each layer to every other layer in a feed-forward fashion.

> Recent work has shown that convolutional networks can be substantially deeper, more accurate, and efficient to train if they contain shorter connections between layers close to the input and those close to the output. In this paper, we embrace this observation and introduce the Dense Convolutional Network (DenseNet), which connects each layer to every other layer in a feed-forward fashion. Whereas traditional convolutional networks with L layers have L connections - one between each layer and its subsequent layer - our network has L(L+1)/2 direct connections. For each layer, the feature-maps of all preceding layers are used as inputs, and its own feature-maps are used as inputs into all subsequent layers. DenseNets have several compelling advantages: they alleviate the vanishing-gradient problem, strengthen feature propagation, encourage feature reuse, and substantially reduce the number of parameters.

**Keywords**: ResNet, DenseNet, CNN

---

### *Layer Normalization* ([arXiv:1607.06450](https://arxiv.org/pdf/1607.06450)) ([cited by 1000+](https://scholar.google.com/scholar?cites=4160792425577293394))
By Jimmy Lei Ba, Jamie Ryan Kiros, Geoffrey E. Hinton

**tl;dr** We apply batch normalization to recurrent neural networks and we show that layer normalization can substantially reduce the training time compared with previously published techniques.

> Training state-of-the-art, deep neural networks is computationally expensive. One way to reduce the training time is to normalize the activities of the neurons. A recently introduced technique called batch normalization uses the distribution of the summed input to a neuron over a mini-batch of training cases to compute a mean and variance which are then used to normalize the summed input to that neuron on each training case. This significantly reduces the training time in feed-forward neural networks. However, the effect of batch normalization is dependent on the mini-batch size and it is not obvious how to apply it to recurrent neural networks. In this paper, we transpose batch normalization into layer normalization by computing the mean and variance used for normalization from all of the summed inputs to the neurons in a layer on a single training case. Like batch normalization, we also give each neuron its own adaptive bias and gain which are applied after the normalization but before the non-linearity. Unlike batch normalization, layer normalization performs exactly the same computation at training and test times. It is also straightforward to apply to recurrent neural networks by computing the normalization statistics separately at each time step. Layer normalization is very effective at stabilizing the hidden state dynamics in recurrent networks. Empirically, we show that layer normalization can substantially reduce the training time compared with previously published techniques.

**Keywords**: normalization

---

### *Instance Normalization*: The Missing Ingredient for Fast Stylization ([arXiv:1607.08022](https://arxiv.org/pdf/1607.08022)) ([cited by 700+](https://scholar.google.com/scholar?cites=7452685526525981959))
By Dmitry Ulyanov, Andrea Vedaldi, Victor Lempitsky

**tl;dr** Fast Stylization with Instance Normalization for Real-time Image Generation.

> It this paper we revisit the fast stylization method introduced in Ulyanov et. al. (2016). We show how a small change in the stylization architecture results in a significant qualitative improvement in the generated images. The change is limited to swapping batch normalization with instance normalization, and to apply the latter both at training and testing times. The resulting method can be used to train high-performance architectures for real-time image generation.

**Github**: [github.com/DmitryUlyanov/texture_nets](https://github.com/DmitryUlyanov/texture_nets)

**Keywords**: instance normalization, neural style transfer

---

### 🌟 Learning to learn with backpropagation of Hebbian plasticity ([arXiv:1609.02228](https://arxiv.org/pdf/1609.02228)) ([cited by 3+](https://scholar.google.com/scholar?cites=13291537822698411280))
By [Thomas Miconi](https://scholar.google.ca/citations?hl=en&user=EXun8woAAAAJ)

**tl;dr** Backpropagation of Hebbian plasticity offers a powerful model for lifelong learning.

> Hebbian plasticity is a powerful principle that allows biological brains to learn from their lifetime experience. By contrast, artificial neural networks trained with backpropagation generally have fixed connection weights that do not change once training is complete. While recent methods can endow neural networks with long-term memories, Hebbian plasticity is currently not amenable to gradient descent. Here we derive analytical expressions for activity gradients in neural networks with Hebbian plastic connections. Using these expressions, we can use backpropagation to train not just the baseline weights of the connections, but also their plasticity. As a result, the networks "learn how to learn" in order to solve the problem at hand: the trained networks automatically perform fast learning of unpredictable environmental features during their lifetime, expanding the range of solvable problems. We test the algorithm on various on-line learning tasks, including pattern completion, one-shot learning, and reversal learning. The algorithm successfully learns how to learn the relevant associations from one-shot instruction, and fine-tunes the temporal dynamics of plasticity to allow for continual learning in response to changing environmental parameters. We conclude that backpropagation of Hebbian plasticity offers a powerful model for lifelong learning.

**Keywords**: meta-learning, Hebbian plasticity

---

### *WaveNet*: A Generative Model for Raw Audio ([arXiv:1609.03499](https://arxiv.org/pdf/1609.03499)) ([cited by 1000+](https://scholar.google.com/scholar?cites=18335624689534049212))
By Aaron van den Oord, Sander Dieleman, Heiga Zen, Karen Simonyan, Oriol Vinyals, Alex Graves, Nal Kalchbrenner, Andrew Senior, Koray Kavukcuoglu

**tl;dr** This paper introduces WaveNet, a deep neural network for generating raw audio waveforms for text-to-speech and music.

> This paper introduces WaveNet, a deep neural network for generating raw audio waveforms. The model is fully probabilistic and autoregressive, with the predictive distribution for each audio sample conditioned on all previous ones; nonetheless we show that it can be efficiently trained on data with tens of thousands of samples per second of audio. When applied to text-to-speech, it yields state-of-the-art performance, with human listeners rating it as significantly more natural sounding than the best parametric and concatenative systems for both English and Mandarin. A single WaveNet can capture the characteristics of many different speakers with equal fidelity, and can switch between them by conditioning on the speaker identity. When trained to model music, we find that it generates novel and often highly realistic musical fragments. We also show that it can be employed as a discriminative model, returning promising results for phoneme recognition.

**Keywords**: speech synthesis, audio synthesis

---

### Photo-Realistic Single Image Super-Resolution Using a Generative Adversarial Network ([arXiv:1609.04802](https://arxiv.org/pdf/1609.04802)) ([cited by 3000+](https://scholar.google.com/scholar?cites=1219263946448760936))
By Christian Ledig, Lucas Theis, Ferenc Huszar, Jose Caballero, Andrew Cunningham, Alejandro Acosta, Andrew Aitken, Alykhan Tejani, Johannes Totz, Zehan Wang, Wenzhe Shi

**tl;dr** We propose SRGAN, a generative adversarial network for image super-resolution at 4x upscaling factors.

> Despite the breakthroughs in accuracy and speed of single image super-resolution using faster and deeper convolutional neural networks, one central problem remains largely unsolved: how do we recover the finer texture details when we super-resolve at large upscaling factors? The behavior of optimization-based super-resolution methods is principally driven by the choice of the objective function. Recent work has largely focused on minimizing the mean squared reconstruction error. The resulting estimates have high peak signal-to-noise ratios, but they are often lacking high-frequency details and are perceptually unsatisfying in the sense that they fail to match the fidelity expected at the higher resolution. In this paper, we present SRGAN, a generative adversarial network (GAN) for image super-resolution (SR). To our knowledge, it is the first framework capable of inferring photo-realistic natural images for 4x upscaling factors. To achieve this, we propose a perceptual loss function which consists of an adversarial loss and a content loss. The adversarial loss pushes our solution to the natural image manifold using a discriminator network that is trained to differentiate between the super-resolved images and original photo-realistic images. In addition, we use a content loss motivated by perceptual similarity instead of similarity in pixel space. Our deep residual network is able to recover photo-realistic textures from heavily downsampled images on public benchmarks. An extensive mean-opinion-score (MOS) test shows hugely significant gains in perceptual quality using SRGAN. The MOS scores obtained with SRGAN are closer to those of the original high-resolution images than to those obtained with any state-of-the-art method.

**Keywords**: GAN, image generation, super resolution

---

### Scaling Memory-Augmented Neural Networks with Sparse Reads and Writes ([arXiv:1610.09027](https://arxiv.org/pdf/1610.09027.pdf)) ([cited by 100+](https://scholar.google.com/scholar?cites=5913255713498338191))
By Jack W Rae, Jonathan J Hunt, Tim Harley, Ivo Danihelka, Andrew Senior, Greg Wayne, Alex Graves, Timothy P Lillicrap

**tl;dr** We present an end-to-end differentiable memory access scheme, which we call Sparse Access Memory (SAM), that retains the representational power of the original approaches whilst training efficiently with very large memories.

> Neural networks augmented with external memory have the ability to learn algorithmic solutions to complex tasks. These models appear promising for applications such as language modeling and machine translation. However, they scale poorly in both space and time as the amount of memory grows --- limiting their applicability to real-world domains. Here, we present an end-to-end differentiable memory access scheme, which we call Sparse Access Memory (SAM), that retains the representational power of the original approaches whilst training efficiently with very large memories. We show that SAM achieves asymptotic lower bounds in space and time complexity, and find that an implementation runs 1,000× faster and with 3,000× less physical memory than non-sparse models. SAM learns with comparable data efficiency to existing models on a range of synthetic tasks and one-shot Omniglot character recognition, and can scale to tasks requiring 100,000s of time steps and memories. As well, we show how our approach can be adapted for models that maintain temporal associations between memories, as with the recently introduced Differentiable Neural Computer.

**Keywords**: SAM, MANN, memory

---

### Reinforcement Learning with Unsupervised Auxiliary Tasks ([arXiv:1611.05397](https://arxiv.org/pdf/1611.05397)) ([cited by 500+](https://scholar.google.com/scholar?cites=14888805482854497974))
By Max Jaderberg, Volodymyr Mnih, Wojciech Marian Czarnecki, Tom Schaul, Joel Z Leibo, David Silver, Koray Kavukcuoglu

**tl;dr** We introduce a novel reinforcement learning agent that maximises many other pseudo-reward functions simultaneously by reinforcement learning.

> Deep reinforcement learning agents have achieved state-of-the-art results by directly maximising cumulative reward. However, environments contain a much wider variety of possible training signals. In this paper, we introduce an agent that also maximises many other pseudo-reward functions simultaneously by reinforcement learning. All of these tasks share a common representation that, like unsupervised learning, continues to develop in the absence of extrinsic rewards. We also introduce a novel mechanism for focusing this representation upon extrinsic rewards, so that learning can rapidly adapt to the most relevant aspects of the actual task. Our agent significantly outperforms the previous state-of-the-art on Atari, averaging 880% expert human performance, and a challenging suite of first-person, three-dimensional *Labyrinth* tasks leading to a mean speedup in learning of 10× and averaging 87% expert human performance on Labyrinth.

**Keywords**: RL, unsupervised learning

---

### Aggregated Residual Transformations for Deep Neural Networks ([arXiv:1611.05431](https://arxiv.org/pdf/1611.05431)) ([cited by 2000+](https://scholar.google.com/scholar?cites=11890020786646058356))
By Saining Xie, Ross Girshick, Piotr Dollár, Zhuowen Tu, Kaiming He

**tl;dr** We present a simple, highly modularized network architecture for image classification that has only a few hyper-parameters to set.

> We present a simple, highly modularized network architecture for image classification. Our network is constructed by repeating a building block that aggregates a set of transformations with the same topology. Our simple design results in a homogeneous, multi-branch architecture that has only a few hyper-parameters to set. This strategy exposes a new dimension, which we call "cardinality" (the size of the set of transformations), as an essential factor in addition to the dimensions of depth and width. On the ImageNet-1K dataset, we empirically show that even under the restricted condition of maintaining complexity, increasing cardinality is able to improve classification accuracy. Moreover, increasing cardinality is more effective than going deeper or wider when we increase the capacity. Our models, named ResNeXt, are the foundations of our entry to the ILSVRC 2016 classification task in which we secured 2nd place. We further investigate ResNeXt on an ImageNet-5K set and the COCO detection set, also showing better results than its ResNet counterpart. The code and models are publicly available online.

**Keywords**: image classification

---

### 🌟 Learning to reinforcement learn ([arXiv:1611.05763](https://arxiv.org/pdf/1611.05763.pdf)) ([cited by 350+](https://scholar.google.com/scholar?cites=2635837800726039837))
By Jane X Wang, Zeb Kurth-Nelson, Dhruva Tirumala, Hubert Soyer, Joel Z Leibo, Remi Munos, Charles Blundell, Dharshan Kumaran, Matt Botvinick

**tl;dr** Deep meta-reinforcement learning is a meta-learning approach to deep reinforcement learning that leverages structure in the training domain.

> In recent years deep reinforcement learning (RL) systems have attained superhuman performance in a number of challenging task domains. However, a major limitation of such applications is their demand for massive amounts of training data. A critical present objective is thus to develop deep RL methods that can adapt rapidly to new tasks. In the present work we introduce a novel approach to this challenge, which we refer to as deep meta-reinforcement learning. Previous work has shown that recurrent networks can support meta-learning in a fully supervised context. We extend this approach to the RL setting. What emerges is a system that is trained using one RL algorithm, but whose recurrent dynamics implement a second, quite separate RL procedure. This second, learned RL algorithm can differ from the original one in arbitrary ways. Importantly, because it is learned, it is configured to exploit structure in the training domain. We unpack these points in a series of seven proof-of-concept experiments, each of which examines a key aspect of deep meta-RL. We consider prospects for extending and scaling up the approach, and also point out some potentially important implications for neuroscience.

**Keywords**: reinforcement learning

---

### Image-to-Image Translation with Conditional Adversarial Networks ([arXiv:1611.07004](https://arxiv.org/pdf/1611.07004)) ([cited by 5000+](https://scholar.google.com/scholar?cites=16757839449706651543))
By Phillip Isola, Jun-Yan Zhu, Tinghui Zhou, Alexei A. Efros

**tl;dr** We investigate conditional adversarial networks as a general-purpose solution to image-to-image translation problems.

> We investigate conditional adversarial networks as a general-purpose solution to image-to-image translation problems. These networks not only learn the mapping from input image to output image, but also learn a loss function to train this mapping. This makes it possible to apply the same generic approach to problems that traditionally would require very different loss formulations. We demonstrate that this approach is effective at synthesizing photos from label maps, reconstructing objects from edge maps, and colorizing images, among other tasks. Indeed, since the release of the pix2pix software associated with this paper, a large number of internet users (many of them artists) have posted their own experiments with our system, further demonstrating its wide applicability and ease of adoption without the need for parameter tweaking. As a community, we no longer hand-engineer our mapping functions, and this work suggests we can achieve reasonable results without hand-engineering our loss functions either.

**Keywords**: img2img, image generation, GAN

---

### 🌟 *DNC*: Hybrid computing using a neural network with dynamic external memory ([paper](http://www.nature.com/articles/nature20101.epdf?author_access_token=ImTXBI8aWbYxYQ51Plys8NRgN0jAjWel9jnR3ZoTv0MggmpDmwljGswxVdeocYSurJ3hxupzWuRNeGvvXnoO8o4jTJcnAyhGuZzXJ1GEaD-Z7E6X_a9R-xqJ9TfJWBqz)) ([cited by 800+](https://scholar.google.com/scholar?cites=8100274942961380405))
By [Alex Graves](https://scholar.google.com/citations?user=DaFHynwAAAAJ), Greg Wayne, Malcolm Reynolds, Tim Harley, Ivo Danihelka, Agnieszka Grabska-Barwińska, Sergio Gómez Colmenarejo, Edward Grefenstette, Tiago Ramalho, John Agapiou, Adrià Puigdomènech Badia, Karl Moritz Hermann, Yori Zwols, Georg Ostrovski, Adam Cain, Helen King, Christopher Summerfield, Phil Blunsom, Koray Kavukcuoglu, Demis Hassabis

**tl;dr** A differentiable neural computer that can read from and write to an external memory matrix, analogous to the random-access memory in a conventional computer.

> Artificial neural networks are remarkably adept at sensory processing, sequence learning and reinforcement learning, but are limited in their ability to represent variables and data structures and to store data over long timescales, owing to the lack of an external memory. Here we introduce a machine learning model called a differentiable neural computer (DNC), which consists of a neural network that can read from and write to an external memory matrix, analogous to the random-access memory in a conventional computer. Like a conventional computer, it can use its memory to represent and manipulate complex data structures, but, like a neural network, it can learn to do so from data. When trained with supervised learning, we demonstrate that a DNC can successfully answer synthetic questions designed to emulate reasoning and inference problems in natural language. We show that it can learn tasks such as finding the shortest path between specified points and inferring the missing links in randomly generated graphs, and then generalize these tasks to specific graphs such as transport networks and family trees. When trained with reinforcement learning, a DNC can complete a moving blocks puzzle in which changing goals are specified by sequences of symbols. Taken together, our results demonstrate that DNCs have the capacity to solve complex, structured tasks that are inaccessible to neural networks without external read–write memory.

**Keywords**: DNC

---

### *StackGAN*: Text to Photo-realistic Image Synthesis with Stacked Generative Adversarial Networks ([arXiv:1612.03242](https://arxiv.org/pdf/1612.03242)) ([cited by 1000+](https://scholar.google.com/scholar?cites=17276767268002585863))
By Han Zhang, Tao Xu, Hongsheng Li, Shaoting Zhang, Xiaogang Wang, Xiaolei Huang, Dimitris Metaxas

**tl;dr** We propose Stacked Generative Adversarial Networks (StackGAN) to generate 256x256 photo-realistic images conditioned on text descriptions.

> Synthesizing high-quality images from text descriptions is a challenging problem in computer vision and has many practical applications. Samples generated by existing text-to-image approaches can roughly reflect the meaning of the given descriptions, but they fail to contain necessary details and vivid object parts. In this paper, we propose Stacked Generative Adversarial Networks (StackGAN) to generate 256x256 photo-realistic images conditioned on text descriptions. We decompose the hard problem into more manageable sub-problems through a sketch-refinement process. The Stage-I GAN sketches the primitive shape and colors of the object based on the given text description, yielding Stage-I low-resolution images. The Stage-II GAN takes Stage-I results and text descriptions as inputs, and generates high-resolution images with photo-realistic details. It is able to rectify defects in Stage-I results and add compelling details with the refinement process. To improve the diversity of the synthesized images and stabilize the training of the conditional-GAN, we introduce a novel Conditioning Augmentation technique that encourages smoothness in the latent conditioning manifold. Extensive experiments and comparisons with state-of-the-arts on benchmark datasets demonstrate that the proposed method achieves significant improvements on generating photo-realistic images conditioned on text descriptions.

**Keywords**: StackGAN, GAN, text2img

## 2017

### *AdaIN*: Arbitrary Style Transfer in Real-time with Adaptive Instance Normalization ([arXiv:1703.06868](https://arxiv.org/pdf/1703.06868.pdf)) ([cited by 500+](https://scholar.google.com/scholar?cites=6462913724934880335))
By Xun Huang, Serge Belongie

**tl;dr** We present a novel adaptive instance normalization layer that aligns the mean and variance of the content features with those of the style features.

> Gatys et al. recently introduced a neural algorithm that renders a content image in the style of another image, achieving so-called style transfer. However, their frame-work requires a slow iterative optimization process, which limits its practical application. Fast approximations with feed-forward neural networks have been proposed to speed up neural style transfer. Unfortunately, the speed improvement comes at a cost: the network is usually tied to a fixed set of styles and cannot adapt to arbitrary new styles. In this paper, we present a simple yet effective approach that for the first time enables arbitrary style transfer in real-time. At the heart of our method is a novel adaptive instance normalization (AdaIN) layer that aligns the mean and variance of the content features with those of the style features. Our method achieves speed comparable to the fastest existing approach, without the restriction to a pre-defined set of styles. In addition, our approach allows flexible user controls such as content-style trade-off, style interpolation, color & spatial controls, all using a single feed-forward neural network.

**Keywords**: AdaIN, neural style transfer

---

### Improved Texture Networks: Maximizing Quality and Diversity in Feed-forward Stylization and Texture Synthesis ([arXiv:1701.02096](https://arxiv.org/pdf/1701.02096)) ([cited by 200+](https://scholar.google.com/scholar?cites=15121556168732029899))
By Dmitry Ulyanov, Andrea Vedaldi, Victor Lempitsky

**tl;dr** Improve texture generation and image stylization by introducing a new learning formulation that encourages generators to sample unbiasedly from the Julesz texture ensemble.

> The recent work of Gatys et al., who characterized the style of an image by the statistics of convolutional neural network filters, ignited a renewed interest in the texture generation and image stylization problems. While their image generation technique uses a slow optimization process, recently several authors have proposed to learn generator neural networks that can produce similar outputs in one quick forward pass. While generator networks are promising, they are still inferior in visual quality and diversity compared to generation-by-optimization. In this work, we advance them in two significant ways. First, we introduce an instance normalization module to replace batch normalization with significant improvements to the quality of image stylization. Second, we improve diversity by introducing a new learning formulation that encourages generators to sample unbiasedly from the Julesz texture ensemble, which is the equivalence class of all images characterized by certain filter responses. Together, these two improvements take feed forward texture synthesis and image stylization much closer to the quality of generation-via-optimization, while retaining the speed advantage.

**Keywords**: CNN, instance normalization, neural style transfer

---

### Unsupervised Image-to-Image Translation Networks ([arXiv:1703.00848](https://arxiv.org/pdf/1703.00848)) ([cited by 900+](https://scholar.google.com/scholar?cites=14169741715291172305))
By Ming-Yu Liu, Thomas Breuel, Jan Kautz

**tl;dr** We propose an unsupervised image-to-image translation framework based on Coupled GANs.

> Unsupervised image-to-image translation aims at learning a joint distribution of images in different domains by using images from the marginal distributions in individual domains. Since there exists an infinite set of joint distributions that can arrive the given marginal distributions, one could infer nothing about the joint distribution from the marginal distributions without additional assumptions. To address the problem, we make a shared-latent space assumption and propose an unsupervised image-to-image translation framework based on Coupled GANs.

Keywords: GAN, VAE, VAE-GAN, img2img

---

### Intrinsic Motivation and Automatic Curricula via Asymmetric Self-Play ([arXiv:https://arxiv.org/pdf/1703.05407](https://arxiv.org/pdf/1703.05407)) ([cited by 100+](https://scholar.google.com/scholar?cites=4160913521858709316))
By Sainbayar Sukhbaatar, Zeming Lin, Ilya Kostrikov, Gabriel Synnaeve, Arthur Szlam, Rob Fergus

**tl;dr** We describe a simple scheme that allows an agent to learn about its environment in an unsupervised manner.

> We describe a simple scheme that allows an agent to learn about its environment in an unsupervised manner. Our scheme pits two versions of the same agent, Alice and Bob, against one another. Alice proposes a task for Bob to complete; and then Bob attempts to complete the task. In this work we will focus on two kinds of environments: (nearly) reversible environments and environments that can be reset. Alice will "propose" the task by doing a sequence of actions and then Bob must undo or repeat them, respectively. Via an appropriate reward structure, Alice and Bob automatically generate a curriculum of exploration, enabling unsupervised training of the agent. When Bob is deployed on an RL task within the environment, this unsupervised training reduces the number of supervised episodes needed to learn, and in some cases converges to a higher reward.

**Keywords**: artificial curiosity, RL, self-play

---

### Tacotron: Towards End-to-End Speech Synthesis ([arXiv:1703.10135](https://arxiv.org/pdf/1703.10135.pdf) ([cited 500+](https://scholar.google.ca/scholar?cites=11291739830353377265))
By Yuxuan Wang, RJ Skerry-Ryan, Daisy Stanton, Yonghui Wu, Ron J. Weiss, Navdeep Jaitly, Zongheng Yang, Ying Xiao, Zhifeng Chen, Samy Bengio, Quoc Le, Yannis Agiomyrgiannakis, Rob Clark, Rif A. Saurous

**tl;dr** Tacotron is a generative text-to-speech model that synthesizes speech directly from characters.

> A text-to-speech synthesis system typically consists of multiple stages, such as a text analysis frontend, an acoustic model and an audio synthesis module. Building these components often requires extensive domain expertise and may contain brittle design choices. In this paper, we present Tacotron, an end-to-end generative text-to-speech model that synthesizes speech directly from characters. Given <text, audio> pairs, the model can be trained completely from scratch with random initialization. We present several key techniques to make the sequence-to-sequence framework perform well for this challenging task. Tacotron achieves a 3.82 subjective 5-scale mean opinion score on US English, outperforming a production parametric system in terms of naturalness. In addition, since Tacotron generates speech at the frame level, it's substantially faster than sample-level autoregressive methods.

**Keywords**: speech synthesis

---

### *CycleGAN*: Unpaired Image-to-Image Translation using Cycle-Consistent Adversarial Networks ([arXiv:1703.10593](https://arxiv.org/pdf/1703.10593)) ([cited by 4000+](https://scholar.google.com/scholar?cites=18396328236259959400))

**tl;dr** Learning to translate an image from a source domain X to a target domain Y in the absence of paired examples using an adversarial loss.

> Image-to-image translation is a class of vision and graphics problems where the goal is to learn the mapping between an input image and an output image using a training set of aligned image pairs. However, for many tasks, paired training data will not be available. We present an approach for learning to translate an image from a source domain X to a target domain Y in the absence of paired examples. Our goal is to learn a mapping G:X→Y such that the distribution of images from G(X) is indistinguishable from the distribution Y using an adversarial loss. Because this mapping is highly under-constrained, we couple it with an inverse mapping F:Y→X and introduce a cycle consistency loss to push F(G(X))≈X (and vice versa).

**Keywords**: CycleGAN, GAN, img2img, style transfer

---

### MobileNets: Efficient Convolutional Neural Networks for Mobile Vision Applications ([arXiv:1704.04861](https://arxiv.org/pdf/1704.04861)) ([cited by 4000+](https://scholar.google.com/scholar?cites=5758617088745811384))
By Andrew G. Howard, Menglong Zhu, Bo Chen, Dmitry Kalenichenko, Weijun Wang, Tobias Weyand, Marco Andreetto, Hartwig Adam

**tl;dr** We present a class of efficient models called MobileNets for mobile and embedded vision applications.

> We present a class of efficient models called MobileNets for mobile and embedded vision applications. MobileNets are based on a streamlined architecture that uses depth-wise separable convolutions to build light weight deep neural networks. We introduce two simple global hyper-parameters that efficiently trade off between latency and accuracy. These hyper-parameters allow the model builder to choose the right sized model for their application based on the constraints of the problem. We present extensive experiments on resource and accuracy tradeoffs and show strong performance compared to other popular models on ImageNet classification. We then demonstrate the effectiveness of MobileNets across a wide range of applications and use cases including object detection, finegrain classification, face attributes and large scale geo-localization.

**Keywords**: efficiency

---

### Curiosity-driven Exploration by Self-supervised Prediction ([arXiv:1705.05363](https://arxiv.org/pdf/1705.05363)) ([cited by 500+](https://scholar.google.com/scholar?cites=9379743003299559904))
By Deepak Pathak, Pulkit Agrawal, Alexei A. Efros, Trevor Darrell

**tl;dr** We formulate curiosity as an error in a visual feature space learned by a self-supervised inverse dynamics model.

> In many real-world scenarios, rewards extrinsic to the agent are extremely sparse, or absent altogether. In such cases, curiosity can serve as an intrinsic reward signal to enable the agent to explore its environment and learn skills that might be useful later in its life. We formulate curiosity as the error in an agent's ability to predict the consequence of its own actions in a visual feature space learned by a self-supervised inverse dynamics model. Our formulation scales to high-dimensional continuous state spaces like images, bypasses the difficulties of directly predicting pixels, and, critically, ignores the aspects of the environment that cannot affect the agent. The proposed approach is evaluated in two environments: VizDoom and Super Mario Bros. Three broad settings are investigated: 1) sparse extrinsic reward, where curiosity allows for far fewer interactions with the environment to reach the goal; 2) exploration with no extrinsic reward, where curiosity pushes the agent to explore more efficiently; and 3) generalization to unseen scenarios (e.g. new levels of the same game) where the knowledge gained from earlier experience helps the agent explore new places much faster than starting from scratch.

**Source code**: [pathak22.github.io/noreward-rl/](https://pathak22.github.io/noreward-rl/)

**Keywords**: RL, artificial curiosity

---

### Universal Style Transfer via Feature Transforms ([arXiv:1705.08086](https://arxiv.org/pdf/1705.08086)) ([cited by 200+](https://scholar.google.com/scholar?cites=7001062204457348357))
By Yijun Li, Chen Fang, Jimei Yang, Zhaowen Wang, Xin Lu, Ming-Hsuan Yang

**tl;dr** We present a simple yet effective method that tackles these limitations without training on any pre-defined styles.

> Universal style transfer aims to transfer arbitrary visual styles to content images. Existing feed-forward based methods, while enjoying the inference efficiency, are mainly limited by inability of generalizing to unseen styles or compromised visual quality. In this paper, we present a simple yet effective method that tackles these limitations without training on any pre-defined styles. The key ingredient of our method is a pair of feature transforms, whitening and coloring, that are embedded to an image reconstruction network. The whitening and coloring transforms reflect a direct matching of feature covariance of the content image to a given style image, which shares similar spirits with the optimization of Gram matrix based cost in neural style transfer. We demonstrate the effectiveness of our algorithm by generating high-quality stylized images with comparisons to a number of recent methods. We also analyze our method by visualizing the whitened features and synthesizing textures via simple feature coloring.

**Keywords**: neural style transfer

---

### *SELU*: Self-Normalizing Neural Networks ([arXiv:1706.02515](https://arxiv.org/pdf/1706.02515)) ([cited by 800+](https://scholar.google.com/scholar?cites=3659160383490046744))
By Günter Klambauer, Thomas Unterthiner, Andreas Mayr, [Sepp Hochreiter](https://scholar.google.com/citations?user=tvUH3WMAAAAJ)

**tl;dr** We introduce self-normalizing neural networks (SNNs) to enable high-level abstract representations.

> Deep Learning has revolutionized vision via convolutional neural networks (CNNs) and natural language processing via recurrent neural networks (RNNs). However, success stories of Deep Learning with standard feed-forward neural networks (FNNs) are rare. FNNs that perform well are typically shallow and, therefore cannot exploit many levels of abstract representations. We introduce self-normalizing neural networks (SNNs) to enable high-level abstract representations. While batch normalization requires explicit normalization, neuron activations of SNNs automatically converge towards zero mean and unit variance. The activation function of SNNs are "scaled exponential linear units" (SELUs), which induce self-normalizing properties. Using the Banach fixed-point theorem, we prove that activations close to zero mean and unit variance that are propagated through many network layers will converge towards zero mean and unit variance -- even under the presence of noise and perturbations. This convergence property of SNNs allows to (1) train deep networks with many layers, (2) employ strong regularization, and (3) to make learning highly robust. Furthermore, for activations not close to unit variance, we prove an upper and lower bound on the variance, thus, vanishing and exploding gradients are impossible. We compared SNNs on (a) 121 tasks from the UCI machine learning repository, on (b) drug discovery benchmarks, and on (c) astronomy tasks with standard FNNs and other machine learning methods such as random forests and support vector machines. SNNs significantly outperformed all competing FNN methods at 121 UCI tasks, outperformed all competing methods at the Tox21 dataset, and set a new record at an astronomy data set. The winning SNN architectures are often very deep.

**Source code**: [github.com/bioinf-jku/SNNs](https://github.com/bioinf-jku/SNNs)

**Keywords**: normalization

---

### Attention Is All You Need ([arXiv:1706.03762](https://arxiv.org/pdf/1706.03762)) ([cited by 7000+](https://scholar.google.com/scholar?cites=2960712678066186980))
By Ashish Vaswani, Noam Shazeer, Niki Parmar, Jakob Uszkoreit, Llion Jones, Aidan N. Gomez, Lukasz Kaiser, Illia Polosukhin<br>

**tl;dr** We propose a new simple network architecture, the Transformer, based solely on attention mechanisms, dispensing with recurrence and convolutions entirely.

> We propose a new simple network architecture, the Transformer, based solely on attention mechanisms, dispensing with recurrence and convolutions entirely. Experiments on two machine translation tasks show these models to be superior in quality while being more parallelizable and requiring significantly less time to train.

**Keywords**: NLP, transformer

---

### *ShuffleNet*: An Extremely Efficient Convolutional Neural Network for Mobile Devices ([arXiv:1707.01083](https://arxiv.org/pdf/1707.01083)) ([cited by 1000+](https://scholar.google.com/scholar?cites=6469340744827368429))
By Xiangyu Zhang, Xinyu Zhou, Mengxiao Lin, Jian Sun

**tl;dr** We introduce an extremely computation-efficient CNN architecture named ShuffleNet, which is designed specially for mobile devices with very limited computing power.

> We introduce an extremely computation-efficient CNN architecture named ShuffleNet, which is designed specially for mobile devices with very limited computing power (e.g., 10-150 MFLOPs). The new architecture utilizes two new operations, pointwise group convolution and channel shuffle, to greatly reduce computation cost while maintaining accuracy. Experiments on ImageNet classification and MS COCO object detection demonstrate the superior performance of ShuffleNet over other structures, e.g. lower top-1 error (absolute 7.8%) than recent MobileNet on ImageNet classification task, under the computation budget of 40 MFLOPs. On an ARM-based mobile device, ShuffleNet achieves ~13x actual speedup over AlexNet while maintaining comparable accuracy.

**Keywords**: CNN

---

### *HER*: Hindsight Experience Replay ([arXiv:1707.01495](https://arxiv.org/pdf/1707.01495)) ([cited by 500+](https://scholar.google.com/scholar?cites=14733084267697271284))

**tl;dr** We present a novel technique called Hindsight Experience Replay which allows sample-efficient learning from rewards which are sparse and binary and therefore avoid the need for complicated reward engineering.

> Dealing with sparse rewards is one of the biggest challenges in Reinforcement Learning (RL). We present a novel technique called Hindsight Experience Replay which allows sample-efficient learning from rewards which are sparse and binary and therefore avoid the need for complicated reward engineering. It can be combined with an arbitrary off-policy RL algorithm and may be seen as a form of implicit curriculum.

**Demo**: [Video](https://www.youtube.com/watch?v=Dz_HuzgMxzo)

**Summaries**

+ [Video by Two Minute Papers](https://www.youtube.com/watch?v=Dvd1jQe3pq0)
+ [Video by ArXiv Insights](https://www.youtube.com/watch?v=0Ey02HT_1Ho)

**Explanation**: [Video by Olivier Siguad](https://www.youtube.com/watch?v=77xkqEAsHFI)

**Keywords**: RL, experience replay

---

### *PPO*: Proximal Policy Optimization Algorithms ([arXiv:1707.06347](https://arxiv.org/pdf/1707.06347)) ([cited by 2000+](https://scholar.google.com/scholar?cites=2664197784944153194))
By John Schulman, Filip Wolski, Prafulla Dhariwal, Alec Radford, Oleg Klimov

**tl;dr** We propose a new family of policy gradient methods for reinforcement learning that alternate between sampling data through interaction with the environment, and optimizing a "surrogate" objective function using stochastic gradient ascent. 

> We propose a new family of policy gradient methods for reinforcement learning, which alternate between sampling data through interaction with the environment, and optimizing a "surrogate" objective function using stochastic gradient ascent. Whereas standard policy gradient methods perform one gradient update per data sample, we propose a novel objective function that enables multiple epochs of minibatch updates. The new methods, which we call proximal policy optimization (PPO), have some of the benefits of trust region policy optimization (TRPO), but they are much simpler to implement, more general, and have better sample complexity (empirically). Our experiments test PPO on a collection of benchmark tasks, including simulated robotic locomotion and Atari game playing, and we show that PPO outperforms other online policy gradient methods, and overall strikes a favorable balance between sample complexity, simplicity, and wall-time.

**Keywords**: PPO, RL

---

### *SE*: Squeeze-and-Excitation Networks ([arXiv:1709.01507](https://arxiv.org/pdf/1709.01507)) ([cited by 2000+](https://scholar.google.com/scholar?cites=11424287065250598243))
By Jie Hu, Li Shen, Samuel Albanie, Gang Sun, Enhua Wu

**tl;dr** We propose a novel architectural unit that adaptively recalibrates channel-wise feature responses by explicitly modelling interdependencies between channels.

> The central building block of convolutional neural networks (CNNs) is the convolution operator, which enables networks to construct informative features by fusing both spatial and channel-wise information within local receptive fields at each layer. A broad range of prior research has investigated the spatial component of this relationship, seeking to strengthen the representational power of a CNN by enhancing the quality of spatial encodings throughout its feature hierarchy. In this work, we focus instead on the channel relationship and propose a novel architectural unit, which we term the "Squeeze-and-Excitation" (SE) block, that adaptively recalibrates channel-wise feature responses by explicitly modelling interdependencies between channels. We show that these blocks can be stacked together to form SENet architectures that generalise extremely effectively across different datasets. We further demonstrate that SE blocks bring significant improvements in performance for existing state-of-the-art CNNs at slight additional computational cost. Squeeze-and-Excitation Networks formed the foundation of our ILSVRC 2017 classification submission which won first place and reduced the top-5 error to 2.251%, surpassing the winning entry of 2016 by a relative improvement of ~25%.

**Source code**: [github.com/hujie-frank/SENet](https://github.com/hujie-frank/SENet)

**Keywords**: CNN

---

### Progressive growing of gans for improved quality, stability, and variation ([arXiv:1710.10196](https://arxiv.org/pdf/1710.10196)) ([cited by 1000+](https://scholar.google.com/scholar?cites=11486098150916361186))
By Tero Karras, Timo Aila, Samuli Laine, Jaakko Lehtinen

**tl;dr** We describe a new training methodology for generative adversarial networks that add new layers that model increasingly fine details as training progresses.

> We describe a new training methodology for generative adversarial networks. The key idea is to grow both the generator and discriminator progressively: starting from a low resolution, we add new layers that model increasingly fine details as training progresses. This both speeds the training up and greatly stabilizes it, allowing us to produce images of unprecedented quality.

**Keywords**: GAN, image generation

---

### *AlphaGo Zero*: Mastering the game of Go without human knowledge ([paper](https://www.nature.com/articles/nature24270)) ([cited by 3000+](https://scholar.google.com/scholar?cites=7419430260981373186))
By [David Silver](https://scholar.google.com/citations?user=-8DNE4UAAAAJ), Julian Schrittwieser, Karen Simonyan, Ioannis Antonoglou, Aja Huang, Arthur Guez, Thomas Hubert, Lucas Baker, Matthew Lai, Adrian Bolton, Yutian Chen, [Timothy Lillicrap](https://scholar.google.ca/citations?user=htPVdRMAAAAJ), Fan Hui, Laurent Sifre, George van den Driessche, Thore Graepel, Demis Hassabis

**tl;dr** A long-standing goal of artificial intelligence is an algorithm that learns, tabula rasa, superhuman proficiency in challenging domains.

> A long-standing goal of artificial intelligence is an algorithm that learns, tabula rasa, superhuman proficiency in challenging domains. Recently, AlphaGo became the first program to defeat a world champion in the game of Go. The tree search in AlphaGo evaluated positions and selected moves using deep neural networks. These neural networks were trained by supervised learning from human expert moves, and by reinforcement learning from self-play. Here we introduce an algorithm based solely on reinforcement learning, without human data, guidance or domain knowledge beyond game rules. AlphaGo becomes its own teacher: a neural network is trained to predict AlphaGo’s own move selections and also the winner of AlphaGo’s games. This neural network improves the strength of the tree search, resulting in higher quality move selection and stronger self-play in the next iteration. Starting tabula rasa, our new program AlphaGo Zero achieved superhuman performance, winning 100–0 against the previously published, champion-defeating AlphaGo.

**Publication**: [deepmind.com](https://deepmind.com/blog/alphago-zero-learning-scratch/)

**How it works (summary)**: [YouTube](https://www.youtube.com/watch?v=MgowR4pq3e8)

**How it works**: [YouTube](https://www.youtube.com/watch?v=XuzIqE2IshY)

**Keywords**: RL, self-play, MCTS

---

### *CapsuleNet*: Dynamic Routing Between Capsules ([arXiv:1710.09829](https://arxiv.org/pdf/1710.09829)) ([cited by 1000+](https://scholar.google.com/scholar?cites=5914955692202761908))
By Sara Sabour, Nicholas Frosst, [Geoffrey E Hinton](https://scholar.google.ca/citations?hl=en&user=JicYPdAAAAAJ)

**tl;dr** We show that a multi-layer capsule system achieves state-of-the-art performance on MNIST.

> A capsule is a group of neurons whose activity vector represents the instantiation parameters of a specific type of entity such as an object or an object part. We use the length of the activity vector to represent the probability that the entity exists and its orientation to represent the instantiation parameters. Active capsules at one level make predictions, via transformation matrices, for the instantiation parameters of higher-level capsules. When multiple predictions agree, a higher level capsule becomes active. We show that a discrimininatively trained, multi-layer capsule system achieves state-of-the-art performance on MNIST and is considerably better than a convolutional net at recognizing highly overlapping digits. To achieve these results we use an iterative routing-by-agreement mechanism: A lower-level capsule prefers to send its output to higher level capsules whose activity vectors have a big scalar product with the prediction coming from the lower-level capsule. 

**Explanation**: [YouTube](https://www.youtube.com/watch?v=pPN8d0E3900)

**Keywords**: CapsuleNet, network architecture

---

### *StackGAN++*: Realistic Image Synthesis with Stacked Generative Adversarial Networks ([arXiv:1710.10916](https://arxiv.org/pdf/1710.10916)) ([cited by 200+](https://scholar.google.com/scholar?cites=5251688290326540457))
By Han Zhang, Tao Xu, Hongsheng Li, Shaoting Zhang, Xiaogang Wang, Xiaolei Huang, Dimitris Metaxas

**tl;dr** StackGAN: Stacked Generative Adversarial Networks for Photo-Realistic Image Generation

> Although Generative Adversarial Networks (GANs) have shown remarkable success in various tasks, they still face challenges in generating high quality images. In this paper, we propose Stacked Generative Adversarial Networks (StackGAN) aiming at generating high-resolution photo-realistic images. First, we propose a two-stage generative adversarial network architecture, StackGAN-v1, for text-to-image synthesis. The Stage-I GAN sketches the primitive shape and colors of the object based on given text description, yielding low-resolution images. The Stage-II GAN takes Stage-I results and text descriptions as inputs, and generates high-resolution images with photo-realistic details. Second, an advanced multi-stage generative adversarial network architecture, StackGAN-v2, is proposed for both conditional and unconditional generative tasks. Our StackGAN-v2 consists of multiple generators and discriminators in a tree-like structure; images at multiple scales corresponding to the same scene are generated from different branches of the tree. StackGAN-v2 shows more stable training behavior than StackGAN-v1 by jointly approximating multiple distributions. Extensive experiments demonstrate that the proposed stacked generative adversarial networks significantly outperform other state-of-the-art methods in generating photo-realistic images.

**Keywords**: StackGAN, GAN, text2img

---

### *MoS*: Breaking the Softmax Bottleneck: A High-Rank RNN Language Model ([arXiv:1711.03953](https://arxiv.org/pdf/1711.03953)) ([cited by 100+](https://scholar.google.com/scholar?cites=15538946355362697879))
By Zhilin Yang, Zihang Dai, Ruslan Salakhutdinov, William W. Cohen

**tl;dr** We formulate language modeling as a matrix factorization problem, and show that the expressiveness of Softmax-based models (including the majority of neural language models) is limited by a Softmax bottleneck.

> We formulate language modeling as a matrix factorization problem, and show that the expressiveness of Softmax-based models (including the majority of neural language models) is limited by a Softmax bottleneck. Given that natural language is highly context-dependent, this further implies that in practice Softmax with distributed word embeddings does not have enough capacity to model natural language. We propose a simple and effective method to address this issue, and improve the state-of-the-art perplexities on Penn Treebank and WikiText-2 to 47.69 and 40.68 respectively. The proposed method also excels on the large-scale 1B Word dataset, outperforming the baseline by over 5.6 points in perplexity.

**Keywords**: NLP, RNN, activation function

---

### Natural TTS Synthesis by Conditioning WaveNet on Mel Spectrogram Predictions
By Jonathan Shen, Ruoming Pang, Ron J. Weiss, Mike Schuster, Navdeep Jaitly, Zongheng Yang, Zhifeng Chen, Yu Zhang, Yuxuan Wang, RJ Skerry-Ryan, Rif A. Saurous, Yannis Agiomyrgiannakis, Yonghui Wu

**tl;dr** Tacotron 2: A neural network architecture for speech synthesis directly from text.

> This paper describes Tacotron 2, a neural network architecture for speech synthesis directly from text. The system is composed of a recurrent sequence-to-sequence feature prediction network that maps character embeddings to mel-scale spectrograms, followed by a modified WaveNet model acting as a vocoder to synthesize timedomain waveforms from those spectrograms. Our model achieves a mean opinion score (MOS) of 4.53 comparable to a MOS of 4.58 for professionally recorded speech. To validate our design choices, we present ablation studies of key components of our system and evaluate the impact of using mel spectrograms as the input to WaveNet instead of linguistic, duration, and F0 features. We further demonstrate that using a compact acoustic intermediate representation enables significant simplification of the WaveNet architecture.

---

### Open-endedness: The last grand challenge you’ve never heard of ([article](https://www.oreilly.com/radar/open-endedness-the-last-grand-challenge-youve-never-heard-of/))

**tl;dr** Artificial intelligence is a grand challenge for computer science, but it’s something that will take astronomical effort over expansive time to achieve.

> Artificial intelligence (AI) is a grand challenge for computer science. Lifetimes of effort and billions of dollars have powered its pursuit. Yet, today its most ambitious vision remains unmet: though progress continues, no human-competitive general digital intelligence is within our reach. However, such an elusive goal is exactly what we expect from a “grand challenge”—it’s something that will take astronomical effort over expansive time to achieve—and is likely worth the wait. There are other grand challenges, like curing cancer, achieving 100% renewable energy, or unifying physics. Some fields have entire sets of grand challenges, such as David Hilbert’s 23 unsolved problems in mathematics, which laid down the gauntlet for the entire 20th century. What’s unusual, though, is for there to be a problem whose solution could radically alter our civilization and our understanding of ourselves while being known only to the smallest sliver of researchers. Despite how strangely implausible that sounds, it is precisely the scenario today with the challenge of open-endedness.

---

### A generative vision model that trains with high data efficiency and breaks text-based CAPTCHAs ([pdf](http://www.academia.edu/download/55648400/science.aag2612.full.pdf))
By D. George, W. Lehrach, K. Kansky, M. Lázaro-Gredilla, C. Laan, B. Marthi, X. Lou, Z. Meng, Y. Liu, H. Wang,
A. Lavin, D. S. Phoenix

**tl;dr** Learning from few examples and generalizing to dramatically different situations are capabilities of human visual intelligence that are yet to be matched by leading machine learning models.

> Learning from few examples and generalizing to dramatically different situations are capabilities of human visual intelligence that are yet to be matched by leading machine learning models. By drawing inspiration from systems neuroscience, we introduce a probabilistic generative model for vision in which message-passing based inference handles recognition, segmentation and reasoning in a unified way. The model demonstrates excellent generalization and occlusion-reasoning capabilities, and outperforms deep neural networks on a challenging scene text recognition benchmark while being 300-fold more data efficient. In addition, the model fundamentally breaks the defense of modern text-based CAPTCHAs by generatively segmenting characters without CAPTCHA-specific heuristics. Our model emphasizes aspects like data efficiency and compositionality that may be important in the path toward general artificial intelligence.

**Keywords**: computer vision

## 2018

### Innateness, AlphaZero, and Artificial Intelligence ([arXiv:1801.05667](https://arxiv.org/pdf/1801.05667)) ([cited by 30+](https://scholar.google.com/scholar?cites=13839738697223328361))
By Gary Marcus

**tl;dr** Artificial intelligence needs greater attention to innateness.

> The concept of innateness is rarely discussed in the context of artificial intelligence. When it is discussed, or hinted at, it is often the context of trying to reduce the amount of innate machinery in a given system. In this paper, I consider as a test case a recent series of papers by Silver et al (Silver et al., 2017a) on AlphaGo and its successors that have been presented as an argument that a "even in the most challenging of domains: it is possible to train to superhuman level, without human examples or guidance", "starting tabula rasa." I argue that these claims are overstated, for multiple reasons. I close by arguing that artificial intelligence needs greater attention to innateness, and I point to some proposals about what that innateness might look like.

**Keywords**: biological learning

---

### Soft Actor-Critic: Off-Policy Maximum Entropy Deep Reinforcement Learning with a Stochastic Actor ([arXiv:1801.01290](https://arxiv.org/pdf/1801.01290)) ([cited by 500+](https://scholar.google.com/scholar?cites=13282174879342015249))
By Tuomas Haarnoja, Aurick Zhou, Pieter Abbeel, Sergey Levine

**tl;dr** We propose soft actor-critic, an off-policy actor-Critic deep RL algorithm based on the maximum entropy reinforcement learning framework.

> Model-free deep reinforcement learning (RL) algorithms have been demonstrated on a range of challenging decision making and control tasks. However, these methods typically suffer from two major challenges: very high sample complexity and brittle convergence properties, which necessitate meticulous hyperparameter tuning. Both of these challenges severely limit the applicability of such methods to complex, real-world domains. In this paper, we propose soft actor-critic, an off-policy actor-critic deep RL algorithm based on the maximum entropy reinforcement learning framework. In this framework, the actor aims to maximize expected reward while also maximizing entropy. That is, to succeed at the task while acting as randomly as possible. Prior deep RL methods based on this framework have been formulated as Q-learning methods. By combining off-policy updates with a stable stochastic actor-critic formulation, our method achieves state-of-the-art performance on a range of continuous control benchmark tasks, outperforming prior on-policy and off-policy methods. Furthermore, we demonstrate that, in contrast to other off-policy algorithms, our approach is very stable, achieving very similar performance across different random seeds.

**Keywords**: A3C, RL

---

### *MVAE*: Multimodal Generative Models for Scalable Weakly-Supervised Learning ([arXiv:1802.05335](https://arxiv.org/pdf/1802.05335)) ([cited by 33+](https://scholar.google.com/scholar?cites=3003516605424055081))
By Mike Wu, Noah Goodman

**tl;dr** A multimodal variational autoencoder that uses a product-of-experts inference network and a sub-sampled training paradigm.

> Multiple modalities often co-occur when describing natural phenomena. Learning a joint representation of these modalities should yield deeper and more useful representations. Previous generative approaches to multi-modal input either do not learn a joint distribution or require additional computation to handle missing data. Here, we introduce a multimodal variational autoencoder (MVAE) that uses a product-of-experts inference network and a sub-sampled training paradigm to solve the multi-modal inference problem. Notably, our model shares parameters to efficiently learn under any combination of missing modalities. We apply the MVAE on four datasets and match state-of-the-art performance using many fewer parameters. In addition, we show that the MVAE is directly applicable to weakly-supervised learning, and is robust to incomplete supervision. We then consider two case studies, one of learning image transformations---
edge detection, colorization, segmentation---
as a set of modalities, followed by one of machine translation between two languages. We find appealing results across this range of tasks.

**Keywords**: VAE

---

### Addressing Function Approximation Error in Actor-Critic Methods ([arXiv:1802.09477](https://arxiv.org/pdf/1802.09477)) ([cited by 300+](https://scholar.google.com/scholar?cites=2930747733592680111))
By Scott Fujimoto, Herke van Hoof, David Meger

**tl;dr** We show that function approximation errors persist in an actor-critic setting and propose novel mechanisms to minimize its effects on both the actor and the critic.

> In value-based reinforcement learning methods such as deep Q-learning, function approximation errors are known to lead to overestimated value estimates and suboptimal policies. We show that this problem persists in an actor-critic setting and propose novel mechanisms to minimize its effects on both the actor and the critic. Our algorithm builds on Double Q-learning, by taking the minimum value between a pair of critics to limit overestimation. We draw the connection between target networks and overestimation bias, and suggest delaying policy updates to reduce per-update error and further improve performance. We evaluate our method on the suite of OpenAI gym tasks, outperforming the state of the art in every environment tested.

**Keywords**: A3C, reinforcement learning

---

### Diversity is All You Need: Learning Skills without a Reward Function ([arXiv:1802.06070](https://arxiv.org/pdf/1802.06070)) ([cited by 100+](https://scholar.google.com/scholar?cites=12324439663284457782))
By Benjamin Eysenbach, Abhishek Gupta, Julian Ibarz, Sergey Levine

**tl;dr** We propose DIAYN ('Diversity is All You Need'), a method for learning useful skills without a reward function.

> Intelligent creatures can explore their environments and learn useful skills without supervision. In this paper, we propose DIAYN ('Diversity is All You Need'), a method for learning useful skills without a reward function. Our proposed method learns skills by maximizing an information theoretic objective using a maximum entropy policy. On a variety of simulated robotic tasks, we show that this simple objective results in the unsupervised emergence of diverse skills, such as walking and jumping. In a number of reinforcement learning benchmark environments, our method is able to learn a skill that solves the benchmark task despite never receiving the true task reward. We show how pretrained skills can provide a good parameter initialization for downstream tasks, and can be composed hierarchically to solve complex, sparse reward tasks. Our results suggest that unsupervised discovery of skills can serve as an effective pretraining mechanism for overcoming challenges of exploration and data efficiency in reinforcement learning.

**Keywords**: RL, artificial curiosity

---

### Investigating Human Priors for Playing Video Games ([arXiv:1802.10217](https://arxiv.org/pdf/1802.10217)) ([cited by 53+](https://scholar.google.com/scholar?cites=2202192690517876762))
By Rachit Dubey, Pulkit Agrawal, Deepak Pathak, Thomas L. Griffiths, Alexei A. Efros

**tl;dr** What makes humans so good at solving video games?

> What makes humans so good at solving seemingly complex video games? Unlike computers, humans bring in a great deal of prior knowledge about the world, enabling efficient decision making. This paper investigates the role of human priors for solving video games. Given a sample game, we conduct a series of ablation studies to quantify the importance of various priors on human performance. We do this by modifying the video game environment to systematically mask different types of visual information that could be used by humans as priors. We find that removal of some prior knowledge causes a drastic degradation in the speed with which human players solve the game, e.g. from 2 minutes to over 20 minutes. Furthermore, our results indicate that general priors, such as the importance of objects and visual consistency, are critical for efficient game-play.

**Website**: [rach0012.github.io/humanRL_website/](https://rach0012.github.io/humanRL_website/)

**Keywords**: RL, priors, biological learning

---

### 🌟 *MERLIN*: Unsupervised Predictive Memory in a Goal-Directed Agent ([arXiv:1803.10760](https://arxiv.org/pdf/1803.10760)) ([cited by 70+](https://scholar.google.com/scholar?cites=1449593791259825848))
By Greg Wayne, Chia-Chun Hung, David Amos, Mehdi Mirza, Arun Ahuja, Agnieszka Grabska-Barwinska, Jack Rae, Piotr Mirowski, Joel Z. Leibo, Adam Santoro, Mevlana Gemici, Malcolm Reynolds, Tim Harley, Josh Abramson, Shakir Mohamed, Danilo Rezende, David Saxton, Adam Cain, Chloe Hillier, [David Silver](https://scholar.google.com/citations?user=-8DNE4UAAAAJ), Koray Kavukcuoglu, Matt Botvinick, Demis Hassabis, [Timothy Lillicrap](https://scholar.google.ca/citations?user=htPVdRMAAAAJ)

**tl;dr** We show that memory is not enough; it is critical that the right information be stored in the right format. We develop a model, the Memory, RL, and Inference Network (MERLIN), in which memory formation is guided by a process of predictive modeling, to solve canonical behavioural tasks in psychology and neurobiology.

> Animals execute goal-directed behaviours despite the limited range and scope of their sensors. To cope, they explore environments and store memories maintaining estimates of important information that is not presently available. Recently, progress has been made with artificial intelligence (AI) agents that learn to perform tasks from sensory input, even at a human level, by merging reinforcement learning (RL) algorithms with deep neural networks, and the excitement surrounding these results has led to the pursuit of related ideas as explanations of non-human animal learning. However, we demonstrate that contemporary RL algorithms struggle to solve simple tasks when enough information is concealed from the sensors of the agent, a property called "partial observability". An obvious requirement for handling partially observed tasks is access to extensive memory, but we show memory is not enough; it is critical that the right information be stored in the right format. We develop a model, the Memory, RL, and Inference Network (MERLIN), in which memory formation is guided by a process of predictive modeling. MERLIN facilitates the solution of tasks in 3D virtual reality environments for which partial observability is severe and memories must be maintained over long durations. Our model demonstrates a single learning agent architecture that can solve canonical behavioural tasks in psychology and neurobiology without strong simplifying assumptions about the dimensionality of sensory input or the duration of experiences.

**Keywords**: RL, MANN

**Video Links**

* [https://youtu.be/YFx-D4eEs5A](https://youtu.be/YFx-D4eEs5A)
* [https://youtu.be/IiR_NOomcpk](https://youtu.be/IiR_NOomcpk)
* [https://youtu.be/dQMKJtLScmk](https://youtu.be/dQMKJtLScmk)
* [https://youtu.be/xrYDlTXyC6Q](https://youtu.be/xrYDlTXyC6Q)
* [https://youtu.be/04H28-qA3f8](https://youtu.be/04H28-qA3f8)
* [https://youtu.be/3iA19h0Vvq0](https://youtu.be/3iA19h0Vvq0)

---

### The Kanerva Machine: A Generative Distributed Memory ([arXiv:1804.01756](https://arxiv.org/pdf/1804.01756)) ([cited by 10+](https://scholar.google.com/scholar?cites=9888262262485457347))
By Yan Wu, Greg Wayne, [Alex Graves](https://scholar.google.com/citations?user=DaFHynwAAAAJ), [Timothy Lillicrap](https://scholar.google.ca/citations?user=htPVdRMAAAAJ)

**tl;dr** We present an end-to-end trained memory system that quickly adapts to new data and generates samples like them.

> We present an end-to-end trained memory system that quickly adapts to new data and generates samples like them. Inspired by Kanerva's sparse distributed memory, it has a robust distributed reading and writing mechanism. The memory is analytically tractable, which enables optimal on-line compression via a Bayesian update-rule. We formulate it as a hierarchical conditional generative model, where memory provides a rich data-dependent prior distribution. Consequently, the top-down memory and bottom-up perception are combined to produce the code representing an observation. Empirically, we demonstrate that the adaptive memory significantly improves generative models trained on both the Omniglot and CIFAR datasets. Compared with the Differentiable Neural Computer (DNC) and its variants, our memory model has greater capacity and is significantly easier to train.

**Jupyter Notebook (PyTorch)**: [github.com/Kajiyu/kanerva_machine](https://nbviewer.jupyter.org/github/Kajiyu/kanerva_machine/blob/master/notebook/kanerva_machine_pytorch.ipynb)

**Keywords**: Bayesian, MANN, memory model, one-shot learning

---

### 🌟 Differentiable plasticity: training plastic neural networks with backpropagation ([arXiv:1804.02464](https://arxiv.org/pdf/1804.02464)) ([cited by 40+](https://scholar.google.com/scholar?cites=16849084099727983459))
By [Thomas Miconi](https://scholar.google.ca/citations?hl=en&user=EXun8woAAAAJ), [Jeff Clune](https://scholar.google.ca/citations?hl=en&user=5TZ7f5wAAAAJ), [Kenneth O. Stanley](https://scholar.google.ca/citations?user=6Q6oO1MAAAAJ)

**tl;dr** We show that plasticity, just like connection weights, can be optimized by gradient descent in large recurrent networks with Hebbian plastic connections.

> How can we build agents that keep learning from experience, quickly and efficiently, after their initial training? Here we take inspiration from the main mechanism of learning in biological brains: synaptic plasticity, carefully tuned by evolution to produce efficient lifelong learning. We show that plasticity, just like connection weights, can be optimized by gradient descent in large (millions of parameters) recurrent networks with Hebbian plastic connections. First, recurrent plastic networks with more than two million parameters can be trained to memorize and reconstruct sets of novel, high-dimensional 1000+ pixels natural images not seen during training. Crucially, traditional non-plastic recurrent networks fail to solve this task. Furthermore, trained plastic networks can also solve generic meta-learning tasks such as the Omniglot task, with competitive results and little parameter overhead. Finally, in reinforcement learning settings, plastic networks outperform a non-plastic equivalent in a maze exploration task. We conclude that differentiable plasticity may provide a powerful novel approach to the learning-to-learn problem.

**Keywords**: meta-learning, Hebbian plasticity

---

### Deep Reinforcement Learning for Playing 2.5D Fighting Games ([arXiv:1805.02070](https://arxiv.org/pdf/1805.02070)) ([cited by 40+](https://scholar.google.com/scholar?cites=3298968977177896332))
By Yu-Jhe Li, Hsin-Yu Chang, Yu-Jing Lin, Po-Wei Wu, Yu-Chiang Frank Wang

**tl;dr** We present a novel A3C+ network for learning RL agents for 2.5D fighting games.

> Deep reinforcement learning has shown its success in game playing. However, 2.5D fighting games would be a challenging task to handle due to ambiguity in visual appearances like height or depth of the characters. Moreover, actions in such games typically involve particular sequential action orders, which also makes the network design very difficult. Based on the network of Asynchronous Advantage Actor-Critic (A3C), we create an OpenAI-gym-like gaming environment with the game of Little Fighter 2 (LF2), and present a novel A3C+ network for learning RL agents. The introduced model includes a Recurrent Info network, which utilizes game-related info features with recurrent layers to observe combo skills for fighting. In the experiments, we consider LF2 in different settings, which successfully demonstrates the use of our proposed model for learning 2.5D fighting games.

**Keywords**: RL, A3C

---

### World Models ([arXiv:1803.10122](https://arxiv.org/pdf/1803.10122)) ([cited by 200+](https://scholar.google.com/scholar?cites=8020027393506054346))
By David Ha, [Jürgen Schmidhuber](https://scholar.google.com/citations?user=gLnCTgIAAAAJ)

**tl;dr** We explore building generative neural network models of popular reinforcement learning environments to learn a compressed spatial and temporal representation of the environment.

> We explore building generative neural network models of popular reinforcement learning environments. Our world model can be trained quickly in an unsupervised manner to learn a compressed spatial and temporal representation of the environment. By using features extracted from the world model as inputs to an agent, we can train a very compact and simple policy that can solve the required task. We can even train our agent entirely inside of its own hallucinated dream generated by its world model, and transfer this policy back into the actual environment.

**Interactive paper**: [worldmodels.github.io](https://worldmodels.github.io/)

**Keywords**: RL, model-based

---

### Binary Ensemble Neural Network: More Bits per Network or More Networks per Bit? ([arXiv:1806.07550](https://arxiv.org/pdf/1806.07550)) ([cited by 30+](https://scholar.google.com/scholar?cites=17391681667773276447))
By Shilin Zhu, Xin Dong, Hao Su

**tl;dr** We propose the Binary Ensemble Neural Network (BENN) to improve the performance of BNNs with limited efficiency cost.

> Binary neural networks (BNN) have been studied extensively since they run dramatically faster at lower memory and power consumption than floating-point networks, thanks to the efficiency of bit operations. However, contemporary BNNs whose weights and activations are both single bits suffer from severe accuracy degradation. To understand why, we investigate the representation ability, speed and bias/variance of BNNs through extensive experiments. We conclude that the error of BNNs is predominantly caused by the intrinsic instability (training time) and non-robustness (train & test time). Inspired by this investigation, we propose the Binary Ensemble Neural Network (BENN) which leverages ensemble methods to improve the performance of BNNs with limited efficiency cost. While ensemble techniques have been broadly believed to be only marginally helpful for strong classifiers such as deep neural networks, our analyses and experiments show that they are naturally a perfect fit to boost BNNs. We find that our BENN, which is faster and much more robust than state-of-the-art binary networks, can even surpass the accuracy of the full-precision floating number network with the same architecture.

**Keywords**: network architecture, compression

---

### Recurrent World Models Facilitate Policy Evolution ([arXiv:1809.01999](https://arxiv.org/pdf/1809.01999)) ([cited by 100+](https://scholar.google.com/scholar?cites=15327043406005665564))
By David Ha, [Jürgen Schmidhuber](https://scholar.google.com/citations?user=gLnCTgIAAAAJ)

**tl;dr** A generative recurrent neural network is quickly trained to model popular reinforcement learning environments through compressed spatio-temporal representations.

> A generative recurrent neural network is quickly trained in an unsupervised manner to model popular reinforcement learning environments through compressed spatio-temporal representations. The world model's extracted features are fed into compact and simple policies trained by evolution, achieving state of the art results in various environments. We also train our agent entirely inside of an environment generated by its own internal world model, and transfer this policy back into the actual environment.

**Interactive paper**: https://worldmodels.github.io/

**Keywords**: RL, RNN

---

### Neural Approaches to Conversational AI ([arXiv:1809.08267](https://arxiv.org/pdf/1809.08267)) ([cited by 100+](https://scholar.google.com/scholar?cites=11698047246691750774))
By Jianfeng Gao, Michel Galley, Lihong Li

**tl;dr** Neural approaches to conversational AI: A review and comparison.

> The present paper surveys neural approaches to conversational AI that have been developed in the last few years. We group conversational systems into three categories: (1) question answering agents, (2) task-oriented dialogue agents, and (3) chatbots. For each category, we present a review of state-of-the-art neural approaches, draw the connection between them and traditional approaches, and discuss the progress that has been made and challenges still being faced, using specific systems and models as case studies.

**Keywords**: NLP, chatbot

---

### 🌟 BERT: Pre-training of Deep Bidirectional Transformers for Language Understanding ([arXiv:1810.04805](https://arxiv.org/abs/1810.04805)) ([cited by 10,000+](https://scholar.google.com/scholar?cites=3166990653379142174))
By Jacob Devlin, Ming-Wei Chang, Kenton Lee, Kristina Toutanova

**tl;dr** We introduce a new language representation model called BERT, which stands for Bidirectional Encoder Representations from Transformers.

> We introduce a new language representation model called BERT, which stands for Bidirectional Encoder Representations from Transformers. Unlike recent language representation models, BERT is designed to pre-train deep bidirectional representations from unlabeled text by jointly conditioning on both left and right context in all layers. As a result, the pre-trained BERT model can be fine-tuned with just one additional output layer to create state-of-the-art models for a wide range of tasks, such as question answering and language inference, without substantial task-specific architecture modifications.

> BERT is conceptually simple and empirically powerful. It obtains new state-of-the-art results on eleven natural language processing tasks, including pushing the GLUE score to 80.5% (7.7% point absolute improvement), MultiNLI accuracy to 86.7% (4.6% absolute improvement), SQuAD v1.1 question answering Test F1 to 93.2 (1.5 point absolute improvement) and SQuAD v2.0 Test F1 to 83.1 (5.1 point absolute improvement).

**Keywords**: language model

---

### 🌟 Dreaming neural networks: forgetting spurious memories and reinforcing pure ones ([arXiv:1810.12217](https://arxiv.org/pdf/1810.12217)) ([cited by 10+](https://scholar.google.com/scholar?cites=9537827505537532968))
By Alberto Fachechi, Elena Agliari, Adriano Barra

**tl;dr** We propose an extension of the Hopfield model for associative neural networks that saturates the theoretical bound for symmetric networks.

> The standard Hopfield model for associative neural networks accounts for biological Hebbian learning and acts as the harmonic oscillator for pattern recognition, however its maximal storage capacity is α∼0.14, far from the theoretical bound for symmetric networks, i.e. α=1. Inspired by sleeping and dreaming mechanisms in mammal brains, we propose an extension of this model displaying the standard on-line (awake) learning mechanism (that allows the storage of external information in terms of patterns) and an off-line (sleep) unlearning&consolidating mechanism (that allows spurious-pattern removal and pure-pattern reinforcement): this obtained daily prescription is able to saturate the theoretical bound α=1, remaining also extremely robust against thermal noise. Both neural and synaptic features are analyzed both analytically and numerically. In particular, beyond obtaining a phase diagram for neural dynamics, we focus on synaptic plasticity and we give explicit prescriptions on the temporal evolution of the synaptic matrix. We analytically prove that our algorithm makes the Hebbian kernel converge with high probability to the projection matrix built over the pure stored patterns. Furthermore, we obtain a sharp and explicit estimate for the "sleep rate" in order to ensure such a convergence. Finally, we run extensive numerical simulations (mainly Monte Carlo sampling) to check the approximations underlying the analytical investigations (e.g., we developed the whole theory at the so called replica-symmetric level, as standard in the Amit-Gutfreund-Sompolinsky reference framework) and possible finite-size effects, finding overall full agreement with the theory.

**Keywords**: Hopfield networks, dreaming, network pruning

---

### *WaveGlow*: A Flow-based Generative Network for Speech Synthesis ([arXiv:1811.00002](https://arxiv.org/pdf/1811.00002.pdf)) ([cited by 200+](https://scholar.google.ca/scholar?cites=16743544780840044925))
By Ryan Prenger, Rafael Valle, Bryan Catanzaro

**tl;dr** WaveGlow: a flow-based network capable of generating high quality speech from mel-spectrograms.

> In this paper we propose WaveGlow: a flow-based network capable of generating high quality speech from mel-spectrograms. WaveGlow combines insights from Glow and WaveNet in order to provide fast, efficient and high-quality audio synthesis, without the need for auto-regression. WaveGlow is implemented using only a single network, trained using only a single cost function: maximizing the likelihood of the training data, which makes the training procedure simple and stable. Our PyTorch implementation produces audio samples at a rate of more than 500 kHz on an NVIDIA V100 GPU. Mean Opinion Scores show that it delivers audio quality as good as the best publicly available WaveNet implementation. All code will be made publicly available online.

**Keywords**: speech synthesis

---

### ACE: An Actor Ensemble Algorithm for Continuous Control with Tree Search ([arXiv:1811.02696](https://arxiv.org/pdf/1811.02696)) ([cited by 5+](https://scholar.google.com/scholar?cites=12478862000634249438))
By Shangtong Zhang, Hao Chen, Hengshuai Yao

**tl;dr** We propose an actor ensemble algorithm for continuous control with a deterministic policy in reinforcement learning.

> In this paper, we propose an actor ensemble algorithm, named ACE, for continuous control with a deterministic policy in reinforcement learning. In ACE, we use actor ensemble (i.e., multiple actors) to search the global maxima of the critic. Besides the ensemble perspective, we also formulate ACE in the option framework by extending the option-critic architecture with deterministic intra-option policies, revealing a relationship between ensemble and options. Furthermore, we perform a look-ahead tree search with those actors and a learned value prediction model, resulting in a refined value estimation. We demonstrate a significant performance boost of ACE over DDPG and its variants in challenging physical robot simulators.

**Keywords**: 

---

### Learning Attractor Dynamics for Generative Memory ([arXiv:1811.09556](https://arxiv.org/pdf/1811.09556)) ([cited by 8+](https://scholar.google.com/scholar?cites=9940290258944118765))
By Yan Wu, Greg Wayne, Karol Gregor, [Timothy Lillicrap](https://scholar.google.ca/citations?user=htPVdRMAAAAJ)

**tl;dr** We train a generative distributed memory without simulating the attractor dynamics, which improves robust retrieval in the presence of interference due to other stored patterns.

> A central challenge faced by memory systems is the robust retrieval of a stored pattern in the presence of interference due to other stored patterns and noise. A theoretically well-founded solution to robust retrieval is given by attractor dynamics, which iteratively clean up patterns during recall. However, incorporating attractor dynamics into modern deep learning systems poses difficulties: attractor basins are characterised by vanishing gradients, which are known to make training neural networks difficult. In this work, we avoid the vanishing gradient problem by training a generative distributed memory without simulating the attractor dynamics. Based on the idea of memory writing as inference, as proposed in the Kanerva Machine, we show that a likelihood-based Lyapunov function emerges from maximising the variational lower-bound of a generative memory. Experiments shows it converges to correct patterns upon iterative retrieval and achieves competitive performance as both a memory model and a generative model.

**Keywords**: MANN, Kanerva machine, one-shot learning

**Source code (Tensorflow)**: [github.com/deepmind/dynamic-kanerva-machines](https://github.com/deepmind/dynamic-kanerva-machines)

---

### A Style-Based Generator Architecture for Generative Adversarial Networks ([paper](https://www.researchgate.net/profile/Timo_Aila/publication/329607896_A_Style-Based_Generator_Architecture_for_Generative_Adversarial_Networks/links/5cff879692851c874c5db5f0/A-Style-Based-Generator-Architecture-for-Generative-Adversarial-Networks.pdf)) ([cited by 700+](https://scholar.google.com/scholar?cites=9092299839549248053))
By Tero Karras, Samuli Laine, Timo Aila

**tl;dr** We propose an alternative generator architecture for generative adversarial networks, borrowing from style transfer literature.

> We propose an alternative generator architecture for generative adversarial networks, borrowing from style transfer literature. The new architecture leads to an automatically learned, unsupervised separation of high-level attributes (e.g., pose and identity when trained on human faces) and stochastic variation in the generated images (e.g., freckles, hair), and it enables intuitive, scale-specific control of the synthesis. The new generator improves the state-of-the-art in terms of traditional distribution quality metrics, leads to demonstrably better interpolation properties, and also better disentangles the latent factors of variation. To quantify interpolation quality and disentanglement, we propose two new, automated methods that are applicable to any generator architecture. Finally, we introduce a new, highly varied and high-quality dataset of human faces.

**Keywords**: GAN, image generation

---

### Priors in Animal and Artificial Intelligence: Where Does Learning Begin? ([paper](https://r.unitn.it/filesresearch/images/cimec-abc/Publications/2018/versace_et_al._trend_cogn_sci_2018.pdf)) ([cited by 30+](https://scholar.google.com/scholar?cites=5763478379456668774))
By Elisabetta Versace, Antone Martinho-Truswell, Alex Kacelnik, and Giorgio Vallortigara

**tl;dr** A major goal for the next generation of artificial intelligence is to build machines that are able to reason and cope with novel tasks, environments, and situations.

> A major goal for the next generation of artificial intelligence (AI) is to build machines that are able to reason and cope with novel tasks, environments,  and situations in a manner that approaches the abilities of animals. Evidence from pre-cocial species suggests that driving learning through suitable priors can help to successfully face this challenge.

**Keywords**: biological learning

---

### *AlphaZero*: A general reinforcement learning algorithm that masters chess, shogi, and Go through self-play ([paper](http://science.sciencemag.org/cgi/content/full/362/6419/1140?ijkey=XGd77kI6W4rSc&keytype=ref&siteid=sci)) ([cited by 500+](https://scholar.google.com/scholar?cites=13089619183937314101))
By [David Silver](https://scholar.google.com/citations?user=-8DNE4UAAAAJ), Thomas Hubert, Julian Schrittwieser, Ioannis Antonoglou, Matthew Lai, Arthur Guez, Marc Lanctot, Laurent Sifre, Dharshan Kumaran, Thore Graepel, [Timothy Lillicrap](https://scholar.google.ca/citations?user=htPVdRMAAAAJ), Karen Simonyan, Demis Hassabis

**tl;dr** We generalize the AlphaGo Zero program and achieve superhuman performance in the games of chess, shogi, and Go.

> The game of chess is the longest-studied domain in the history of artificial intelligence. The strongest programs are based on a combination of sophisticated search techniques, domain-specific adaptations, and handcrafted evaluation functions that have been refined by human experts over several decades. By contrast, the AlphaGo Zero program recently achieved superhuman performance in the game of Go by reinforcement learning from self-play. In this paper, we generalize this approach into a single AlphaZero algorithm that can achieve superhuman performance in many challenging games. Starting from random play and given no domain knowledge except the game rules, AlphaZero convincingly defeated a world champion program in the games of chess and shogi (Japanese chess), as well as Go.

**Publication**: [deepmind.com](https://deepmind.com/blog/alphazero-shedding-new-light-grand-games-chess-shogi-and-go/)

**Early paper**: [arXiv:1712.01815](https://arxiv.org/pdf/1712.01815)

**How it works (in-depth)**: [YouTube](https://www.youtube.com/watch?v=_Z31-5D3RZg)

**Tutorial**: [web.stanford.edu](https://web.stanford.edu/~surag/posts/alphazero.html)

**Keywords**: RL, MCTS, self-play

## 2019

### Designing neural networks through neuroevolution ([paper](https://www.gwern.net/docs/rl/2019-stanley.pdf)) ([cited by 80+](https://scholar.google.com/scholar?cites=5962020871608022062))
By [Kenneth O. Stanley](https://scholar.google.ca/citations?user=6Q6oO1MAAAAJ), [Jeff Clune](https://scholar.google.ca/citations?hl=en&user=5TZ7f5wAAAAJ), Joel Lehman and Risto Miikkulainen

**tl;dr** This Review looks at several key aspects of modern neuroevolution, including large-scale computing, the benefits of novelty and diversity, the power of indirect encoding, and the field’s contributions to meta-learning and architecture search.

> Much of recent machine learning has focused on deep learning, in which neural network weights are trained through variants of stochastic gradient descent. An  alternative approach comes from the field of neuroevolution, which harnesses evolutionary algorithms to optimize neural networks, inspired by the fact that natural brains themselves are the products of an evolutionary process. Neuroevolution enables important capabilities that are typically unavailable to  gradient-based approaches, including learning neural network building blocks (for example activation functions), hyperparameters, architectures and even the algorithms for learning themselves. Neuroevolution also differs from deep learning (and deep reinforcement learning) by maintaining a population of solutions during search, enabling extreme exploration and massive parallelization. Finally, because neuroevolution research has (until recently) developed largely in isolation from gradient-based neural network research, it  has developed many unique and effective techniques that should be effective in other machine learning areas too. This Review looks at several key aspects of modern neuroevolution, including large-scale computing, the benefits of novelty and diversity, the power of indirect encoding, and the field’s contributions to meta-learning and architecture search. Our hope is to inspire renewed interest in the field as it meets the potential of the increasing computation available today, to highlight how many of its ideas can provide an exciting resource for inspiration and hybridization to the deep learning, deep reinforcement learning and machine learning communities, and to explain how neuroevolution could prove to be a critical tool in the long-term pursuit of artificial general intelligence.

**Keywords**: meta-learning, architecture search

---

### Paired Open-Ended Trailblazer (POET): Endlessly Generating Increasingly Complex and Diverse Learning Environments and Their Solutions ([arXiv:1901.01753](https://arxiv.org/pdf/1901.01753)) ([cited by 30+](https://scholar.google.com/scholar?cites=11725210653398039148))
By Rui Wang, Joel Lehman, [Jeff Clune](https://scholar.google.ca/citations?hl=en&user=5TZ7f5wAAAAJ), [Kenneth O. Stanley](https://scholar.google.ca/citations?user=6Q6oO1MAAAAJ)

**tl;dr** The Paired Open-Ended Trailblazer (POET) algorithm introduced in this paper explores many different paths through the space of possible problems.

> While the history of machine learning so far largely encompasses a series of problems posed by researchers and algorithms that learn their solutions, an important question is whether the problems themselves can be generated by the algorithm at the same time as they are being solved. Such a process would in effect build its own diverse and expanding curricula, and the solutions to problems at various stages would become stepping stones towards solving even more challenging problems later in the process. The Paired Open-Ended Trailblazer (POET) algorithm introduced in this paper does just that: it pairs the generation of environmental challenges and the optimization of agents to solve those challenges. It simultaneously explores many different paths through the space of possible problems and solutions and, critically, allows these stepping-stone solutions to transfer between problems if better, catalyzing innovation. The term open-ended signifies the intriguing potential for algorithms like POET to continue to create novel and increasingly complex capabilities without bound. Our results show that POET produces a diverse range of sophisticated behaviors that solve a wide range of environmental challenges, many of which cannot be solved by direct optimization alone, or even through a direct-path curriculum-building control algorithm introduced to highlight the critical role of open-endedness in solving ambitious challenges. The ability to transfer solutions from one environment to another proves essential to unlocking the full potential of the system as a whole, demonstrating the unpredictable nature of fortuitous stepping stones. We hope that POET will inspire a new push towards open-ended discovery across many domains, where algorithms like POET can blaze a trail through their interesting possible manifestations and solutions.

**Keywords**: neuroevolution

---

### *Transformer-XL*: Attentive Language Models Beyond a Fixed-Length Context ([arXiv:1901.02860](https://arxiv.org/pdf/1901.02860)) ([cited by 300+](https://scholar.google.com/scholar?cites=7150055013029036741))
By Zihang Dai, Zhilin Yang, Yiming Yang, Jaime Carbonell, Quoc V. Le, Ruslan Salakhutdinov

**tl;dr** We propose a novel neural architecture Transformer-XL that enables learning longer-term dependency beyond a fixed length without disrupting temporal coherence.

> Transformers have a potential of learning longer-term dependency, but are limited by a fixed-length context in the setting of language modeling. We propose a novel neural architecture Transformer-XL that enables learning dependency beyond a fixed length without disrupting temporal coherence. It consists of a segment-level recurrence mechanism and a novel positional encoding scheme. Our method not only enables capturing longer-term dependency, but also resolves the context fragmentation problem. As a result, Transformer-XL learns dependency that is 80% longer than RNNs and 450% longer than vanilla Transformers, achieves better performance on both short and long sequences, and is up to 1,800+ times faster than vanilla Transformers during evaluation.

---

### 🌟 *Go-Explore*: a New Approach for Hard-Exploration Problems ([arXiv:1901.10995](https://arxiv.org/pdf/1901.10995)) ([cited by 60+](https://scholar.google.com/scholar?cites=4261026080332318785))
By Adrien Ecoffet, Joost Huizinga, Joel Lehman, [Kenneth O. Stanley](https://scholar.google.ca/citations?user=6Q6oO1MAAAAJ), [Jeff Clune](https://scholar.google.ca/citations?hl=en&user=5TZ7f5wAAAAJ)

**tl;dr** We introduce a new RL algorithm that exploits the following principles: (1) remember previously visited states, (2) first return to a promising state, then explore from it, and (3) solve simulated environments through any available means, then robustify via imitation learning.

> A grand challenge in reinforcement learning is intelligent exploration, especially when rewards are sparse or deceptive. Two Atari games serve as benchmarks for such hard-exploration domains: Montezuma's Revenge and Pitfall. On both games, current RL algorithms perform poorly, even those with intrinsic motivation, which is the dominant method to improve performance on hard-exploration domains. To address this shortfall, we introduce a new algorithm called Go-Explore. It exploits the following principles: (1) remember previously visited states, (2) first return to a promising state (without exploration), then explore from it, and (3) solve simulated environments through any available means (including by introducing determinism), then robustify via imitation learning. The combined effect of these principles is a dramatic performance improvement on hard-exploration problems.

![Detachment and derailment](https://i.imgur.com/B6ig7ul.png)

> A hypothetical example of detachment in intrinsic motivation (IM) algorithms. Green areas indicate intrinsic reward, white indicates areas where no intrinsic reward remains, and purple areas indicate where the algorithm is currently exploring.

![A high-level overview of the Go-Explore algorithm](https://i.imgur.com/oF5p3mS.png)

> A high-level overview of the Go-Explore algorithm.

**Keywords**: RL, exploration, curiosity

---

### Model-Based Reinforcement Learning for Atari ([arXiv:1903.00374](https://arxiv.org/pdf/1903.00374)) ([cited by 80+](https://scholar.google.com/scholar?cites=4841006515344388997))
By Lukasz Kaiser, Mohammad Babaeizadeh, Piotr Milos, Blazej Osinski, Roy H Campbell, Konrad Czechowski, Dumitru Erhan, Chelsea Finn, Piotr Kozakowski, Sergey Levine, Afroz Mohiuddin, Ryan Sepassi, George Tucker, Henryk Michalewski

**tl;dr** Simulated Policy Learning: A Deep RL Algorithm based on Video Prediction Models

> In this paper, we explore how video prediction models can similarly enable agents to solve Atari games with fewer interactions than model-free methods. We describe Simulated Policy Learning (SimPLe), a complete model-based deep RL algorithm based on video prediction models and present a comparison of several model architectures, including a novel architecture that yields the best results in our setting. ... In most games SimPLe outperforms state-of-the-art model-free algorithms, in some games by over an order of magnitude.

**Keywords**: RL

---

### MSG-GAN: Multi-Scale Gradients for Generative Adversarial Networks ([arXiv:1903.06048](https://arxiv.org/pdf/1903.06048.pdf)) ([cited by 10+](https://scholar.google.com/scholar?cites=11292105841871558486))
By Animesh Karnewar, Oliver Wang

**tl;dr** Multi-Scale Gradient Generative Adversarial Networks for High Resolution Image Synthesis

> While Generative Adversarial Networks (GANs) have seen huge successes in image synthesis tasks, they are notoriously difficult to adapt to different datasets, in part due to instability during training and sensitivity to hyperparameters. One commonly accepted reason for this instability is that gradients passing from the discriminator to the generator become uninformative when there isn't enough overlap in the supports of the real and fake distributions. In this work, we propose the Multi-Scale Gradient Generative Adversarial Network (MSG-GAN), a simple but effective technique for addressing this by allowing the flow of gradients from the discriminator to the generator at multiple scales. This technique provides a stable approach for high resolution image synthesis, and serves as an alternative to the commonly used progressive growing technique. We show that MSG-GAN converges stably on a variety of image datasets of different sizes, resolutions and domains, as well as different types of loss functions and architectures, all with the same set of fixed hyperparameters. When compared to state-of-the-art GANs, our approach matches or exceeds the performance in most of the cases we tried.

**Keywords**: GAN, high resolution image generation

---

### *SPADE*: Semantic Image Synthesis with Spatially-Adaptive Normalization ([arXiv:1903.07291](https://arxiv.org/pdf/1903.07291)) ([cited by 400+](https://scholar.google.com/scholar?cites=12479535951654162053))
By Taesung Park, Ming-Yu Liu, Ting-Chun Wang, Jun-Yan Zhu

**tl;dr** We propose spatially-adaptive normalization, a simple but effective layer for synthesizing photorealistic images given an input semantic layout.

> We propose spatially-adaptive normalization, a simple but effective layer for synthesizing photorealistic images given an input semantic layout. Previous methods directly feed the semantic layout as input to the deep network, which is then processed through stacks of convolution, normalization, and nonlinearity layers. We show that this is suboptimal as the normalization layers tend to wash away semantic information. To address the issue, we propose using the input layout for modulating the activations in normalization layers through a spatially-adaptive, learned transformation. Experiments on several challenging datasets demonstrate the advantage of the proposed method over existing approaches, regarding both visual fidelity and alignment with input layouts. Finally, our model allows user control over both semantic and style.

**Interactive Demo**: [www.nvidia.com/en-us/research/ai-playground/](https://www.nvidia.com/en-us/research/ai-playground/)

**Video demos**

* [GauGAN: Changing Sketches into Photorealistic Masterpieces](https://www.youtube.com/watch?v=p5U4NgVGAwg)
* [Interactive Demo App "GauGAN", built using SPADE](https://youtu.be/MXWm6w4E5q0)

**Code**: [https://github.com/NVlabs/SPADE](https://github.com/NVlabs/SPADE)

**Keywords**: normalization, style transfer, image generation, GAN

---

### Regularizing Trajectory Optimization with Denoising Autoencoders ([arXiv:1903.11981](https://arxiv.org/pdf/1903.11981)) ([cited by 1+](https://scholar.google.com/scholar?cites=31624080029180005))
By Rinu Boney, Norman Di Palo, Mathias Berglund, Alexander Ilin, Juho Kannala, Antti Rasmus, Harri Valpola

**tl;dr** We propose to regularize trajectory optimization by means of a denoising autoencoder that is trained on the same trajectories as the model.

> Trajectory optimization using a learned model of the environment is one of the core elements of model-based reinforcement learning. This procedure often suffers from exploiting inaccuracies of the learned model. We propose to regularize trajectory optimization by means of a denoising autoencoder that is trained on the same trajectories as the model of the environment. We show that the proposed regularization leads to improved planning with both gradient-based and gradient-free optimizers. We also demonstrate that using regularized trajectory optimization leads to rapid initial learning in a set of popular motor control tasks, which suggests that the proposed approach can be a useful tool for improving sample efficiency.

**Keywords**: regularization, RL, planning

**Explanation**: [Video by Yannic Kilcher](https://www.youtube.com/watch?v=UjJU13GdL94)

---

### Generating Long Sequences with Sparse Transformers ([arXiv:1904.10509](https://arxiv.org/pdf/1904.10509)) ([cited by 60+](https://scholar.google.com/scholar?cites=15804183398893801414))
By Rewon Child, Scott Gray, Alec Radford, [Ilya Sutskever](https://scholar.google.ca/citations?user=x04W_mMAAAAJ)

**tl;dr** Sparse attention matrices, fast attention kernels, and fast kernels for training.

> Transformers are powerful sequence models, but require time and memory that grows quadratically with the sequence length. In this paper we introduce sparse factorizations of the attention matrix which reduce this to O(n√n). We also introduce a) a variation on architecture and initialization to train deeper networks, b) the recomputation of attention matrices to save memory, and c) fast attention kernels for training. We call networks with these changes Sparse Transformers, and show they can model sequences tens of thousands of timesteps long using hundreds of layers. We use the same architecture to model images, audio, and text from raw bytes, setting a new state of the art for density modeling of Enwik8, CIFAR-10, and ImageNet-64. We generate unconditional samples that demonstrate global coherence and great diversity, and show it is possible in principle to use self-attention to model sequences of length one million or more.

**Keywords**: transformers

---

### *XLNet*: Generalized Autoregressive Pretraining for Language Understanding ([arXiv:1906.08237](https://arxiv.org/pdf/1906.08237)) ([cited by 500+](https://scholar.google.com/scholar?cites=14487406216105917109))
By Zhilin Yang, Zihang Dai, Yiming Yang, Jaime Carbonell, Ruslan Salakhutdinov, Quoc V. Le

**tl;dr** We propose XLNet, a generalized autoregressive pretraining method that enables learning bidirectional contexts by maximizing the expected likelihood over all permutations.

> With the capability of modeling bidirectional contexts, denoising autoencoding based pretraining like BERT achieves better performance than pretraining approaches based on autoregressive language modeling. However, relying on corrupting the input with masks, BERT neglects dependency between the masked positions and suffers from a pretrain-finetune discrepancy. In light of these pros and cons, we propose XLNet, a generalized autoregressive pretraining method that (1) enables learning bidirectional contexts by maximizing the expected likelihood over all permutations of the factorization order and (2) overcomes the limitations of BERT thanks to its autoregressive formulation. Furthermore, XLNet integrates ideas from Transformer-XL, the state-of-the-art autoregressive model, into pretraining. Empirically, under comparable experiment settings, XLNet outperforms BERT on 20 tasks, often by a large margin, including question answering, natural language inference, sentiment analysis, and document ranking.

**Keywords**: NLP, transformer

---

### 🌟 Large Memory Layers with Product Keys ([arXiv:1907.05242](https://arxiv.org/pdf/1907.05242)) ([cited by 10+](https://scholar.google.com/scholar?cites=8134570978766877507))
By Guillaume Lample, Alexandre Sablayrolles, Marc'Aurelio Ranzato, Ludovic Denoyer, Hervé Jégou

**tl;dr** This paper introduces a structured memory which can be easily integrated into a neural network, by up to a billion parameters with a negligible computational overhead.

> This paper introduces a structured memory which can be easily integrated into a neural network. The memory is very large by design and significantly increases the capacity of the architecture, by up to a billion parameters with a negligible computational overhead. Its design and access pattern is based on product keys, which enable fast and exact nearest neighbor search. The ability to increase the number of parameters while keeping the same computational budget lets the overall system strike a better trade-off between prediction accuracy and computation efficiency both at training and test time. This memory layer allows us to tackle very large scale language modeling tasks. In our experiments we consider a dataset with up to 30 billion words, and we plug our memory layer in a state-of-the-art transformer-based architecture. In particular, we found that a memory augmented model with only 12 layers outperforms a baseline transformer model with 24 layers, while being twice faster at inference time. We release our code for reproducibility purposes.

**Source code**: [github.com/facebookresearch/XLM/blob/master/src/model/memory/memory.py](https://github.com/facebookresearch/XLM/blob/master/src/model/memory/memory.py)

**Keywords**: approximate k-nearest neighbor search

---

### Backpropagation through time and the brain ([paper](https://www.sciencedirect.com/science/article/pii/S0959438818302009)) ([cited by 20+](https://scholar.google.com/scholar?cites=14033049240084634707))
By [Timothy P. Lillicrap](https://scholar.google.ca/citations?user=htPVdRMAAAAJ), Adam Santoro

**tl;dr** It has long been speculated that the backpropagation-of-error algorithm may be a model of how the brain learns.

> It has long been speculated that the backpropagation-of-error algorithm (backprop) may be a model of how the brain learns. Backpropagation-through-time (BPTT) is the canonical temporal-analogue to backprop used to assign credit in recurrent neural networks in machine learning, but there's even less conviction about whether BPTT has anything to do with the brain. Even in machine learning the use of BPTT in classic neural network architectures has proven insufficient for some challenging temporal credit assignment (TCA) problems that we know the brain is capable of solving. Nonetheless, recent work in machine learning has made progress in solving difficult TCA problems by employing novel memory-based and attention-based architectures and algorithms, some of which are brain inspired. Importantly, these recent machine learning methods have been developed in the context of, and with reference to BPTT, and thus serve to strengthen BPTT's position as a useful normative guide for thinking about temporal credit assignment in artificial and biological systems alike.

**Keywords**: BPTT, TCA, RNN, biological learning

---

### 🌟 ALBERT: A Lite BERT for Self-supervised Learning of Language Representations ([arXiv:1909.11942](https://arxiv.org/abs/1909.11942)) ([cited by 500+](https://scholar.google.com/scholar?cites=6606720413006378435))
By Zhenzhong Lan, Mingda Chen, Sebastian Goodman, Kevin Gimpel, Piyush Sharma, Radu Soricut

**tl;dr** We present two parameter-reduction techniques to lower memory consumption and increase the training speed of BERT.

> Increasing model size when pretraining natural language representations often results in improved performance on downstream tasks. However, at some point further model increases become harder due to GPU/TPU memory limitations and longer training times. To address these problems, we present two parameter-reduction techniques to lower memory consumption and increase the training speed of BERT. Comprehensive empirical evidence shows that our proposed methods lead to models that scale much better compared to the original BERT. We also use a self-supervised loss that focuses on modeling inter-sentence coherence, and show it consistently helps downstream tasks with multi-sentence inputs. As a result, our best model establishes new state-of-the-art results on the GLUE, RACE, and squad benchmarks while having fewer parameters compared to BERT-large. The code and the pretrained models are available at this https URL.

**Keywords**: language model, NLP

---

### 🌟 *MIM*: High Mutual Information in Representation Learning with Symmetric Variational Inference ([arXiv:1910.04153](https://arxiv.org/pdf/1910.04153)) ([waiting for citations?<sup>*</sup>](https://scholar.google.com/scholar?cites=12274618158995019067))
By [Micha Livne](https://scholar.google.com/citations?user=Cmm8ZugAAAAJ), [Kevin Swersky](https://scholar.google.com/citations?hl=en&user=IrixA8MAAAAJ), [David J. Fleet](https://scholar.google.com/citations?hl=en&user=njOmQFsAAAAJ)

**tl;dr** We introduce the Mutual Information Machine (MIM), a novel formulation of representation learning, using a joint distribution over the observations and latent state in an encoder/decoder framework.

> We introduce the Mutual Information Machine (MIM), a novel formulation of representation learning, using a joint distribution over the observations and latent state in an encoder/decoder framework. Our key principles are symmetry and mutual information, where symmetry encourages the encoder and decoder to learn different factorizations of the same underlying distribution, and mutual information, to encourage the learning of useful representations for downstream tasks. Our starting point is the symmetric Jensen-Shannon divergence between the encoding and decoding joint distributions, plus a mutual information encouraging regularizer. We show that this can be bounded by a tractable cross entropy loss function between the true model and a parameterized approximation, and relate this to the maximum likelihood framework. We also relate MIM to variational autoencoders (VAEs) and demonstrate that MIM is capable of learning symmetric factorizations, with high mutual information that avoids posterior collapse.

**Keywords**: VAE, JSD

*: _Already cited by SentenceMIM_ ([arXiv:2003.02645](https://arxiv.org/pdf/2003.02645))

---

### Regularizing Model-Based Planning with Energy-Based Models ([arXiv:1910.05527](https://arxiv.org/pdf/1910.05527)) ([cited by 7+](https://scholar.google.com/scholar?cites=17518653031307172157))
By Rinu Boney, Juho Kannala, Alexander Ilin

**tl;dr** We propose to regularize planning with learned dynamics models using energy estimates of state transitions in the environment.

> Model-based reinforcement learning could enable sample-efficient learning by quickly acquiring rich knowledge about the world and using it to improve behaviour without additional data. Learned dynamics models can be directly used for planning actions but this has been challenging because of inaccuracies in the learned models. In this paper, we focus on planning with learned dynamics models and propose to regularize it using energy estimates of state transitions in the environment. We visually demonstrate the effectiveness of the proposed method and show that off-policy training of an energy estimator can be effectively used to regularize planning with pre-trained dynamics models. Further, we demonstrate that the proposed method enables sample-efficient learning to achieve competitive performance in challenging continuous control tasks such as Half-cheetah and Ant in just a few minutes of experience.

**Keywords**: regularization, RL, planning

---

### BART: Denoising Sequence-to-Sequence Pre-training for Natural Language Generation, Translation, and Comprehension ([arXiv:1910.13461](https://arxiv.org/abs/1910.13461)) ([cited by 190+](https://scholar.google.com/scholar?cites=10589398302527104823))
By Mike Lewis, Yinhan Liu, Naman Goyal, Marjan Ghazvininejad, Abdelrahman Mohamed, Omer Levy, Ves Stoyanov, Luke Zettlemoyer

**tl;dr** We present BART, a denoising autoencoder for pretraining sequence-to-sequence models.

> We present BART, a denoising autoencoder for pretraining sequence-to-sequence models. BART is trained by (1) corrupting text with an arbitrary noising function, and (2) learning a model to reconstruct the original text. It uses a standard Tranformer-based neural machine translation architecture which, despite its simplicity, can be seen as generalizing BERT (due to the bidirectional encoder), GPT (with the left-to-right decoder), and many other more recent pretraining schemes. We evaluate a number of noising approaches, finding the best performance by both randomly shuffling the order of the original sentences and using a novel in-filling scheme, where spans of text are replaced with a single mask token. BART is particularly effective when fine tuned for text generation but also works well for comprehension tasks. It matches the performance of RoBERTa with comparable training resources on GLUE and SQuAD, achieves new state-of-the-art results on a range of abstractive dialogue, question answering, and summarization tasks, with gains of up to 6 ROUGE. BART also provides a 1.1 BLEU increase over a back-translation system for machine translation, with only target language pretraining. We also report ablation experiments that replicate other pretraining schemes within the BART framework, to better measure which factors most influence end-task performance.

**Keywords**: language model, NLP

---

### 🌟 *MuZero*: Mastering Atari, Go, Chess and Shogi by Planning with a Learned Model ([arXiv:1911.08265](https://arxiv.org/pdf/1911.08265)) ([cited by 20+](https://scholar.google.com/scholar?cites=14616963305010665808))
By Julian Schrittwieser, Ioannis Antonoglou, Thomas Hubert, Karen Simonyan, Laurent Sifre, Simon Schmitt, Arthur Guez, Edward Lockhart, Demis Hassabis, Thore Graepel, [Timothy Lillicrap](https://scholar.google.ca/citations?user=htPVdRMAAAAJ), [David Silver](https://scholar.google.com/citations?user=-8DNE4UAAAAJ)

**tl;dr** We present the MuZero algorithm, which, by combining a tree-based search with a learned model, achieves superhuman performance in a range of challenging and visually complex domains.

> Constructing agents with planning capabilities has long been one of the main challenges in the pursuit of artificial intelligence. Tree-based planning methods have enjoyed huge success in challenging domains, such as chess and Go, where a perfect simulator is available. However, in real-world problems the dynamics governing the environment are often complex and unknown. In this work we present the MuZero algorithm which, by combining a tree-based search with a learned model, achieves superhuman performance in a range of challenging and visually complex domains, without any knowledge of their underlying dynamics. MuZero learns a model that, when applied iteratively, predicts the quantities most directly relevant to planning: the reward, the action-selection policy, and the value function. When evaluated on 57 different Atari games - the canonical video game environment for testing AI techniques, in which model-based planning approaches have historically struggled - our new algorithm achieved a new state of the art. When evaluated on Go, chess and shogi, without any knowledge of the game rules, MuZero matched the superhuman performance of the AlphaZero algorithm that was supplied with the game rules.

**Publication**: [deepmind.com/research/publications/Mastering-Atari-Go-Chess-and-Shogi-by-Planning-with-a-Learned-Model](https://deepmind.com/research/publications/Mastering-Atari-Go-Chess-and-Shogi-by-Planning-with-a-Learned-Model)

**Keywords**: game agent, planning, tree-based search, learned model

---

### *AttentionGAN*: Unpaired Image-to-Image Translation using Attention-Guided Generative Adversarial Networks ([arXiv:1911.11897](https://arxiv.org/pdf/1911.11897)) ([cited by 2+](https://scholar.google.com/scholar?cites=877210874690838822))
By Hao Tang, Hong Liu, Dan Xu, Philip H.S. Torr, Nicu Sebe

**tl;dr** AttentionGAN: Attention-Guided Generative Adversarial Networks for Unpaired Image-to-Image Translation

> State-of-the-art methods in the unpaired image-to-image translation are capable of learning a mapping from a source domain to a target domain with unpaired image data. Though the existing methods have achieved promising results, they still produce unsatisfied artifacts, being able to convert low-level information while limited in transforming high-level semantics of input images. One possible reason is that generators do not have the ability to perceive the most discriminative semantic parts between the source and target domains, thus making the generated images low quality. In this paper, we propose a new Attention-Guided Generative Adversarial Networks (AttentionGAN) for the unpaired image-to-image translation task. AttentionGAN can identify the most discriminative semantic objects and minimize changes of unwanted parts for semantic manipulation problems without using extra data and models. The attention-guided generators in AttentionGAN are able to produce attention masks via a built-in attention mechanism, and then fuse the generation output with the attention masks to obtain high-quality target images. Accordingly, we also design a novel attention-guided discriminator which only considers attended regions.

**Source code**: [github.com/Ha0Tang/AttentionGAN](https://github.com/Ha0Tang/AttentionGAN)

**Keywords**: AttentionGAN, GAN, img2img

---

### Dream to Control: Learning Behaviors by Latent Imagination ([arXiv:1912.01603](https://arxiv.org/pdf/1912.01603)) ([cited by 8+](https://scholar.google.com/scholar?cites=14974700822970491825))
By Danijar Hafner, Timothy Lillicrap, Jimmy Ba, Mohammad Norouzi

**tl;dr** We present Dreamer, a reinforcement learning agent that solves long-horizon tasks from images purely by latent imagination.

> Learned world models summarize an agent's experience to facilitate learning complex behaviors. While learning world models from high-dimensional sensory inputs is becoming feasible through deep learning, there are many potential ways for deriving behaviors from them. We present Dreamer, a reinforcement learning agent that solves long-horizon tasks from images purely by latent imagination. We efficiently learn behaviors by propagating analytic gradients of learned state values back through trajectories imagined in the compact state space of a learned world model. On 20 challenging visual control tasks, Dreamer exceeds existing approaches in data-efficiency, computation time, and final performance.

**Keywords**: imagination, planning, RL

---

### 🌟 12-in-1: Multi-Task Vision and Language Representation Learning ([arXiv:1912.02315](https://arxiv.org/pdf/1912.02315.pdf)) ([cited by 20+](https://scholar.google.com/scholar?cites=17276757515931533114))
By Jiasen Lu, Vedanuj Goswami, Marcus Rohrbach, Devi Parikh, Stefan Lee

**tl;dr** Large-scale multi-task training for vision-and-language tasks improves performance by up to 2.05 points on average across tasks, with a reduction from approximately 3 billion parameters to 270 million.

> Much of vision-and-language research focuses on a small but diverse set of independent tasks and supporting datasets often studied in isolation; however, the visually grounded language understanding skills required for success at these tasks overlap significantly. In this work, we investigate these relationships between vision-and-language tasks by developing a large-scale, multi-task training regime. Our approach culminates in a single model on 12 datasets from four broad categories of task including visual question answering, caption-based image retrieval, grounding referring expressions, and multi-modal verification. Compared to independently trained single-task models, this represents a reduction from approximately 3 billion parameters to 270 million while simultaneously improving performance by 2.05 points on average across tasks. We use our multi-task framework to perform in-depth analysis of the effect of joint training diverse tasks. Further, we show that finetuning task-specific models from our single multi-task model can lead to further improvements, achieving performance at or above the state-of-the-art.

**Keywords**: vision-language models


---

### Analyzing and Improving the Image Quality of StyleGAN ([arXiv:1912.04958](https://arxiv.org/pdf/1912.04958)) ([cited by 40+](https://scholar.google.com/scholar?cites=1745485284765270951))
By Tero Karras, Samuli Laine, Miika Aittala, Janne Hellsten, Jaakko Lehtinen, Timo Aila

**tl;dr** The style-based GAN architecture yields state-of-the-art results in data-driven unconditional generative image modeling.

> The style-based GAN architecture (StyleGAN) yields state-of-the-art results in data-driven unconditional generative image modeling. We expose and analyze several of its characteristic artifacts, and propose changes in both model architecture and training methods to address them. In particular, we redesign the generator normalization, revisit progressive growing, and regularize the generator to encourage good conditioning in the mapping from latent codes to images. In addition to improving image quality, this path length regularizer yields the additional benefit that the generator becomes significantly easier to invert. This makes it possible to reliably attribute a generated image to a particular network. We furthermore visualize how well the generator utilizes its output resolution, and identify a capacity problem, motivating us to train larger models for additional quality improvements. Overall, our improved model redefines the state of the art in unconditional image modeling, both in terms of existing distribution quality metrics as well as perceived image quality.

![Example](https://i.imgur.com/DvOkEuo.png)

**Explanation**

+ [StyleGANv2 Explained!](https://www.youtube.com/watch?v=u8qPvzk0AfY)
+ [Face editing with Generative Adversarial Networks](https://www.youtube.com/watch?v=dCKbRCUyop8)
+ [StyleGAN2: Near-Perfect Human Face Synthesis and More](https://www.youtube.com/watch?v=SWoravHhsUU)

**Source code**: [github.com/NVlabs/stylegan2](https://github.com/NVlabs/stylegan2)

**Source code (PyTorch)**: [github.com/lucidrains/stylegan2-pytorch](https://github.com/lucidrains/stylegan2-pytorch)

**Keywords**: GAN, image generation

---

### *GTN*: Generative Teaching Networks: Accelerating Neural Architecture Search by Learning to Generate Synthetic Training Data ([arXiv:1912.07768](https://arxiv.org/pdf/1912.07768)) ([cited by 2+](https://scholar.google.com/scholar?cites=17431064421189476903))
By Felipe Petroski Such, Aditya Rawal, Joel Lehman, [Kenneth O. Stanley](https://scholar.google.ca/citations?user=6Q6oO1MAAAAJ), [Jeff Clune](https://scholar.google.ca/citations?hl=en&user=5TZ7f5wAAAAJ)

**tl;dr** This paper investigates the intriguing question of whether we can create learning algorithms that automatically generate training data, learning environments, and curricula in order to help AI agents rapidly learn.

> This paper investigates the intriguing question of whether we can create learning algorithms that automatically generate training data, learning environments, and curricula in order to help AI agents rapidly learn. We show that such algorithms are possible via Generative Teaching Networks (GTNs), a general approach that is, in theory, applicable to supervised, unsupervised, and reinforcement learning, although our experiments only focus on the supervised case. GTNs are deep neural networks that generate data and/or training environments that a learner (e.g. a freshly initialized neural network) trains on for a few SGD steps before being tested on a target task. We then differentiate through the entire learning process via meta-gradients to update the GTN parameters to improve performance on the target task. GTNs have the beneficial property that they can theoretically generate any type of data or training environment, making their potential impact large. This paper introduces GTNs, discusses their potential, and showcases that they can substantially accelerate learning. We also demonstrate a practical and exciting application of GTNs: accelerating the evaluation of candidate architectures for neural architecture search (NAS), which is rate-limited by such evaluations, enabling massive speed-ups in NAS. GTN-NAS improves the NAS state of the art, finding higher performing architectures when controlling for the search proposal mechanism. GTN-NAS also is competitive with the overall state of the art approaches, which achieve top performance while using orders of magnitude less computation than typical NAS methods. Speculating forward, GTNs may represent a first step toward the ambitious goal of algorithms that generate their own training data and, in doing so, open a variety of interesting new research questions and directions.

**Keywords**: meta-learning, training data generation

---

### *FARGAN*: Alleviation of Gradient Exploding in GANs: Fake Can Be Real ([arXiv:1912.12485](https://arxiv.org/pdf/1912.12485)) ([waiting for citations](https://scholar.google.com/scholar?cites=2597201475146774957))
By Song Tao, Jia Wang

**tl;dr** We propose a novel training method of GANs in which certain fake samples are considered as real ones during the training process to alleviate mode collapse phenomenon.

> In order to alleviate the notorious mode collapse phenomenon in generative adversarial networks (GANs), we propose a novel training method of GANs in which certain fake samples are considered as real ones during the training process. This strategy can reduce the gradient value that generator receives in the region where gradient exploding happens. We show the process of an unbalanced generation and a vicious circle issue resulted from gradient exploding in practical training, which explains the instability of GANs. We also theoretically prove that gradient exploding can be alleviated by penalizing the difference between discriminator outputs and fake-as-real consideration for very close real and fake samples. Accordingly, Fake-As-Real GAN (FARGAN) is proposed with a more stable training process and a more faithful generated distribution. Experiments on different datasets verify our theoretical analysis.

**Keywords**: GAN, exploding gradient problem

## 2020

### Reformer: The Efficient Transformer ([arXiv:2001.04451](https://arxiv.org/pdf/2001.04451)) ([cited by 20+](https://scholar.google.com/scholar?cites=16827908105960721293))
By Nikita Kitaev, Łukasz Kaiser, Anselm Levskaya

**tl;dr** Efficient Transformer with locality-sensitive hashing and reversible residuals.

> Large Transformer models routinely achieve state-of-the-art results on a number of tasks but training these models can be prohibitively costly, especially on long sequences. We introduce two techniques to improve the efficiency of Transformers. For one, we replace dot-product attention by one that uses locality-sensitive hashing, changing its complexity from O(L2) to O(LlogL), where L is the length of the sequence. Furthermore, we use reversible residual layers instead of the standard residuals, which allows storing activations only once in the training process instead of N times, where N is the number of layers. The resulting model, the Reformer, performs on par with Transformer models while being much more memory-efficient and much faster on long sequences.

**Keywords**: NLP, transformers

---

### 🌟 PET: Exploiting Cloze Questions for Few Shot Text Classification and Natural Language Inference ([arXiv:2001.07676](https://arxiv.org/abs/2001.07676)) ([cited by 4+](https://scholar.google.com/scholar?cites=11757272191456169611))
By Timo Schick, Hinrich Schütze

**tl;dr** A semi-supervised training procedure that reformulates input examples as cloze-style phrases to help language models understand a given task.

> Some NLP tasks can be solved in a fully unsupervised fashion by providing a pretrained language model with "task descriptions" in natural language (e.g., Radford et al., 2019). While this approach underperforms its supervised counterpart, we show in this work that the two ideas can be combined: We introduce Pattern-Exploiting Training (PET), a semi-supervised training procedure that reformulates input examples as cloze-style phrases to help language models understand a given task. These phrases are then used to assign soft labels to a large set of unlabeled examples. Finally, regular supervised training is performed on the resulting training set. For several tasks and languages, PET outperforms both supervised training and unsupervised approaches in low-resource settings by a large margin.

Blog summary: [pragmatic.ml/pet](https://www.pragmatic.ml/pet/)
Code: [timoschick/pet](https://github.com/timoschick/pet)

**Keywords**: language models, natural language, NLP

---

### Towards a Human-like Open-Domain Chatbot ([arXiv:2001.09977](https://arxiv.org/pdf/2001.09977)) ([cited by 20+](https://scholar.google.com/scholar?cites=4079885979813489169))
By Daniel Adiwardana, Minh-Thang Luong, David R. So, Jamie Hall, Noah Fiedel, Romal Thoppilan, Zi Yang, Apoorv Kulshreshtha, Gaurav Nemade, Yifeng Lu, Quoc V. Le

**tl;dr** A multi-turn open-domain chatbot trained end-to-end on data mined and filtered from public domain social media conversations.

> We present Meena, a multi-turn open-domain chatbot trained end-to-end on data mined and filtered from public domain social media conversations. This 2.6B parameter neural network is simply trained to minimize perplexity of the next token. We also propose a human evaluation metric called Sensibleness and Specificity Average (SSA), which captures key elements of a human-like multi-turn conversation. Our experiments show strong correlation between perplexity and SSA. The fact that the best perplexity end-to-end trained Meena scores high on SSA (72% on multi-turn evaluation) suggests that a human-level SSA of 86% is potentially within reach if we can better optimize perplexity. Additionally, the full version of Meena (with a filtering mechanism and tuned decoding) scores 79% SSA, 23% higher in absolute SSA than the existing chatbots we evaluated.

**Keywords**: NLP, chatbot

---

### Generating Digital Painting Lighting Effects via RGB-space Geometry ([paper](https://lllyasviel.github.io/PaintingLight/files/TOG20PaintingLight.pdf)) (---
)
By Lvmin Zhang, Edgar Simo-Serra, Yi Ji, and Chunping Liu 

**tl;dr** We present an algorithm to generate digital painting lighting effects from a single image by simulating artists' coarse-to-fine workflow.

> We present an algorithm to generate digital painting lighting effects from a single image. Our algorithm is based on a key observation: artists use many overlapping strokes to paint lighting effects, i.e., pixels with dense stroke history tend to gather more illumination strokes. Based on this observation, we design an algorithm to both estimate the density of strokes in a digital painting using color geometry, and then generate novel lighting effects by mimicking artists' coarse-to-fine workflow. Coarse lighting effects are first generated using a wave transform, and then retouched according to the stroke density of the original illustrations into usable lighting effects.

>Our algorithm is content-aware, with generated lighting effects naturally adapting to image structures, and can be used as an interactive tool to simplify current labor-intensive workflows for generating lighting effects for digital and matte paintings. In addition, our algorithm can also produce usable lighting effects for photographs or 3D rendered images. We evaluate our approach with both an in-depth qualitative and a quantitative analysis which includes a perceptual user study. Results show that our proposed approach is not only able to produce favorable lighting effects with respect to existing approaches, but also that it is able to significantly reduce the needed interaction time.

**Website**: [lllyasviel.github.io/PaintingLight/](https://lllyasviel.github.io/PaintingLight/)

**Video**: [YouTube](https://www.youtube.com/watch?v=X7li86oMBLA)

**Keywords**: art generation, image lighting

---

### Product Kanerva Machines: Factorized Bayesian Memory ([arXiv:2002.02385](https://arxiv.org/pdf/2002.02385.pdf)) ([cited by 2+](https://scholar.google.com/scholar?cites=14422382372492266069))
By Adam Marblestone, Yan Wu, Greg Wayne

**tl;dr** We introduce the Product Kanerva Machine, which dynamically combines many smaller Kanerva Machines into one large one, which can exhibit unsupervised clustering, find sparse and combinatorial allocation patterns, and discover spatial tunings that approximately factorize simple images by object.

> An ideal cognitively-inspired memory system would compress and organize incoming items. The Kanerva Machine (Wu et al, 2018) is a Bayesian model that naturally implements online memory compression. However, the organization of the Kanerva Machine is limited by its use of a single Gaussian random matrix for storage. Here we introduce the Product Kanerva Machine, which dynamically combines many smaller Kanerva Machines. Its hierarchical structure provides a principled way to abstract invariant features and gives scaling and capacity advantages over single Kanerva Machines. We show that it can exhibit unsupervised clustering, find sparse and combinatorial allocation patterns, and discover spatial tunings that approximately factorize simple images by object.

**Keywords**: memory

---

### The Next Decade in AI: Four Steps Towards Robust Artificial Intelligence ([arXiv:2002.06177](https://arxiv.org/pdf/2002.06177)) ([cited by 3+](https://scholar.google.com/scholar?cites=10762068228089742706))
By Gary Marcus

**tl;dr** I propose a knowledge-driven, reasoning-based approach, centered around cognitive models, that could provide the substrate for a richer, more robust AI.

> Recent research in artificial intelligence and machine learning has largely emphasized general-purpose learning and ever-larger training sets and more and more compute. In contrast, I propose a hybrid, knowledge-driven, reasoning-based approach, centered around cognitive models, that could provide the substrate for a richer, more robust AI than is currently possible.

**Keywords**: 

---

### 🌟 *ANML*: Learning to Continually Learn ([arXiv:2002.09571](https://arxiv.org/pdf/2002.09571)) ([waiting for citations](https://scholar.google.com/scholar?cites=300016629715374995))
By Shawn Beaulieu, Lapo Frati, [Thomas Miconi](https://scholar.google.ca/citations?hl=en&user=EXun8woAAAAJ), Joel Lehman, [Kenneth O. Stanley](https://scholar.google.ca/citations?user=6Q6oO1MAAAAJ), [Jeff Clune](https://scholar.google.ca/citations?hl=en&user=5TZ7f5wAAAAJ), Nick Cheney

**tl;dr** We propose A Neuromodulated Meta-Learning Algorithm for continual learning without catastrophic forgetting at scale.

> Continual lifelong learning requires an agent or model to learn many sequentially ordered tasks, building on previous knowledge without catastrophically forgetting it. Much work has gone towards preventing the default tendency of machine learning models to catastrophically forget, yet virtually all such work involves manually-designed solutions to the problem. We instead advocate meta-learning a solution to catastrophic forgetting, allowing AI to learn to continually learn. Inspired by neuromodulatory processes in the brain, we propose A Neuromodulated Meta-Learning Algorithm (ANML). It differentiates through a sequential learning process to meta-learn an activation-gating function that enables context-dependent selective activation within a deep neural network. Specifically, a neuromodulatory (NM) neural network gates the forward pass of another (otherwise normal) neural network called the prediction learning network (PLN). The NM network also thus indirectly controls selective plasticity (i.e. the backward pass of) the PLN. ANML enables continual learning without catastrophic forgetting at scale: it produces state-of-the-art continual learning performance, sequentially learning as many as 600 classes (over 9,000 SGD updates).

**Keywords**: meta-learning, neuromodulation

---

### 🌟 *Backpropamine*: training self-modifying neural networks with differentiable neuromodulated plasticity ([arXiv:2002.10585](https://arxiv.org/pdf/2002.10585)) ([cited by 20+](https://scholar.google.com/scholar?cites=15035648294490623736))
By [Thomas Miconi](https://scholar.google.ca/citations?hl=en&user=EXun8woAAAAJ), Aditya Rawal, [Jeff Clune](https://scholar.google.ca/citations?hl=en&user=5TZ7f5wAAAAJ), [Kenneth O. Stanley](https://scholar.google.ca/citations?user=6Q6oO1MAAAAJ)

**tl;dr** We show that neuromodulated plasticity improves the performance of neural networks on both reinforcement learning and supervised learning tasks.

> The impressive lifelong learning in animal brains is primarily enabled by plastic changes in synaptic connectivity. Importantly, these changes are not passive, but are actively controlled by neuromodulation, which is itself under the control of the brain. The resulting self-modifying abilities of the brain play an important role in learning and adaptation, and are a major basis for biological reinforcement learning. Here we show for the first time that artificial neural networks with such neuromodulated plasticity can be trained with gradient descent. Extending previous work on differentiable Hebbian plasticity, we propose a differentiable formulation for the neuromodulation of plasticity. We show that neuromodulated plasticity improves the performance of neural networks on both reinforcement learning and supervised learning tasks. In one task, neuromodulated plastic LSTMs with millions of parameters outperform standard LSTMs on a benchmark language modeling task (controlling for the number of parameters). We conclude that differentiable neuromodulation of plasticity offers a powerful new framework for training neural networks.

**Keywords**: meta-learning, LSTM, biological learning, Hebbian learning

---

### 🌟 *SentenceMIM*: A Latent Variable Language Model ([arXiv:2003.02645](https://arxiv.org/pdf/2003.02645)) ([cited by 2+](https://scholar.google.com/scholar?cites=10250754712606094616))
By Micha Livne, Kevin Swersky, David J. Fleet

**tl;dr** We introduce SentenceMIM, a probabilistic auto-encoder for language modelling, trained with Mutual Information Machine learning.

> We introduce SentenceMIM, a probabilistic auto-encoder for language modelling, trained with Mutual Information Machine (MIM) learning. Previous attempts to learn variational auto-encoders for language data have had mixed success, with empirical performance well below state-of-the-art auto-regressive models, a key barrier being the occurrence of posterior collapse with VAEs. The recently proposed MIM framework encourages high mutual information between observations and latent variables, and is more robust against posterior collapse. This paper formulates a MIM model for text data, along with a corresponding learning algorithm. We demonstrate excellent perplexity (PPL) results on several datasets, and show that the framework learns a rich latent space, allowing for interpolation between sentences of different lengths with a fixed-dimensional latent representation. We also demonstrate the versatility of SentenceMIM by utilizing a trained model for question-answering, a transfer learning task, without fine-tuning.

![Chart](https://i.imgur.com/XBzVfyX.png "Chart")
*[Achieved state of the art](https://paperswithcode.com/sota/language-modelling-on-penn-treebank-word) on Penn Treebank (Word Level) dataset with 179M parameters and outperforms GPT2 with 12M parameters and no extra training.*

**Keywords**: NLP, VAE, MIM, JSD, LVM

---

### NeRF: Representing Scenes as Neural Radiance Fields for View Synthesis ([arXiv:2003.08934](https://arxiv.org/pdf/2003.08934)) ([cited by 1+](https://scholar.google.com/scholar?cites=9378169911033868166))
By Ben Mildenhall, Pratul P. Srinivasan, Matthew Tancik, Jonathan T. Barron, Ravi Ramamoorthi, Ren Ng

**tl;dr** We present a method for synthesizing novel views of complex scenes by optimizing an underlying continuous volumetric scene function using a sparse set of input views.

> We present a method that achieves state-of-the-art results for synthesizing novel views of complex scenes by optimizing an underlying continuous volumetric scene function using a sparse set of input views. Our algorithm represents a scene using a fully-connected (non-convolutional) deep network, whose input is a single continuous 5D coordinate (spatial location (x,y,z) and viewing direction (θ,ϕ)) and whose output is the volume density and view-dependent emitted radiance at that spatial location. We synthesize views by querying 5D coordinates along camera rays and use classic volume rendering techniques to project the output colors and densities into an image. Because volume rendering is naturally differentiable, the only input required to optimize our representation is a set of images with known camera poses. We describe how to effectively optimize neural radiance fields to render photorealistic novel views of scenes with complicated geometry and appearance, and demonstrate results that outperform prior work on neural rendering and view synthesis. View synthesis results are best viewed as videos, so we urge readers to view our supplementary video for convincing comparisons.

**Website**: [www.matthewtancik.com/nerf](http://www.matthewtancik.com/nerf)

**Summary**: [Video by Two Minute Papers](https://www.youtube.com/watch?v=nCpGStnayHk)

**Keywords**: volumetric scene generation

---

### GAN Compression: Efficient Architectures for Interactive Conditional GANs ([arXiv:2003.08936](https://arxiv.org/pdf/2003.08936)) ([cited by 10+](https://scholar.google.com/scholar?cites=4661770039879488594))
By Muyang Li, Ji Lin, Yaoyao Ding, Zhijian Liu, Jun-Yan Zhu, Song Han

**tl;dr** We propose a general-purpose compression framework for reducing the inference time and model size of the generator in cGANs.

> Conditional Generative Adversarial Networks (cGANs) have enabled controllable image synthesis for many computer vision and graphics applications. However, recent cGANs are 1-2 orders of magnitude more computationally-intensive than modern recognition CNNs. For example, GauGAN consumes 281G MACs per image, compared to 0.44G MACs for MobileNet-v3, making it difficult for interactive deployment. In this work, we propose a general-purpose compression framework for reducing the inference time and model size of the generator in cGANs. Directly applying existing CNNs compression methods yields poor performance due to the difficulty of GAN training and the differences in generator architectures. We address these challenges in two ways. First, to stabilize the GAN training, we transfer knowledge of multiple intermediate representations of the original model to its compressed model, and unify unpaired and paired learning. Second, instead of reusing existing CNN designs, our method automatically finds efficient architectures via neural architecture search (NAS). To accelerate the search process, we decouple the model training and architecture search via weight sharing. Experiments demonstrate the effectiveness of our method across different supervision settings (paired and unpaired), model architectures, and learning methods (e.g., pix2pix, GauGAN, CycleGAN). Without losing image quality, we reduce the computation of CycleGAN by more than 20X and GauGAN by 9X, paving the way for interactive image synthesis. The code and demo are publicly available.

**Keywords**: GAN, compression, image generation

---

### 🌟 Enhanced POET: Open-Ended Reinforcement Learning through Unbounded Invention of Learning Challenges and their Solutions ([arXiv:2003.08536](https://arxiv.org/pdf/2003.08536)) ([waiting for citations](https://scholar.google.com/scholar?cites=17583648324422024748))
By Rui Wang, Joel Lehman, Aditya Rawal, Jiale Zhi, Yulun Li, [Jeff Clune](https://scholar.google.ca/citations?hl=en&user=5TZ7f5wAAAAJ), [Kenneth O. Stanley](https://scholar.google.ca/citations?user=6Q6oO1MAAAAJ)

**tl;dr** We introduce and empirically validate two new innovations to the original POET, as well as two external innovations designed to elucidate its full potential.

> Creating open-ended algorithms, which generate their own never-ending stream of novel and appropriately challenging learning opportunities, could help to automate and accelerate progress in machine learning. A recent step in this direction is the Paired Open-Ended Trailblazer (POET), an algorithm that generates and solves its own challenges, and allows solutions to goal-switch between challenges to avoid local optima. However, the original POET was unable to demonstrate its full creative potential because of limitations of the algorithm itself and because of external issues including a limited problem space and lack of a universal progress measure. Importantly, both limitations pose impediments not only for POET, but for the pursuit of open-endedness in general. Here we introduce and empirically validate two new innovations to the original algorithm, as well as two external innovations designed to help elucidate its full potential. Together, these four advances enable the most open-ended algorithmic demonstration to date. The algorithmic innovations are (1) a domain-general measure of how meaningfully novel new challenges are, enabling the system to potentially create and solve interesting challenges endlessly, and (2) an efficient heuristic for determining when agents should goal-switch from one problem to another (helping open-ended search better scale). Outside the algorithm itself, to enable a more definitive demonstration of open-endedness, we introduce (3) a novel, more flexible way to encode environmental challenges, and (4) a generic measure of the extent to which a system continues to exhibit open-ended innovation. Enhanced POET produces a diverse range of sophisticated behaviors that solve a wide range of environmental challenges, many of which cannot be solved through other means.

**Explanation**: [Video by Henry AI Labs](https://www.youtube.com/watch?v=jxIkPxkN10U)

**Keywords**: POET, neuroevolution

---

### In-Domain GAN Inversion for Real Image Editing ([arXiv:2004.00049](https://arxiv.org/pdf/2004.00049.pdf)) (New)
By Jiapeng Zhu,  Yujun Shen,  Deli Zhao,  Bolei Zhou

**tl;dr** In this work, we argue that the GAN inversion task is required not only to reconstruct the target image by pixel values, but also to keep the inverted code in the semantic domain of the original latent space of well-trained GANs.

> In this work, we argue that the GAN inversion task is required not only to reconstruct the target image by pixel values, but also to keep the inverted code in the semantic domain of the original latent space of well-trained GANs. For this purpose, we propose In-Domain GAN inversion (IDInvert) by first training a novel domain-guided encoder which is able to produce in-domain latent code, and then performing domain-regularized optimization which involves the encoder as a regularizer to land the code inside the latent space when being finetuned. The in-domain codes produced by IDInvert enable high-quality real image editing with fixed GAN models.

Website: [genforce.github.io/idinvert](https://genforce.github.io/idinvert/)
Code (PyTorch): [genforce/idinvert_pytorch](https://github.com/genforce/idinvert_pytorch)
Code (Tensorflow): [genforce/idinvert](https://github.com/genforce/idinvert)

**Keywords**: image generation, style control

---

### Longformer: The Long-Document Transformer ([arXiv:2004.05150](https://arxiv.org/pdf/2004.05150)) ([cited by 4+](https://scholar.google.com/scholar?cites=9544623782762227924))
By Iz Beltagy, Matthew E. Peters, Arman Cohan

**tl;dr** We introduce Longformer with an attention mechanism that scales linearly with sequence length, making it easy to process documents of thousands of tokens or longer.

> Transformer-based models are unable to process long sequences due to their self-attention operation, which scales quadratically with the sequence length. To address this limitation, we introduce the Longformer with an attention mechanism that scales linearly with sequence length, making it easy to process documents of thousands of tokens or longer. Longformer's attention mechanism is a drop-in replacement for the standard self-attention and combines a local windowed attention with a task motivated global attention. Following prior work on long-sequence transformers, we evaluate Longformer on character-level language modeling and achieve state-of-the-art results on text8 and enwik8. In contrast to most prior work, we also pretrain Longformer and finetune it on a variety of downstream tasks. Our pretrained Longformer consistently outperforms RoBERTa on long document tasks and sets new state-of-the-art results on WikiHop and TriviaQA.

**Explanation**: [YouTube](https://www.youtube.com/watch?v=_8KNb5iqblE)

**Memory requirements**: [YouTube](https://www.youtube.com/watch?v=gJR28onlqzs)

**Keywords**: transformer

---

### 🌟 Divide-and-Conquer Monte Carlo Tree Search For Goal-Directed Planning ([arXiv:2004.11410](https://arxiv.org/pdf/2004.11410)) ([waiting for citations](https://scholar.google.com/scholar?cites=8335433676614214635))
By Giambattista Parascandolo, Lars Buesing, Josh Merel, Leonard Hasenclever, John Aslanides, Jessica B. Hamrick, Nicolas Heess, Alexander Neitz, Theophane Weber

**tl;dr** We propose a planning algorithm, Divide-and-Conquer Monte Carlo Tree Search (DC-MCTS), for approximating the optimal plan.

> Standard planners for sequential decision making (including Monte Carlo planning, tree search, dynamic programming, etc.) are constrained by an implicit sequential planning assumption: The order in which a plan is constructed is the same in which it is executed. We consider alternatives to this assumption for the class of goal-directed Reinforcement Learning (RL) problems. Instead of an environment transition model, we assume an imperfect, goal-directed policy. This low-level policy can be improved by a plan, consisting of an appropriate sequence of sub-goals that guide it from the start to the goal state. We propose a planning algorithm, Divide-and-Conquer Monte Carlo Tree Search (DC-MCTS), for approximating the optimal plan by means of proposing intermediate sub-goals which hierarchically partition the initial tasks into simpler ones that are then solved independently and recursively. The algorithm critically makes use of a learned sub-goal proposal for finding appropriate partitions trees of new tasks based on prior experience. Different strategies for learning sub-goal proposals give rise to different planning strategies that strictly generalize sequential planning. We show that this algorithmic flexibility over planning order leads to improved results in navigation tasks in grid-worlds as well as in challenging continuous control environments.

**Explanation**: [YouTube](https://www.youtube.com/watch?v=tjbEVY5XIk0)

**Keywords**: MCTS, planning, RL, search

---

### 🌟 First return then explore ([arXiv:2004.12919](https://arxiv.org/pdf/2004.12919)) ([waiting for citations](https://scholar.google.com/scholar?cluster=16571518062556861489))
By Adrien Ecoffet, Joost Huizinga, Joel Lehman, [Kenneth O. Stanley](https://scholar.google.ca/citations?user=6Q6oO1MAAAAJ), [Jeff Clune](https://scholar.google.ca/citations?hl=en&user=5TZ7f5wAAAAJ)

**tl;dr** We introduce Go-Explore, a family of RL algorithms that addresses these two challenges directly through the simple principles of explicitly remembering promising states and first returning.

> The promise of reinforcement learning is to solve complex sequential decision problems by specifying a high-level reward function only. However, RL algorithms struggle when, as is often the case, simple and intuitive rewards provide sparse and deceptive feedback. Avoiding these pitfalls requires thoroughly exploring the environment, but despite substantial investments by the community, creating algorithms that can do so remains one of the central challenges of the field. We hypothesize that the main impediment to effective exploration originates from algorithms forgetting how to reach previously visited states ("detachment") and from failing to first return to a state before exploring from it ("derailment"). We introduce Go-Explore, a family of algorithms that addresses these two challenges directly through the simple principles of explicitly remembering promising states and first returning to such states before exploring. Go-Explore solves all heretofore unsolved Atari games (those for which algorithms could not previously outperform humans when evaluated following current community standards) and surpasses the state of the art on all hard-exploration games, with orders of magnitude improvements on the grand challenges Montezuma's Revenge and Pitfall. We also demonstrate the practical potential of Go-Explore on a challenging and extremely sparse-reward robotics task. Additionally, we show that adding a goal-conditioned policy can further improve Go-Explore's exploration efficiency and enable it to handle stochasticity throughout training. The striking contrast between the substantial performance gains from Go-Explore and the simplicity of its mechanisms suggests that remembering promising states, returning to them, and exploring from them is a powerful and general approach to exploration, an insight that may prove critical to the creation of truly intelligent learning agents.

**Keywords**: RL, exploration, curiosity

---

### Recipes for building an open-domain chatbot ([arXiv:2004.13637](https://arxiv.org/pdf/2004.13637)) ([cited by 1+](https://scholar.google.com/scholar?cites=14419775353832230497))
By Stephen Roller, Emily Dinan, Naman Goyal, Da Ju, Mary Williamson, Yinhan Liu, Jing Xu, Myle Ott, Kurt Shuster, Eric M. Smith, Y-Lan Boureau, Jason Weston

**tl;dr** We show that large scale models can learn conversationalist skills when given appropriate training data and choice of generation strategy.

> Building open-domain chatbots is a challenging area for machine learning research. While prior work has shown that scaling neural models in the number of parameters and the size of the data they are trained on gives improved results, we show that other ingredients are important for a high-performing chatbot. Good conversation requires a number of skills that an expert conversationalist blends in a seamless way: providing engaging talking points and listening to their partners, and displaying knowledge, empathy and personality appropriately, while maintaining a consistent persona. We show that large scale models can learn these skills when given appropriate training data and choice of generation strategy. We build variants of these recipes with 90M, 2.7B and 9.4B parameter models, and make our models and code publicly available. Human evaluations show our best models are superior to existing approaches in multi-turn dialogue in terms of engagingness and humanness measurements. We then discuss the limitations of this work by analyzing failure cases of our models.

**Keywords**: NLP, chatbots

---

### Reinforcement Learning with Augmented Data ([arXiv:2004.14990](https://arxiv.org/pdf/2004.14990)) ([waiting for citations](https://scholar.google.com/scholar?cites=6008226201213548118))
By Michael Laskin, Kimin Lee, Adam Stooke, Lerrel Pinto, Pieter Abbeel, Aravind Srinivas

**tl;dr** We show that data diversity alone can make agents focus on meaningful information from high-dimensional observations without any changes to the reinforcement learning method.

> Learning from visual observations is a fundamental yet challenging problem in reinforcement learning (RL). Although algorithmic advancements combined with convolutional neural networks have proved to be a recipe for success, current methods are still lacking on two fronts: (a) sample efficiency of learning and (b) generalization to new environments. To this end, we present RAD: Reinforcement Learning with Augmented Data, a simple plug-and-play module that can enhance any RL algorithm. We show that data augmentations such as random crop, color jitter, patch cutout, and random convolutions can enable simple RL algorithms to match and even outperform complex state-of-the-art methods across common benchmarks in terms of data-efficiency, generalization, and wall-clock speed. We find that data diversity alone can make agents focus on meaningful information from high-dimensional observations without any changes to the reinforcement learning method. On the DeepMind Control Suite, we show that RAD is state-of-the-art in terms of data-efficiency and performance across 15 environments. We further demonstrate that RAD can significantly improve the test-time generalization on several OpenAI ProcGen benchmarks. Finally, our customized data augmentation modules enable faster wall-clock speed compared to competing RL techniques.

**Explanation**: [Video by Yannic Kilcher](https://www.youtube.com/watch?v=to7vCdkLi4s)

**Keywords**: RL, data augmentation

---

### TLDR: Extreme Summarization of Scientific Documents ([arXiv:2004.15011](https://arxiv.org/abs/2004.15011)) (New)

**tl;dr** We introduce TLDR generation, a new form of extreme summarization, for scientific papers that produces high-quality summaries while minimizing annotation burden.

> We introduce TLDR generation, a new form of extreme summarization, for scientific papers. TLDR generation involves high source compression and requires expert background knowledge and understanding of complex domain-specific language. To facilitate study on this task, we introduce SciTLDR, a new multi-target dataset of 5.4K TLDRs over 3.2K papers. SciTLDR contains both author-written and expert-derived TLDRs, where the latter are collected using a novel annotation protocol that produces high-quality summaries while minimizing annotation burden. We propose CATTS, a simple yet effective learning strategy for generating TLDRs that exploits titles as an auxiliary training signal. CATTS improves upon strong baselines under both automated metrics and human evaluations. Data and code are publicly available at this https URL.

---

### It's Easier to Translate out of English than into it: Measuring Neural Translation Difficulty by Cross-Mutual Information ([arXiv:2005.02354](https://arxiv.org/abs/2005.02354)) (New)
By Emanuele Bugliarello, Sabrina J. Mielke, Antonios Anastasopoulos, Ryan Cotterell, Naoaki Okazaki

**tl;dr** An asymmetric information-theoretic metric of machine translation difficulty that exploits the probabilistic nature of most neural machine translation models.

> The performance of neural machine translation systems is commonly evaluated in terms of BLEU. However, due to its reliance on target language properties and generation, the BLEU metric does not allow an assessment of which translation directions are more difficult to model. In this paper, we propose cross-mutual information (XMI): an asymmetric information-theoretic metric of machine translation difficulty that exploits the probabilistic nature of most neural machine translation models. XMI allows us to better evaluate the difficulty of translating text into the target language while controlling for the difficulty of the target-side generation component independent of the translation task. We then present the first systematic and controlled study of cross-lingual translation difficulties using modern neural translation systems. Code for replicating our experiments is available online at this https URL.

**Keywords**: mutual information

---

### Reference-Based Sketch Image Colorization using Augmented-Self Reference and Dense Semantic Correspondence ([arXiv:2005.05207](https://arxiv.org/pdf/2005.05207)) ([waiting for citations](https://scholar.google.com/scholar?cites=14285897374649732318))
By Junsoo Lee, Eungyeup Kim, Yunsung Lee, Dongjun Kim, Jaehyuk Chang, Jaegul Choo

**tl;dr** This paper tackles the automatic colorization task of a sketch image given an already-colored reference image.

> This paper tackles the automatic colorization task of a sketch image given an already-colored reference image. Colorizing a sketch image is in high demand in comics, animation, and other content creation applications, but it suffers from information scarcity of a sketch image. To address this, a reference image can render the colorization process in a reliable and user-driven manner. However, it is difficult to prepare for a training data set that has a sufficient amount of semantically meaningful pairs of images as well as the ground truth for a colored image reflecting a given reference (e.g., coloring a sketch of an originally blue car given a reference green car). To tackle this challenge, we propose to utilize the identical image with geometric distortion as a virtual reference, which makes it possible to secure the ground truth for a colored output image. Furthermore, it naturally provides the ground truth for dense semantic correspondence, which we utilize in our internal attention mechanism for color transfer from reference to sketch input. We demonstrate the effectiveness of our approach in various types of sketch image colorization via quantitative as well as qualitative evaluation against existing methods.

**Keywords**: image colorization, style transfer

---

### *Plan2Explore*: Planning to Explore via Self-Supervised World Models ([arXiv:2005.05960](https://arxiv.org/pdf/2005.05960)) (New)
By Ramanan Sekar, Oleh Rybkin, Kostas Daniilidis, Pieter Abbeel, Danijar Hafner, Deepak Pathak

**tl;dr** Plan2Explore is a self-supervised exploration and fast adaptation to new tasks, which need not be known during exploration.

> Reinforcement learning allows solving complex tasks, however, the learning tends to be task-specific and the sample efficiency remains a challenge. We present Plan2Explore, a self-supervised reinforcement learning agent that tackles both these challenges through a new approach to self-supervised exploration and fast adaptation to new tasks, which need not be known during exploration. During exploration, unlike prior methods which retrospectively compute the novelty of observations after the agent has already reached them, our agent acts efficiently by leveraging planning to seek out expected future novelty. After exploration, the agent quickly adapts to multiple downstream tasks in a zero or a few-shot manner. We evaluate on challenging control tasks from high-dimensional image inputs. Without any training supervision or task-specific interaction, Plan2Explore outperforms prior self-supervised exploration methods, and in fact, almost matches the performances oracle which has access to rewards.

**Keywords**: intrinsic curiosity, exploration, RL

**Demo and source code**: [ramanans1.github.io/plan2explore/](https://ramanans1.github.io/plan2explore/)

**Explanation**: [Video by Yannic Kilcher](https://www.youtube.com/watch?v=IiBFqnNu7A8)

---

### Language Models are Few-Shot Learners ([arXiv:2005.14165](https://arxiv.org/pdf/2005.14165)) ([cited by 160+](https://scholar.google.com/scholar?cites=15953747982133883426))
By Tom B. Brown, Benjamin Mann, Nick Ryder, Melanie Subbiah, Jared Kaplan, Prafulla Dhariwal, Arvind Neelakantan, Pranav Shyam, Girish Sastry, Amanda Askell, Sandhini Agarwal, Ariel Herbert-Voss, Gretchen Krueger, Tom Henighan, Rewon Child, Aditya Ramesh, Daniel M. Ziegler, Jeffrey Wu, Clemens Winter, Christopher Hesse, Mark Chen, Eric Sigler, Mateusz Litwin, Scott Gray, Benjamin Chess, Jack Clark, Christopher Berner, Sam McCandlish, Alec Radford, Ilya Sutskever, Dario Amodei

**tl;dr** We train GPT-3, an autoregressive language model with 175 billion parameters, 10x more than any previous non-sparse language

> Recent work has demonstrated substantial gains on many NLP tasks and benchmarks by pre-training on a large corpus of text followed by fine-tuning on a specific task. While typically task-agnostic in architecture, this method still requires task-specific fine-tuning datasets of thousands or tens of thousands of examples. By contrast, humans can generally perform a new language task from only a few examples or from simple instructions - something which current NLP systems still largely struggle to do. Here we show that scaling up language models greatly improves task-agnostic, few-shot performance, sometimes even reaching competitiveness with prior state-of-the-art fine-tuning approaches. Specifically, we train GPT-3, an autoregressive language model with 175 billion parameters, 10x more than any previous non-sparse language model, and test its performance in the few-shot setting. For all tasks, GPT-3 is applied without any gradient updates or fine-tuning, with tasks and few-shot demonstrations specified purely via text interaction with the model. GPT-3 achieves strong performance on many NLP datasets, including translation, question-answering, and cloze tasks, as well as several tasks that require on-the-fly reasoning or domain adaptation, such as unscrambling words, using a novel word in a sentence, or performing 3-digit arithmetic. At the same time, we also identify some datasets where GPT-3's few-shot learning still struggles, as well as some datasets where GPT-3 faces methodological issues related to training on large web corpora. Finally, we find that GPT-3 can generate samples of news articles which human evaluators have difficulty distinguishing from articles written by humans. We discuss broader societal impacts of this finding and of GPT-3 in general.

**Keywords**: language model, distributed computing

---

### Learning to Simulate Dynamic Environments with GameGAN ([preprint release coming May 25th](https://nv-tlabs.github.io/gameGAN/))
By Seung Wook Kim, Yuhao Zhou, Antonio Torralba, Sanja Fidler

**tl;dr** We introduce GameGAN, a generative model that learns to visually imitate a desired game by ingesting screenplay and keyboard actions during training.

> Simulation is a crucial component of any robotic system. In order to simulate correctly, we need to write complex rules of the environment: how dynamic agents behave, and how the actions of each of the agents affect the behavior of others. In this paper, we aim to learn a simulator by simply watching an agent interact with an environment. We focus on graphics games as a proxy of the real environment. We introduce GameGAN, a generative model that learns to visually imitate a desired game by ingesting screenplay and keyboard actions during training. Given a key pressed by the agent, GameGAN "renders" the next screen using a carefully designed generative adversarial network. Our approach offers key advantages over existing work: we design a memory module that builds an internal map of the environment, allowing for the agent to return to previously visited locations with high visual consistency. In addition, GameGAN is able to disentangle static and dynamic components within an image making the behavior of the model more interpretable, and relevant for downstream tasks that require explicit reasoning over dynamic elements. This enables many interesting applications such as swapping different components of the game to build new games that do not exist.

**Publication**: [blogs.nvidia.com/blog/2020/05/22/gamegan-research-pacman-anniversary/](https://blogs.nvidia.com/blog/2020/05/22/gamegan-research-pacman-anniversary/)

**Explanation**: [Video by Henry AI Labs](https://www.youtube.com/watch?v=H8F6J7mYyz0)

**Website**: [nv-tlabs.github.io/gameGAN/](https://nv-tlabs.github.io/gameGAN/)

**Keywords**: GAN, game agent, planning, image generation

---

### 🌟 Unsupervised Translation of Programming Languages ([arXiv:2006.03511](https://arxiv.org/pdf/2006.03511))
By Marie-Anne Lachaux, Baptiste Roziere, Lowik Chanussot, Guillaume Lample

**tl;dr** A fully unsupervised neural transcompiler that can translate functions between C++, Java, and Python with high accuracy.

> A transcompiler, also known as source-to-source translator, is a system that converts source code from a high-level programming language (such as C++ or Python) to another. Transcompilers are primarily used for interoperability, and to port codebases written in an obsolete or deprecated language (e.g. COBOL, Python 2) to a modern one. They typically rely on handcrafted rewrite rules, applied to the source code abstract syntax tree. Unfortunately, the resulting translations often lack readability, fail to respect the target language conventions, and require manual modifications in order to work properly. The overall translation process is timeconsuming and requires expertise in both the source and target languages, making code-translation projects expensive. Although neural models significantly outperform their rule-based counterparts in the context of natural language translation, their applications to transcompilation have been limited due to the scarcity of parallel data in this domain. In this paper, we propose to leverage recent approaches in unsupervised machine translation to train a fully unsupervised neural transcompiler. We train our model on source code from open source GitHub projects, and show that it can translate functions between C++, Java, and Python with high accuracy. Our method relies exclusively on monolingual source code, requires no expertise in the source or target languages, and can easily be generalized to other programming languages. We also build and release a test set composed of 852 parallel functions, along with unit tests to check the correctness of translations. We show that our model outperforms rule-based commercial baselines by a significant margin.

**Keywords**: transcompiler

---

### 🌟 Predictive Coding Approximates Backprop along Arbitrary Computation Graphs ([arXiv:2006.04182](https://arxiv.org/pdf/2006.04182.pdf)) ([cited by 3+](https://scholar.google.com/scholar?cites=12701710878381226840))
By Beren Millidge, Alexander Tschantz, Christopher L. Buckley

**tl;dr** Predictive coding can approximate backprop in multilayer-perceptrons using only local and Hebbian updates, while utilising only local Hebbian plasticity.

> Backpropagation of error (backprop) is a powerful algorithm for training machine learning architectures through end-to-end differentiation. However, backprop is often criticised for lacking biological plausibility. Recently, it has been shown that backprop in multilayer-perceptrons (MLPs) can be approximated using predictive coding, a biologically-plausible process theory of cortical computation which relies only on local and Hebbian updates. The power of backprop, however, lies not in its instantiation in MLPs, but rather in the concept of automatic differentiation which allows for the optimisation of any differentiable program expressed as a computation graph. Here, we demonstrate that predictive coding converges asymptotically (and in practice rapidly) to exact backprop gradients on arbitrary computation graphs using only local learning rules. We apply this result to develop a straightforward strategy to translate core machine learning architectures into their predictive coding equivalents. We construct predictive coding CNNs, RNNs, and the more complex LSTMs, which include a non-layer-like branching internal graph structure and multiplicative interactions. Our models perform equivalently to backprop on challenging machine learning benchmarks, while utilising only local and (mostly) Hebbian plasticity. Our method raises the potential that standard machine learning algorithms could in principle be directly implemented in neural circuitry, and may also contribute to the development of completely distributed neuromorphic architectures.

**Keywords**: metalearning, backpropagation

---

### A bio-inspired bistable recurrent cell allows for long-lasting memory ([arXiv:2006.05252](https://arxiv.org/pdf/2006.05252.pdf)) ([New](https://scholar.google.com/scholar?cites=11215192328819614152))
By Nicolas Vecoven, Damien Ernst, Guillaume Drion

**tl;dr** We take inspiration from biological neuron bistability to embed RNNs with long-lasting memory at the cellular level.

> Recurrent neural networks (RNNs) provide state-of-the-art performances in a wide variety of tasks that require memory. These performances can often be achieved thanks to gated recurrent cells such as gated recurrent units (GRU) and long short-term memory (LSTM). Standard gated cells share a layer internal state to store information at the network level, and long term memory is shaped by network-wide recurrent connection weights. Biological neurons on the other hand are capable of holding information at the cellular level for an arbitrary long amount of time through a process called bistability. Through bistability, cells can stabilize to different stable states depending on their own past state and inputs, which permits the durable storing of past information in neuron state. In this work, we take inspiration from biological neuron bistability to embed RNNs with long-lasting memory at the cellular level. This leads to the introduction of a new bistable biologically-inspired recurrent cell that is shown to strongly improves RNN performance on time-series which require very long memory, despite using only cellular connections (all recurrent connections are from neurons to themselves, i.e. a neuron state is not influenced by the state of other neurons). Furthermore, equipping this cell with recurrent neuromodulation permits to link them to standard GRU cells, taking a step towards the biological plausibility of GRU.

**Keywords**: RNN, LSTM, GRU, catastrophic forgetting

---

### Distributed Memory based Self-Supervised Differentiable Neural Computer ([arXiv:2007.10637](https://arxiv.org/pdf/2007.10637.pdf)) ([New](https://scholar.google.com/scholar?cites=3626825700559595339))
By Taewon Park, Inchul Choi, Minho Lee

**tl;dr** We propose a novel distributed memory-based self-supervised DNC architecture for enhanced memory augmented neural network performance.

> A differentiable neural computer (DNC) is a memory augmented neural network devised to solve a wide range of algorithmic and question answering tasks and it showed promising performance in a variety of domains. However, its single memory-based operations are not enough to store and retrieve diverse informative representations existing in many tasks. Furthermore, DNC does not explicitly consider the memorization itself as a target objective, which inevitably leads to a very slow learning speed of the model. To address those issues, we propose a novel distributed memory-based self-supervised DNC architecture for enhanced memory augmented neural network performance. We introduce (i) a multiple distributed memory block mechanism that stores information independently to each memory block and uses stored information in a cooperative way for diverse representation and (ii) a self-supervised memory loss term which ensures how well a given input is written to the memory. Our experiments on algorithmic and question answering tasks show that the proposed model outperforms all other variations of DNC in a large margin, and also matches the performance of other state-of-the-art memory-based network models.

**Keywords**: MANN

---

### Learning Disentangled Representations with Latent Variation Predictability ([arXiv:2007.12885](https://arxiv.org/pdf/2007.12885.pdf)) (New)
By Xinqi Zhu, Chang Xu, Dacheng Tao

**tl;dr** A fully unsupervised neural transcompiler that can translate functions between C++, Java, and Python with high accuracy.

> Latent traversal is a popular approach to visualize the disentangled latent representations. Given a bunch of variations in a single unit of the latent representation, it is expected that there is a change in a single factor of variation of the data while others are fixed. However, this impressive experimental observation is rarely explicitly encoded in the objective function of learning disentangled representations. This paper defines the variation predictability of latent disentangled representations. Given image pairs generated by latent codes varying in a single dimension, this varied dimension could be closely correlated with these image pairs if the representation is well disentangled. Within an adversarial generation process, we encourage variation predictability by maximizing the mutual information between latent variations and corresponding image pairs. We further develop an evaluation metric that does not rely on the ground-truth generative factors to measure the disentanglement of latent representations. The proposed variation predictability is a general constraint that is applicable to the VAE and GAN frameworks for boosting disentanglement of latent representations. Experiments show that the proposed variation predictability correlates well with existing ground-truth-required metrics and the proposed algorithm is effective for disentanglement learning.

**Keywords**: mutual information

---

### NVAE: A Deep Hierarchical Variational Autoencoder ([arXiv:2007.03898](https://arxiv.org/pdf/2007.03898)) (New)
By Arash Vahdat, Jan Kautz

**tl;dr** We propose Nouveau VAE, a deep hierarchical VAE for image generation using depth-wise separable convolutions and batch normalization.

> Normalizing flows, autoregressive models, variational autoencoders (VAEs), and deep energy-based models are among competing likelihood-based frameworks for deep generative learning. Among them, VAEs have the advantage of fast and tractable sampling and easy-to-access encoding networks. However, they are currently outperformed by other models such as normalizing flows and autoregressive models. While the majority of the research in VAEs is focused on the statistical challenges, we explore the orthogonal direction of carefully designing neural architectures for hierarchical VAEs. We propose Nouveau VAE (NVAE), a deep hierarchical VAE built for image generation using depth-wise separable convolutions and batch normalization. NVAE is equipped with a residual parameterization of Normal distributions and its training is stabilized by spectral regularization. We show that NVAE achieves state-of-the-art results among non-autoregressive likelihood-based models on the MNIST, CIFAR-10, and CelebA HQ datasets and it provides a strong baseline on FFHQ. For example, on CIFAR-10, NVAE pushes the state-of-the-art from 2.98 to 2.91 bits per dimension, and it produces high-quality images on CelebA HQ as shown in Fig. 1. To the best of our knowledge, NVAE is the first successful VAE applied to natural images as large as 256×256 pixels.

**Keywords**: VAE

---

### Audiovisual Speech Synthesis using Tacotron2 ([arXiv:2008.00620](https://arxiv.org/abs/2008.00620)) (New)
By Ahmed Hussen Abdelaziz, Anushree Prasanna Kumar, Chloe Seivwright, Gabriele Fanelli, Justin Binder, Yannis Stylianou, Sachin Kajarekar

**tl;dr** AVTacotron2: End-to-end and modular audiovisual speech synthesis for 3D face models.

> Audiovisual speech synthesis is the problem of synthesizing a talking face while maximizing the coherency of the acoustic and visual speech. In this paper, we propose and compare two audiovisual speech synthesis systems for 3D face models. The first system is the AVTacotron2, which is an end-to-end text-to-audiovisual speech synthesizer based on the Tacotron2 architecture. AVTacotron2 converts a sequence of phonemes representing the sentence to synthesize into a sequence of acoustic features and the corresponding controllers of a face model. The output acoustic features are used to condition a WaveRNN to reconstruct the speech waveform, and the output facial controllers are used to generate the corresponding video of the talking face. The second audiovisual speech synthesis system is modular, where acoustic speech is synthesized from text using the traditional Tacotron2. The reconstructed acoustic speech signal is then used to drive the facial controls of the face model using an independently trained audio-to-facial-animation neural network. We further condition both the end-to-end and modular approaches on emotion embeddings that encode the required prosody to generate emotional audiovisual speech. We analyze the performance of the two systems and compare them to the ground truth videos using subjective evaluation tests. The end-to-end and modular systems are able to synthesize close to human-like audiovisual speech with mean opinion scores (MOS) of 4.1 and 3.9, respectively, compared to a MOS of 4.1 for the ground truth generated from professionally recorded videos. While the end-to-end system gives a better overall quality, the modular approach is more flexible and the quality of acoustic speech and visual speech synthesis is almost independent of each other.

---

### It's Not Just Size That Matters: Small Language Models Are Also Few-Shot Learners ([arXiv:2009.07118](https://arxiv.org/pdf/2009.07118)) (New)
By Timo Schick, Hinrich Schütze

**tl;dr** We show that state-of-the-art natural language understanding can be obtained with language models whose parameter count is several orders of magnitude smaller.

> When scaled to hundreds of billions of parameters, pretrained language models such as GPT-3 (Brown et al., 2020) achieve remarkable few-shot performance on challenging natural language understanding benchmarks. In this work, we show that performance similar to GPT-3 can be obtained with language models whose parameter count is several orders of magnitude smaller. This is achieved by converting textual inputs into cloze questions that contain some form of task description, combined with gradient-based optimization; additionally exploiting unlabeled data gives further improvements. Based on our findings, we identify several key factors required for successful natural language understanding with small language models.

**Keywords**: GPT3, few-shot learning

---

### Contrastive learning of medical visual representations from paired images and text ([arXiv:2010.00747](https://arxiv.org/pdf/2010.00747.pdf)) (New)
By Yuhao Zhang, Hang Jiang, Yasuhide Miura, Christopher D. Manning, Curtis P. Langlotz

**tl;dr** Learning visual representations of medical images from the naturally occurring pairing of images and textual data via a bidirectional contrastive objective between the two modalities.

> Learning visual representations of medical images is core to medical image understanding but its progress has been held back by the small size of hand-labeled datasets. Existing work commonly relies on transferring weights from ImageNet pretraining, which is suboptimal due to drastically different image characteristics, or rule-based label extraction from the textual report data paired with medical images, which is inaccurate and hard to generalize. We propose an alternative unsupervised strategy to learn medical visual representations directly from the naturally occurring pairing of images and textual data. Our method of pretraining medical image encoders with the paired text data via a bidirectional contrastive objective between the two modalities is domain-agnostic, and requires no additional expert input. We test our method by transferring our pretrained weights to 4 medical image classification tasks and 2 zero-shot retrieval tasks, and show that our method leads to image representations that considerably outperform strong baselines in most settings. Notably, in all 4 classification tasks, our method requires only 10% as much labeled training data as an ImageNet initialized counterpart to achieve better or comparable performance, demonstrating superior data efficiency.

**Keywords**: vision-language models

---

### Vokenization: Improving Language Understanding with Contextualized, Visual-Grounded Supervision ([arXiv:2010.06775](https://arxiv.org/pdf/2010.06775.pdf)) (New)
By Hao Tan, Mohit Bansal

**tl;dr** Humans learn language by listening, speaking, writing, reading, and also, via interaction with the multimodal real world.

> Humans learn language by listening, speaking, writing, reading, and also, via interaction with the multimodal real world. Existing language pre-training frameworks show the effectiveness of text-only self-supervision while we explore the idea of a visually-supervised language model in this paper. We find that the main reason hindering this exploration is the large divergence in magnitude and distributions between the visually-grounded language datasets and pure-language corpora. Therefore, we develop a technique named “vokenization” that extrapolates multimodal alignments to language-only data by contextually mapping language tokens to their related images (which we call “vokens”). The “vokenizer” is trained on relatively small image captioning datasets and we then apply it to generate vokens for large language corpora. Trained with these contextually generated vokens, our visually-supervised language models show consistent improvements over self-supervised alternatives on multiple purelanguage tasks such as GLUE, SQuAD, and SWAG.

**Keywords**: vision-language models

Video: [Vokenization Explained](https://www.youtube.com/watch?v=KhWj2twqud8)

## Research paper channels

+ [Henry AI Labs](https://www.youtube.com/channel/UCHB9VepY6kYvZjj0Bgxnpbw)
+ [Yannic Kilcher](https://www.youtube.com/channel/UCZHmQk67mSJgfCCTn7xBfew)
+ [Lex Fridman](https://www.youtube.com/channel/UCSHZKyawb77ixDdsGog4iWA)
+ [Two Minute Papers](https://www.youtube.com/channel/UCbfYPyITQ-7l4upoX8nvctg)
+ [ArXiv Insights](https://www.youtube.com/channel/UCNIkB2IeJ-6AmZv7bQ1oBYg)

## Additional reading

For a deeper exploration into machine learning, its history, and other techniques:

+ [Highlights in artificial intelligence history before 2000](http://people.idsia.ch/~juergen/ai.html)
+ [Deep Learning in Neural Networks: An Overview](https://arxiv.org/pdf/1404.7828)
+ [Deep Reinforcement Learning](https://arxiv.org/pdf/1810.06339)
+ [Jürgen Schmidhuber's home page](http://people.idsia.ch/~juergen/)
+ [Activation Functions: Comparison of Trends in Practice and Research for Deep Learning](https://arxiv.org/pdf/1811.03378.pdf)
+ [Deep Reinforcement Learning that Matters](https://arxiv.org/pdf/1709.06560)

## Tips

- You can find recommended papers on Arxiv by clicking the Recommenders tab below the abstract and enabling the CORE recommender.

## Changelog

### 28 Oct 2020

1. Product Kanerva Machines: Factorized Bayesian Memory (Marblestone et al., 2020)
2. Scaling Memory-Augmented Neural Networks with Sparse Reads and Writes (Rae et al., 2016)
3. Distributed Memory based Self-Supervised Differentiable Neural Computer (Park et al., 2020)

### 26 Oct 2020

1. A bio-inspired bistable recurrent cell allows for long-lasting memory (Vecoven et al., 2020)

### 23 Oct 2020

Generated tl;dr summaries.

1. TLDR: Extreme Summarization of Scientific Documents (Cachola et al., 2020)
2. Tacotron: Towards End-to-End Speech Synthesis (Wang et al. 2017)
3. Natural TTS Synthesis by Conditioning WaveNet on Mel Spectrogram Predictions (Shen et al. 2017)
4. Audiovisual Speech Synthesis using Tacotron2 (Abdelaziz et al. 2020)
5. Waveglow: A flow-based generative network for speech synthesis (Prenger et al., 2019)
6. Language Models are Few-Shot Learners (Brown et al. 2020)
7. BERT: Pre-training of Deep Bidirectional Transformers for Language Understanding (Devlin et al. 2018)
8. BART: Denoising Sequence-to-Sequence Pre-training for Natural Language Generation, Translation, and Comprehension (Lewis et al., 2019)
9. ALBERT: A Lite BERT for Self-supervised Learning of Language Representations (Lan et al. 2019)

### 22 Oct 2020

1. Learning to reinforcement learn (Wang et al., 2016)
2. Predictive Coding Approximates Backprop along Arbitrary Computation Graphs (Millidge et al., 2020)

### 20 Oct 2020

1. In-Domain GAN Inversion for Real Image Editing (Zhu et al., 2020)
2. Interactive Sound Propagation with Bidirectional Path Tracing (Cao et al., 2016)
3. Exploiting Cloze Questions for Few Shot Text Classification and Natural Language Inference (Schick et al. 2020)
4. It's Not Just Size That Matters: Small Language Models Are Also Few-Shot Learners (Schick et al. 2020)
5. Vokenization: Improving Language Understanding with Contextualized, Visual-Grounded Supervision (Tan et al. 2020)
6. Contrastive learning of medical visual representations from paired images and text (Zhang et al. 2020)
7. 12-in-1: Multi-Task Vision and Language Representation Learning (Lu et al. 2020)
8. MSG-GAN: Multi-Scale Gradients for Generative Adversarial Networks (Karnewar et al. 2019)

### 15 Oct 2020

1. It's Easier to Translate out of English than into it: Measuring Neural Translation Difficulty by Cross-Mutual Information (Bugliarello et all. 2020)
2. Learning Disentangled Representations with Latent Variation Predictability (Zhu et al., 2020)
3. NVAE: A Deep Hierarchical Variational Autoencoder (Vahdat et al., 2020)
4. Unsupervised Translation of Programming Languages (Lachaux et al., 2020)
5. A generative vision model that trains with high data efficiency and breaks text-based CAPTCHAs (George et al. 2017)

### 17 May 2020
**New papers**

1. Large Memory Layers with Product Keys (Lample et al., Jul 2019)
2. Reformer: The Efficient Transformer (Kitaev et al., Jan 2020)
3. Recipes for building an open-domain chatbot (Roller et all., Apr 2020)
4. Multimodal Generative Models for Scalable Weakly-Supervised Learning (Wu et al. 2018)
5. Alleviation of Gradient Exploding in GANs: Fake Can Be Real (Tao et al. 2019)
6. Reference-Based Sketch Image Colorization using Augmented-Self Reference and Dense Semantic Correspondence (Lee et al. 2020)
7. Planning to Explore via Self-Supervised World Models (Sekar et al. 2020)
8. ACE: An Actor Ensemble Algorithm for Continuous Control with Tree Search (Zhang et al. 2018)
9. Dream to Control: Learning Behaviors by Latent Imagination (Hafner et al. 2019)
10. Generating Digital Painting Lighting Effects via RGB-space Geometry (Zhang et al. 2020)
11. Investigating Human Priors for Playing Video Games (Dubey et al. 2018)
12. Learning to Simulate Dynamic Environments with GameGAN (Kim et al. 2020)
13. How a life-like system emerges from a simplistic particle motion law (Schmickl et al. 2016)
14. Regularizing Model-Based Planning with Energy-Based Models (Boney et al. 2019)
15. Regularizing Trajectory Optimization with Denoising Autoencoders (Boney et al. 2019)
16. Design and Implementation of Multi-dimensional Flexible Antena-like Hair motivated by ’Aho-Hair’ in Japanese Anime Cartoons: Internal State Expressions beyond Design Limitations (Sasabuchi et al. 2015)

## To-do

1. Write a paper scraper that finds novel papers by constructing a Markov chain from citations and finding the stationary distribution

